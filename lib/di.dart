import 'dart:io';

import 'package:dio/dio.dart';
import 'package:dio/io.dart';
import 'package:get_it/get_it.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:spirit/core/network/network_info.dart';
import 'package:spirit/pages/account/data/datasources/account_details_local_datasource.dart';
import 'package:spirit/pages/account/data/datasources/account_details_remote_datasource.dart';
import 'package:spirit/pages/account/data/repositories/account_details_repository_impl.dart';
import 'package:spirit/pages/account/domain/repositories/account_details_repository.dart';
import 'package:spirit/pages/account/domain/usecases/user_info_usecase.dart';
import 'package:spirit/pages/account/presenter/cubits/user_info_cubit.dart';
import 'package:spirit/pages/addresses/domain/usecases/add_address_usecase.dart';
import 'package:spirit/pages/addresses/presenter/cubits/add_address_cubit.dart';
import 'package:spirit/pages/auth/data/datasources/auth_local_datasource.dart';
import 'package:spirit/pages/auth/data/datasources/auth_remote_datasource.dart';
import 'package:spirit/pages/auth/data/repositories/auth_repository_impl.dart';
import 'package:spirit/pages/auth/domain/repositories/auth_repository.dart';
import 'package:spirit/pages/auth/domain/usecases/login_usecase.dart';
import 'package:spirit/pages/auth/domain/usecases/register_usecase.dart';
import 'package:spirit/pages/auth/presentation/cubits/login_cubit.dart';
import 'package:spirit/pages/auth/presentation/cubits/register_cubit.dart';
import 'package:spirit/pages/cart/data/datasources/cart_local_datasource.dart';
import 'package:spirit/pages/cart/data/datasources/cart_remote_datasource.dart';
import 'package:spirit/pages/cart/data/repositories/cart_repository_impl.dart';
import 'package:spirit/pages/cart/domain/repositories/cart_repository.dart';
import 'package:spirit/pages/cart/domain/usecases/add_to_cart_usecase.dart';
import 'package:spirit/pages/cart/domain/usecases/cached_in_cart_usecase.dart';
import 'package:spirit/pages/cart/domain/usecases/calculate_usecase.dart';
import 'package:spirit/pages/cart/domain/usecases/cart_usecase.dart';
import 'package:spirit/pages/cart/domain/usecases/remove_from_cart_usecase.dart';
import 'package:spirit/pages/cart/presentation/cubits/calculate_cubit.dart';
import 'package:spirit/pages/cart/presentation/cubits/cart_cubit.dart';
import 'package:spirit/pages/cart/presentation/cubits/cart_item_cubit.dart';
import 'package:spirit/pages/categories/data/datasources/categories_local_datasource.dart';
import 'package:spirit/pages/categories/data/datasources/categories_remote_datasource.dart';
import 'package:spirit/pages/categories/data/repositories/categories_repository_impl.dart';
import 'package:spirit/pages/categories/domain/repositories/categories_repository.dart';
import 'package:spirit/pages/categories/domain/usecases/categories_usecase.dart';
import 'package:spirit/pages/categories/presentation/cubits/categories_cubit.dart';
import 'package:spirit/pages/checkout/data/datasources/checkout_local_datasource.dart';
import 'package:spirit/pages/checkout/data/datasources/checkout_remote_datasource.dart';
import 'package:spirit/pages/checkout/data/repositories/checkout_repository_impl.dart';
import 'package:spirit/pages/checkout/domain/repositories/checkout_repository.dart';
import 'package:spirit/pages/checkout/domain/usecases/checkout_usecase.dart';
import 'package:spirit/pages/checkout/domain/usecases/create_payment_link_usecase.dart';
import 'package:spirit/pages/checkout/presenter/cubits/checkout_cubit.dart';
import 'package:spirit/pages/checkout/presenter/cubits/create_payment_link_cubit.dart';
import 'package:spirit/pages/home/data/datasources/home_local_datasource.dart';
import 'package:spirit/pages/home/data/datasources/home_remote_datasource.dart';
import 'package:spirit/pages/home/data/repositories/home_repository_impl.dart';
import 'package:spirit/pages/home/domain/repositories/home_repository.dart';
import 'package:spirit/pages/home/domain/usecases/best_sellers_usecase.dart';
import 'package:spirit/pages/home/domain/usecases/flash_sales_usecase.dart';
import 'package:spirit/pages/home/domain/usecases/recently_added_usecase.dart';
import 'package:spirit/pages/home/domain/usecases/sliders_usecase.dart';
import 'package:spirit/pages/home/presentation/cubits/home_cubit.dart';
import 'package:spirit/pages/home/presentation/cubits/sliders_cubit.dart';
import 'package:spirit/pages/orders/data/datasources/orders_local_datasource.dart';
import 'package:spirit/pages/orders/data/datasources/orders_remote_datasource.dart';
import 'package:spirit/pages/orders/data/repositories/orders_repository_impl.dart';
import 'package:spirit/pages/orders/domain/repositories/orders_repository.dart';
import 'package:spirit/pages/orders/domain/usecases/cancel_order_usecase.dart';
import 'package:spirit/pages/orders/domain/usecases/order_usecase.dart';
import 'package:spirit/pages/orders/domain/usecases/orders_usecase.dart';
import 'package:spirit/pages/orders/presentation/cubit/order_cubit.dart';
import 'package:spirit/pages/orders/presentation/cubit/orders_cubit.dart';
import 'package:spirit/pages/product/data/datasources/product_local_datasource.dart';
import 'package:spirit/pages/product/data/datasources/product_remote_datasource.dart';
import 'package:spirit/pages/product/data/repositories/product_repository_impl.dart';
import 'package:spirit/pages/product/domain/repositories/product_repository.dart';
import 'package:spirit/pages/product/domain/usecases/product_usecase.dart';
import 'package:spirit/pages/product/presenter/cubits/product_cubit.dart';
import 'package:spirit/pages/products/data/datasources/products_local_datasource.dart';
import 'package:spirit/pages/products/data/datasources/products_remote_datasource.dart';
import 'package:spirit/pages/products/data/repositories/products_repository_impl.dart';
import 'package:spirit/pages/products/domain/repositories/products_repository.dart';
import 'package:spirit/pages/products/domain/usecases/products_usecase.dart';
import 'package:spirit/pages/products/presentation/cubits/products_cubit.dart';
import 'package:spirit/pages/addresses/data/datasources/addresses_local_datasource.dart';
import 'package:spirit/pages/addresses/data/datasources/addresses_remote_datasource.dart';
import 'package:spirit/pages/addresses/data/repositories/addresses_repository_impl.dart';
import 'package:spirit/pages/addresses/domain/repositories/addresses_repository.dart';
import 'package:spirit/pages/addresses/domain/usecases/addresses_usecase.dart';
import 'package:spirit/pages/addresses/domain/usecases/edit_address_usecase.dart';
import 'package:spirit/pages/addresses/presenter/cubits/addresses_cubit.dart';
import 'package:spirit/pages/addresses/presenter/cubits/edit_address_cubit.dart';
import 'package:spirit/pages/recipients/data/datasources/recipients_local_datasource.dart';
import 'package:spirit/pages/recipients/data/datasources/recipients_remote_datasource.dart';
import 'package:spirit/pages/recipients/data/repositories/recipients_repository_impl.dart';
import 'package:spirit/pages/recipients/domain/repositories/recipients_repository.dart';
import 'package:spirit/pages/recipients/domain/usecases/add_recipient_usecase.dart';
import 'package:spirit/pages/recipients/domain/usecases/edit_address_usecase.dart';
import 'package:spirit/pages/recipients/domain/usecases/recipients_usecase.dart';
import 'package:spirit/pages/recipients/presenter/cubits/add_recipient_cubit.dart';
import 'package:spirit/pages/recipients/presenter/cubits/edit_recipient_cubit.dart';
import 'package:spirit/pages/recipients/presenter/cubits/recipients_cubit.dart';
import 'package:spirit/pages/wishlist/data/datasources/wishlist_local_datasource.dart';
import 'package:spirit/pages/wishlist/data/datasources/wishlist_remote_datasource.dart';
import 'package:spirit/pages/wishlist/data/repositories/wishlist_repository_impl.dart';
import 'package:spirit/pages/wishlist/domain/repositories/wishlist_repository.dart';
import 'package:spirit/pages/wishlist/domain/usecases/add_to_wishlist_usecase.dart';
import 'package:spirit/pages/wishlist/domain/usecases/cached_in_wishlist_usecase.dart';
import 'package:spirit/pages/wishlist/domain/usecases/remove_from_wishlist_usecase.dart';
import 'package:spirit/pages/wishlist/domain/usecases/wishlist_usecase.dart';
import 'package:spirit/pages/wishlist/presentation/cubits/wishlist_cubit.dart';
import 'package:spirit/pages/wishlist/presentation/cubits/wishlist_item_cubit.dart';
import 'core/server/configurations.dart';

final sl = GetIt.instance;

// Registering dependencies for injection
Future<void> registerDependencies() async {
  await registerDio();
  await registerSharedPreference();
  await registerNetworkInfo();
  await registerUseCases();
  await registerDataSources();
  await registerRepositories();
  await registerCubits();
}

// Register Network info
Future<void> registerNetworkInfo() async {
  sl.registerLazySingleton<NetworkInfo>(
      () => NetworkInfoImpl(internetConnectionChecker: sl()));
  sl.registerLazySingleton(() => InternetConnectionChecker());
}

// Register SharedPreferences
Future<void> registerSharedPreference() async {
  final SharedPreferences sharedPreferences =
      await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
}

// Register Dio
Future<void> registerDio() async {
  var options = BaseOptions(baseUrl: ServerConfigurations.url);
  final dio = Dio(options);

  (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
      (HttpClient client) {
    client.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    return client;
  };
  sl.registerLazySingleton(() => dio);
}

// Register Use Cases
Future<void> registerUseCases() async {
  sl.registerFactory(() => RegisterUseCase(repository: sl()));
  sl.registerFactory(() => LoginUseCase(repository: sl()));
  sl.registerFactory(() => CategoriesUseCase(repository: sl()));
  sl.registerFactory(() => ProductsUseCase(repository: sl()));
  sl.registerFactory(() => ProductUseCase(repository: sl()));
  sl.registerFactory(() => WishlistUseCase(repository: sl()));
  sl.registerFactory(() => AddToWishlistUseCase(repository: sl()));
  sl.registerFactory(() => RemoveFromWishlistUseCase(repository: sl()));
  sl.registerFactory(() => CachedInWishlistUseCase(repository: sl()));
  sl.registerFactory(() => CartUseCase(repository: sl()));
  sl.registerFactory(() => AddToCartUseCase(repository: sl()));
  sl.registerFactory(() => RemoveFromCartUseCase(repository: sl()));
  sl.registerFactory(() => CheckoutUseCase(repository: sl()));
  sl.registerFactory(() => CachedInCartUseCase(repository: sl()));
  sl.registerFactory(() => OrderUseCase(repository: sl()));
  sl.registerFactory(() => OrdersUseCase(repository: sl()));
  sl.registerFactory(() => AddressesUseCase(repository: sl()));
  sl.registerFactory(() => AddAddressUseCase(repository: sl()));
  sl.registerFactory(() => EditAddressUseCase(repository: sl()));
  sl.registerFactory(() => SlidersUseCase(repository: sl()));
  sl.registerFactory(() => CancelOrderUseCase(repository: sl()));
  sl.registerFactory(() => CreatePaymentLinkUseCase(repository: sl()));
  sl.registerFactory(() => RecentlyAddedUseCase(repository: sl()));
  sl.registerFactory(() => BestSellersUseCase(repository: sl()));
  sl.registerFactory(() => FlashSalesUseCase(repository: sl()));
  sl.registerFactory(() => CalculateUseCase(repository: sl()));
  sl.registerFactory(() => UserInfoUseCase(repository: sl()));
  sl.registerFactory(() => RecipientsUseCase(repository: sl()));
  sl.registerFactory(() => AddRecipientUseCase(repository: sl()));
  sl.registerFactory(() => EditRecipientUseCase(repository: sl()));
}

// Register Data Sources
Future<void> registerDataSources() async {
  sl.registerLazySingleton<AuthRemoteDataSource>(
      () => AuthRemoteDataSourceImpl(client: sl()));
  sl.registerLazySingleton<AuthLocalDataSource>(
      () => AuthLocalDataSourceImpl());
  sl.registerLazySingleton<HomeLocalDataSource>(
      () => HomeLocalDataSourceImpl());
  sl.registerLazySingleton<HomeRemoteDataSource>(
      () => HomeRemoteDataSourceImpl(client: sl()));
  sl.registerLazySingleton<CategoriesLocalDataSource>(
      () => CategoriesLocalDataSourceImpl());
  sl.registerLazySingleton<CategoriesRemoteDataSource>(
      () => CategoriesRemoteDataSourceImpl(client: sl()));
  sl.registerLazySingleton<ProductsRemoteDataSource>(
      () => ProductsRemoteDataSourceImpl(client: sl()));
  sl.registerLazySingleton<ProductsLocalDataSource>(
      () => ProductsLocalDataSourceImpl());
  sl.registerLazySingleton<ProductRemoteDataSource>(
      () => ProductRemoteDataSourceImpl(client: sl()));
  sl.registerLazySingleton<ProductLocalDataSource>(
      () => ProductLocalDataSourceImpl());
  sl.registerLazySingleton<WishlistRemoteDataSource>(
      () => WishlistRemoteDataSourceImpl(client: sl()));
  sl.registerLazySingleton<WishlistLocalDataSource>(
      () => WishlistLocalDataSourceImpl());
  sl.registerLazySingleton<CartRemoteDataSource>(
      () => CartRemoteDataSourceImpl(client: sl()));
  sl.registerLazySingleton<CartLocalDataSource>(
      () => CartLocalDataSourceImpl());
  sl.registerLazySingleton<CheckoutRemoteDataSource>(
      () => CheckoutRemoteDataSourceImpl(client: sl()));
  sl.registerLazySingleton<CheckoutLocalDataSource>(
      () => CheckoutLocalDataSourceImpl());
  sl.registerLazySingleton<OrdersRemoteDataSource>(
      () => OrdersRemoteDataSourceImpl(client: sl()));
  sl.registerLazySingleton<OrdersLocalDataSource>(
      () => OrdersLocalDataSourceImpl());
  sl.registerLazySingleton<AddressesRemoteDataSource>(
      () => AddressesRemoteDataSourceImpl(client: sl()));
  sl.registerLazySingleton<AddressesLocalDataSource>(
      () => AddressesLocalDataSourceImpl());
  sl.registerLazySingleton<AccountDetailsLocalDataSource>(
      () => AccountDetailsLocalDataSourceImpl());
  sl.registerLazySingleton<AccountDetailsRemoteDataSource>(
      () => AccountDetailsRemoteDataSourceImpl(client: sl()));
  sl.registerLazySingleton<RecipientsRemoteDataSource>(
      () => RecipientsRemoteDataSourceImpl(client: sl()));
  sl.registerLazySingleton<RecipientsLocalDataSource>(
      () => RecipientsLocalDataSourceImpl());
}

// Register Repositories
Future<void> registerRepositories() async {
  sl.registerLazySingleton<AuthRepository>(() => AuthRepositoryImpl(
      remoteDataSource: sl(), localDataSource: sl(), networkInfo: sl()));
  sl.registerLazySingleton<HomeRepository>(() => HomeRepositoryImpl(
      remoteDataSource: sl(), localDataSource: sl(), networkInfo: sl()));
  sl.registerLazySingleton<CategoriesRepository>(() => CategoriesRepositoryImpl(
      remoteDataSource: sl(), localDataSource: sl(), networkInfo: sl()));
  sl.registerLazySingleton<ProductsRepository>(() => ProductsRepositoryImpl(
      remoteDataSource: sl(), localDataSource: sl(), networkInfo: sl()));
  sl.registerLazySingleton<ProductRepository>(() => ProductRepositoryImpl(
      remoteDataSource: sl(), localDataSource: sl(), networkInfo: sl()));
  sl.registerLazySingleton<WishlistRepository>(() => WishlistRepositoryImpl(
      remoteDataSource: sl(), localDataSource: sl(), networkInfo: sl()));
  sl.registerLazySingleton<CartRepository>(() => CartRepositoryImpl(
      remoteDataSource: sl(),
      localDataSource: sl(),
      authLocalDataSource: sl(),
      networkInfo: sl()));
  sl.registerLazySingleton<CheckoutRepository>(() => CheckoutRepositoryImpl(
      remoteDataSource: sl(),
      localDataSource: sl(),
      cartLocalDataSource: sl(),
      networkInfo: sl()));
  sl.registerLazySingleton<OrdersRepository>(() => OrdersRepositoryImpl(
      remoteDataSource: sl(), localDataSource: sl(), networkInfo: sl()));
  sl.registerLazySingleton<AddressesRepository>(() => AddressesRepositoryImpl(
      remoteDataSource: sl(), localDataSource: sl(), networkInfo: sl()));
  sl.registerLazySingleton<AccountDetailsRepository>(() =>
      AccountDetailsRepositoryImpl(
          remoteDataSource: sl(), localDataSource: sl(), networkInfo: sl()));
  sl.registerLazySingleton<RecipientsRepository>(() => RecipientsRepositoryImpl(
      remoteDataSource: sl(), localDataSource: sl(), networkInfo: sl()));
}

// Register Cubits
Future<void> registerCubits() async {
  sl.registerFactory(() => LoginCubit(loginUseCase: sl()));
  sl.registerFactory(() => RegisterCubit(registerUseCase: sl()));
  sl.registerLazySingleton(() => CategoriesCubit(categoriesUseCase: sl()));
  // Product/Products
  sl.registerFactory(() => ProductsCubit(productsUseCase: sl()));
  sl.registerFactory(() => ProductCubit(productUseCase: sl()));
  // Wishlist/Wishlists
  sl.registerLazySingleton(() => WishlistCubit(wishlistUseCase: sl()));
  sl.registerFactory(() => WishlistItemCubit(
      addToWishlistUseCase: sl(),
      removeFromWishlistUseCase: sl(),
      cachedInWishlistUseCase: sl()));
  // Cart/Carts
  sl.registerLazySingleton(() => CartCubit(cartUseCase: sl()));
  sl.registerLazySingleton(() => OrdersCubit(ordersUseCase: sl()));
  sl.registerFactory(() => CartItemCubit(
      addToCartUseCase: sl(),
      removeFromCartUseCase: sl(),
      cachedInCartUseCase: sl()));
  // Checkout/Order/Orders
  sl.registerFactory(() => CheckoutCubit(checkoutUseCase: sl()));
  // sl.registerFactory(() => OrdersCubit(ordersUseCase: sl()));
  sl.registerFactory(
      () => OrderCubit(orderUseCase: sl(), cancelOrderUseCase: sl()));
  // Addresses
  sl.registerFactory(() => AddressesCubit(addressesUseCase: sl()));
  sl.registerFactory(() => EditAddressCubit(editAddressUseCase: sl()));
  sl.registerFactory(() => AddAddressCubit(addAddressUseCase: sl()));
  // Recippients
  sl.registerFactory(() => RecipientsCubit(recipientsUseCase: sl()));
  sl.registerFactory(() => EditRecipientCubit(editRecipientUseCase: sl()));
  sl.registerFactory(() => AddRecipientCubit(addRecipientUseCase: sl()));
  // Images Slider
  sl.registerFactory(() => SlidersCubit(sliderUseCase: sl()));
  // Account details
  sl.registerFactory(
      () => UserInfoCubit(userInfoUseCase: sl(), editUserInfoUseCase: sl()));
  // Create payment link
  sl.registerFactory(
      () => CreatePaymentLinkCubit(createPaymentLinkUseCase: sl()));
  // Flash sells
  sl.registerFactory(() => HomeCubit(recentlyAddedUseCase: sl(), bestSellersUseCase: sl(), flashSalesUseCase: sl()));
  // Calculate cart
  sl.registerFactory(() => CalculateCubit(calculateUseCase: sl()));
}
