import 'package:flutter/material.dart';
import 'package:spirit/pages/addresses/domain/entities/address.dart';
import 'package:spirit/pages/addresses/presenter/address_page.dart';
import 'package:spirit/pages/addresses/presenter/addresses_page.dart';
import 'package:spirit/pages/auth/presentation/screens/login_page.dart';
import 'package:spirit/pages/auth/presentation/screens/register_page.dart';
import 'package:spirit/pages/browser/browser.dart';
import 'package:spirit/pages/checkout/presenter/checkout_page.dart';
import 'package:spirit/pages/dashboard/presentation/dashboard.dart';
import 'package:spirit/pages/recipients/domain/entities/recipient.dart';
import 'package:spirit/pages/recipients/presenter/recipient_page.dart';
import 'package:spirit/pages/recipients/presenter/recipients_page.dart';
import 'package:spirit/pages/start/presentation/start.dart';
import 'package:spirit/pages/success/presenter/success_page.dart';

class Routes {
  //  Initial Route
  static const initialRoute = "/";
  // Routes
  static const routeHome = "/home";
  static const routeLogin = "/login";
  static const routeLogout = "/logout";
  static const routeRegister = "/register";
  static const routeStart = "/start";
  static const routeDashboard = "/dashboard";
  static const routeAboutUs = "/about_us";
  static const routeBranches = "/branches";
  static const routeMyAccount = "/my_accounts";
  static const routeOrders = "/orders";
  static const routeLanguage = "/language";
  static const routeAddresses = "/addresses";
  static const routeAddAddress = "/add_address";
  static const routeEditAddress = "/edit_address";
  static const routeChangePassword = "/change_password";
  static const routeAccountDetails = "/account_details";
  static const routeProducts = "/products";
  static const routeWishlist = "/wishlist";
  static const routeRecipients = "/recipients";
  static const routeAddRecipient = "/add_recipients";
  static const routeEditRecipient = "/edit_recipients";
  static const routeContactUs = "/contact_us";

  static const routeRepair = "/repair";
  static const routeCart = "/cart";
  static const routeCheckout = "/checkout";
  static const routeCategories = "/categories";
  static const routeSearch = "/search";
  static const routeProduct = "/product";
  static const routeNavigator = "/navigator";
  static const routeNotifications = "/notifications";
  static const routeOrder = "/order";
  static const routeBrowser = "/browser";
  static const routeSuccess = "/success";

  // Mapping Routes With Pages
  static Route<dynamic> generateRoutes(RouteSettings settings) {
    var name = settings.name;
    switch (name) {
      case Routes.routeStart:
        return MaterialPageRoute(builder: (_) {
          return const StartPage();
        });
      case Routes.routeRegister:
        return MaterialPageRoute(builder: (_) {
          return const RegisterPage();
        });
      case Routes.routeLogin:
        return MaterialPageRoute(builder: (_) {
          return const LoginPage();
        });
      case Routes.routeDashboard:
        return MaterialPageRoute(builder: (_) {
          return const DashboardPage();
        });
      case Routes.routeCheckout:
        return MaterialPageRoute(builder: (_) {
          String? coupon = settings.arguments as String?;
          return CheckoutPage(coupon: coupon);
        });
      case Routes.routeAddresses:
        bool? selection = settings.arguments as bool?;
        return MaterialPageRoute(builder: (_) {
          return AddressesPage(selection: selection ?? false);
        });
      case Routes.routeRecipients:
        bool? selection = settings.arguments as bool?;
        return MaterialPageRoute(builder: (_) {
          return RecipientsPage(selection: selection ?? false);
        });
      case Routes.routeAddAddress:
        return MaterialPageRoute(builder: (_) {
          return AddressPage();
        });
      case Routes.routeEditAddress:
        Address address = settings.arguments as Address;
        return MaterialPageRoute(builder: (_) {
          return AddressPage(address: address);
        });
      case Routes.routeAddRecipient:
        return MaterialPageRoute(builder: (_) {
          return RecipientPage();
        });
      case Routes.routeEditRecipient:
        Recipient recipient = settings.arguments as Recipient;
        return MaterialPageRoute(builder: (_) {
          return RecipientPage(recipient: recipient);
        });
      case Routes.routeBrowser:
        String paymentLink = settings.arguments as String;
        return MaterialPageRoute(builder: (_) {
          return BrowserPage(url: paymentLink);
        });
      case Routes.routeSuccess:
        return MaterialPageRoute(builder: (_) {
          return const SuccessPage();
        });
      default:
        return MaterialPageRoute(builder: (_) {
          return const SizedBox();
        });
    }
  }
}
