import 'package:dartz/dartz.dart';
import 'package:spirit/core/usecases/usecase.dart';
import 'package:spirit/pages/home/domain/repositories/home_repository.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';
import '../../../../core/errors/failures.dart';

class FlashSalesUseCase extends UseCase<List<Product>, NoParams> {
  HomeRepository repository;
  FlashSalesUseCase({required this.repository});

  @override
  Future<Either<Failure, List<Product>>> call(NoParams params) {
    return repository.flashSells();
  }
}