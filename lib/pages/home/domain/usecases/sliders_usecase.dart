import 'package:dartz/dartz.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';
import 'package:spirit/pages/home/domain/repositories/home_repository.dart';

import '../entities/slider.dart';

class SlidersUseCase extends UseCase<List<Slider>, NoParams> {
  HomeRepository repository;
  SlidersUseCase({required this.repository});

  @override
  Future<Either<Failure, List<Slider>>> call(NoParams params) {
    return repository.slider();
  }
}

