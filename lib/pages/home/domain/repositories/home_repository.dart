import 'package:dartz/dartz.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/pages/home/domain/entities/slider.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';

abstract class HomeRepository {
  Future<Either<Failure, List<Slider>>> slider();
  Future<Either<Failure, List<Product>>> recentlyAdded();
  Future<Either<Failure, List<Product>>> bestSellers();
  Future<Either<Failure, List<Product>>> flashSells();
}
