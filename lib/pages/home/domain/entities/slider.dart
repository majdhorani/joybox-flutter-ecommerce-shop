import 'package:spirit/core/enums/slider_type.dart';

import '../../../../core/server/configurations.dart';

class Slider {
  String id;
  String? image;
  String? title;
  String? description;
  String? url;
  SliderType type;
  String data;

  Slider(
      {required this.id,
      required this.image,
      required this.title,
      required this.description,
      required this.url,
      required this.data,
      required this.type});

  factory Slider.fromJson(Map<String, dynamic> json) {
    return Slider(
        id: json['id'],
        image: json['image'],
        title: json['title'],
        description: json['description'],
        url: json['url'],
        data: json['data'],
        type: stringToSliderType(json['type']));
  }

  String getImage() {
    return image!;
    return ServerConfigurations.uploads + image!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['image'] = image;
    data['title'] = title;
    data['description'] = description;
    data['type'] = type;
    data['data'] = data;
    return data;
  }
}
