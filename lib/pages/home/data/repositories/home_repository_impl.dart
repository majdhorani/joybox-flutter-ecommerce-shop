import 'package:dartz/dartz.dart';
import 'package:spirit/core/errors/exceptions.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/network/network_info.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';
import '../../domain/entities/slider.dart';
import '../../domain/repositories/home_repository.dart';
import '../datasources/home_local_datasource.dart';
import '../datasources/home_remote_datasource.dart';

class HomeRepositoryImpl implements HomeRepository {
  HomeRemoteDataSource remoteDataSource;
  HomeLocalDataSource localDataSource;
  NetworkInfo networkInfo;
  HomeRepositoryImpl(
      {required this.remoteDataSource,
      required this.localDataSource,
      required this.networkInfo});

  @override
  Future<Either<Failure, List<Slider>>> slider() async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.slider();
        return Right(response.data ?? []);
      } catch (e) {
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }

  @override
  Future<Either<Failure, List<Product>>> recentlyAdded() async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.recentlyAdded();
        return Right(response.products);
      } catch (e) {
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }

  @override
  Future<Either<Failure, List<Product>>> bestSellers() async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.bestSellers();
        return Right(response.products);
      } 
      catch (e) {
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }
  
  @override
  Future<Either<Failure, List<Product>>> flashSells() async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.flashSells();
        return Right(response.products);
      } 
      catch (e) {
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }
}

