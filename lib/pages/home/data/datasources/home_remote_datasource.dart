import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:spirit/core/errors/exceptions.dart';
import 'package:spirit/core/server/responses/products_response.dart';
import '../../../../core/server/configurations.dart';
import '../../../../core/server/responses/best_sellers_response.dart';
import '../../../../core/server/responses/slider_response.dart';
import '../../../../core/session/session.dart';

abstract class HomeRemoteDataSource {
  Future<SliderResponse> slider();
  Future<ProductsResponse> recentlyAdded();
  Future<ProductsResponse> bestSellers();
  Future<ProductsResponse> flashSells();
}

class HomeRemoteDataSourceImpl implements HomeRemoteDataSource {
  Dio client;
  HomeRemoteDataSourceImpl({required this.client});

  @override
  Future<SliderResponse> slider() async {
    try {
      var token = Session.token();
      String jsonString = await rootBundle.loadString('assets/json/slider.json');
      Map<String, dynamic> jsonData = jsonDecode(jsonString);
      return SliderResponse.fromJson(jsonData);
    } catch (e) {
      print(e);
      if (e is DioError) {
        // Status Code
        print('error in slider');
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }

  @override
  Future<ProductsResponse> recentlyAdded() async {
    try {
      String jsonString = await rootBundle.loadString('assets/json/products.json');
      Map<String, dynamic> jsonData = jsonDecode(jsonString);
      var result = ProductsResponse.fromJson(jsonData);
      return result;
    } catch (e) {
      if (e is DioError) {
        // Status Code
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }

  @override
  Future<ProductsResponse> bestSellers() async {
    try {
      // user token
      var token = Session.token();
      String jsonString = await rootBundle.loadString('assets/json/products.json');
      Map<String, dynamic> jsonData = jsonDecode(jsonString);
      var result = ProductsResponse.fromJson(jsonData);
      return result;
    } catch (e) {
      print(e);
      if (e is DioError) {
        // Status Code
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }
  
  @override
  Future<ProductsResponse> flashSells() async {
    try {
      // user token
      var token = Session.token();
      String jsonString = await rootBundle.loadString('assets/json/products.json');
      Map<String, dynamic> jsonData = jsonDecode(jsonString);
      var result = ProductsResponse.fromJson(jsonData);
      return result;
    } catch (e) {
      print(e);
      if (e is DioError) {
        // Status Code
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }
}

