import 'package:flutter/material.dart';
import 'package:iconsax/iconsax.dart';
import 'package:spirit/pages/home/presentation/widgets/product_horizontal.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';

class ProductsHorizontal extends StatefulWidget {
  List<Product> products;
  String title;
  Color? backgroundColor;
  Color? headerColor;
  Color? textColor;
  Color? categoryColor;
  Color? sellingPriceColor;
  Color? regularPriceColor;
  Color? discountBackgroundColor;
  Color? discountTextColor;
  Color? borderColor;
  Widget? headerAction;
  GestureTapCallback? onAction;
  ProductsHorizontal(
      {super.key,
      required this.title,
      required this.products,
      this.backgroundColor,
      this.headerColor,
      this.textColor,
      this.categoryColor,
      this.regularPriceColor,
      this.sellingPriceColor,
      this.discountBackgroundColor,
      this.discountTextColor,
      this.headerAction,
      this.onAction,
      this.borderColor});

  @override
  State<ProductsHorizontal> createState() => _ProductsHorizontalState();
}

class _ProductsHorizontalState extends State<ProductsHorizontal> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: widget.backgroundColor,
      padding: EdgeInsets.symmetric(
          vertical: widget.backgroundColor != null ? 8 : 0),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                      child: Text(
                    widget.title.toUpperCase(),
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color:
                            widget.headerColor ?? Colors.black.withOpacity(0.8),
                        fontFamily: "Oswald"),
                    maxLines: 1,
                  )),
                  widget.headerAction ??
                      InkWell(
                          onTap: widget.onAction,
                          child: const Icon(Iconsax.arrow_right_2))
                ],
              ),
            ),
            const SizedBox(height: 8),
            SizedBox(
              height: 220,
              child: ListView.separated(
                  itemCount: widget.products.length,
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  shrinkWrap: false,
                  scrollDirection: Axis.horizontal,
                  separatorBuilder: (context, index) {
                    return const SizedBox(width: 12);
                  },
                  itemBuilder: (context, index) {
                    Product product = widget.products[index];
                    return SProductHorizontal(
                      product: product,
                      categoryColor: widget.categoryColor,
                      discountTextColor: widget.discountTextColor,
                      discountBackgroundColor: widget.discountBackgroundColor,
                      regularPriceColor: widget.regularPriceColor,
                      sellingPriceColor: widget.sellingPriceColor,
                      textColor: widget.textColor,
                      borderColor: widget.borderColor,
                    );
                  }),
            ),
          ]),
    );
  }
}
