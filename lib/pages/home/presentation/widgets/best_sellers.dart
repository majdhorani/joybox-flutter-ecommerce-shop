import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/di.dart';
import 'package:spirit/pages/home/presentation/cubits/home_cubit.dart';
import 'package:spirit/pages/home/presentation/cubits/home_state.dart';
import 'package:spirit/pages/home/presentation/widgets/products_horizontal.dart';

class BestSellers extends StatefulWidget {
  const BestSellers({super.key});

  @override
  State<BestSellers> createState() => _BestSellersState();
}

class _BestSellersState extends State<BestSellers> {
  HomeCubit bestSellersCubit = sl<HomeCubit>();

  @override
  void initState() {
    bestSellersCubit.bestSellers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
      bloc: bestSellersCubit,
      builder: (context, state) {
        if(state is HomeLoadingState) {
          return const SizedBox();
        }
        if(state is HomeLoadedState) {
          print(state.products);
          return ProductsHorizontal(title: 'best_sellers'.tr(), products: state.products);
        }
        return const SizedBox();
      }
    );
  }
}