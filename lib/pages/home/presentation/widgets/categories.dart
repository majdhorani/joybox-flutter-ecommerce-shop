import 'package:flutter/material.dart';

class SCategories extends StatelessWidget {
  const SCategories({super.key});

  Widget _buildCategory(String name) {
    return Expanded(
      child: Container(
        height: 60,
        padding: EdgeInsets.symmetric(horizontal: 15),
        margin: EdgeInsets.only(bottom: 12),
        decoration: BoxDecoration(border: Border.all(color: Colors.white, width: 1.5)),
        child: Center(child: Text(name.toUpperCase(), textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),)),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black.withOpacity(0.2),
      padding: const EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 4),
      child: Column(
        children: [
          Row(
            children: [
              _buildCategory("Beauty & Personal Care"),
              SizedBox(width: 16),
              _buildCategory("Health & Nutrition"),
            ],
          ),
          Row(
            children: [
              _buildCategory("Fragrance"),
              SizedBox(width: 16),
              _buildCategory("Sweets & Choclates"),
            ],
          ),
          // Row(
          //   children: [
          //     _buildCategory("Men's Fashion"),
          //     SizedBox(width: 16),
          //     _buildCategory("Kid's Fashion")
          //   ],
          // ),
          Row(
            children: [
              _buildCategory("Toys"),
              SizedBox(width: 16),
              _buildCategory("Watches")
            ],
          ),
          Row(
            children: [
              _buildCategory("Eyewear"),
              SizedBox(width: 16),
              _buildCategory("Flowers & Bouquets")
            ],
          ),
        ],
      ),
    );
  }
}
