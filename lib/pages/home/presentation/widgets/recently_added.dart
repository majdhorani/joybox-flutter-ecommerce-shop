import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/di.dart';
import 'package:spirit/pages/home/presentation/cubits/home_cubit.dart';
import 'package:spirit/pages/home/presentation/cubits/home_state.dart';
import 'package:spirit/pages/home/presentation/widgets/products_horizontal.dart';

class RecentlyAdded extends StatefulWidget {
  const RecentlyAdded({super.key});

  @override
  State<RecentlyAdded> createState() => _RecentlyAddedState();
}

class _RecentlyAddedState extends State<RecentlyAdded> {
  HomeCubit recentlyAddedCubit = sl<HomeCubit>();

  @override
  void initState() {
    recentlyAddedCubit.recentlyAdded(); 
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
      bloc: recentlyAddedCubit,
      builder: (context, state) {
        if(state is HomeLoadingState) {
          return const SizedBox();
        }
        if(state is HomeLoadedState) {
          return ProductsHorizontal(title: 'recently_added'.tr(), products: state.products);
        }
        return const SizedBox();
      }
    );
  }
}