import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:iconsax/iconsax.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';
import 'package:spirit/routes/routes.dart';

class SProductVertical extends StatefulWidget {
  Product product;
  Color? discountColor;
  SProductVertical({Key? key, required this.product, this.discountColor})
      : super(key: key);

  @override
  State<SProductVertical> createState() => _SProductVerticalState();
}

class _SProductVerticalState extends State<SProductVertical> {
  bool inWishlist = false;

  @override
  void initState() {
    super.initState();
    checkIfProductInWishlist();
  }

  void checkIfProductInWishlist() async {
    // bool result = await wishlistItemCubit.cached(id: widget.product.id);
    // setState(() {
    //   inWishlist = result;
    // });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, Routes.routeProduct,
            arguments: widget.product);
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border:
                    Border.all(width: 1, color: Colors.grey.withOpacity(0.2))),
            child: AspectRatio(
              aspectRatio: 0.8,
              child: Stack(
                children: [
                  SizedBox(
                    height: double.infinity,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: CachedNetworkImage(
                        imageUrl: '${widget.product.image}',
                        fit: BoxFit.contain,
                        placeholder: (context, url) {
                          return Container(
                            color: Colors.grey.withOpacity(0.1),
                            child: SizedBox(
                              width: double.infinity,
                              height: double.infinity,
                              child: Icon(Icons.image,
                                  size: 50,
                                  color: Colors.grey.withOpacity(0.4)),
                            ),
                          );
                        },
                        errorWidget: (context, url, error) {
                          return Container(
                            color: Colors.grey.withOpacity(0.1),
                            child: SizedBox(
                              width: double.infinity,
                              height: double.infinity,
                              child: Icon(Icons.image,
                                  size: 50,
                                  color: Colors.grey.withOpacity(0.4)),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                  if (widget.product.discount() > 0)
                    Align(
                      alignment: Alignment.bottomRight,
                      child: Container(
                        height: 21,
                        margin: const EdgeInsets.all(10),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 8, vertical: 3),
                        decoration: BoxDecoration(
                            color: (widget.discountColor) != null
                                ? widget.discountColor
                                : Colors.red,
                            borderRadius:
                                const BorderRadius.all(Radius.circular(6))),
                        child: Text(
                          '-${widget.product.discount()}%',
                          style: const TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  Material(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(8),
                    child: InkWell(
                        borderRadius: BorderRadius.circular(8),
                        splashColor: Colors.red.withOpacity(0.2),
                        onTap: () {
                          Navigator.pushNamed(context, Routes.routeProduct,
                              arguments: widget.product);
                        },
                        child: Container()),
                  ),
                ],
              ),
            ),
          ),
          Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 4,
                    ),
                    // Text(
                    //   'CLOTHES',
                    //   overflow: TextOverflow.ellipsis,
                    //   style: const TextStyle(
                    //       fontSize: 12,
                    //       fontWeight: FontWeight.bold,
                    //       color: Colors.grey,
                    //       fontFamily: "Oswald"),
                    //   maxLines: 1,
                    // ),
                    Text(
                      widget.product.name ?? '',
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                          fontSize: 16,
                          fontFamily: "Oswald",
                          fontWeight: FontWeight.w300),
                      maxLines: 2,
                    ),
                    Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: [
                        Text(
                          '${widget.product.salePrice} EUR',
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                              fontSize: 18,
                              fontFamily: "Oswald",
                              fontWeight: FontWeight.bold),
                          maxLines: 2,
                        ),
                        // if (widget.product.discount() > 0)
                        const SizedBox(width: 5),
                        // if (widget.product.discount() > 0)
                        Text(
                          '${widget.product.regularPrice} EUR',
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                              fontSize: 12,
                              color: Colors.red,
                              fontFamily: "Oswald",
                              decoration: TextDecoration.lineThrough),
                          maxLines: 2,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              // InkWell(
              //                 onTap: () {
              //                   // if (!Session.isLoggedIn()) {
              //                   //   showDialog(
              //                   //     context: context,
              //                   //     builder: (_) => WishlistNotAuthedDialog(
              //                   //       onLogin: () {
              //                   //         Navigator.pushNamed(
              //                   //             context, Routes.routeLogin);
              //                   //       },
              //                   //     ),
              //                   //   );
              //                   //   return;
              //                   // }
              //                   // wishlistItemCubit.add(widget.product);
              //                 },
              //                 child: Icon(Iconsax.heart)),
            ],
          )
        ],
      ),
    );
  }
}
