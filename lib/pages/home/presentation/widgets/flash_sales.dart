import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_timer_countdown/flutter_timer_countdown.dart';
import 'package:spirit/di.dart';
import 'package:spirit/pages/home/presentation/cubits/home_cubit.dart';
import 'package:spirit/pages/home/presentation/cubits/home_state.dart';
import 'package:spirit/pages/home/presentation/widgets/products_horizontal.dart';

class FlashSales extends StatefulWidget {
  const FlashSales({super.key});

  @override
  State<FlashSales> createState() => _FlashSalesState();
}

class _FlashSalesState extends State<FlashSales> {
  HomeCubit bestSellersCubit = sl<HomeCubit>();

  @override
  void initState() {
    bestSellersCubit.flashSales();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
        bloc: bestSellersCubit,
        builder: (context, state) {
          if (state is HomeLoadingState) {
            return const SizedBox();
          }
          if (state is HomeLoadedState) {
            print(state.products);
            return ProductsHorizontal(
                title: 'best_sellers'.tr(),
                products: state.products,
                backgroundColor: Color.fromARGB(255, 0, 0, 0),
                textColor: Colors.white,
                sellingPriceColor: Colors.white,
                regularPriceColor: Colors.pink,
                headerColor: Colors.white,
                categoryColor: Colors.pink,
                discountBackgroundColor: Colors.yellow,
                discountTextColor: Colors.black,
                borderColor: Colors.black.withOpacity(0.9),
                headerAction: TimerCountdown(
                  format: CountDownTimerFormat.daysHoursMinutesSeconds,
                  spacerWidth: 6,
                  endTime: DateTime.now().add(
                    Duration(
                      days: 5,
                      hours: 14,
                      minutes: 27,
                      seconds: 34,
                    ),
                  ),
                  enableDescriptions: false,
                  timeTextStyle: const TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
                  colonsTextStyle: const TextStyle(color: Colors.white, fontSize: 16),
                  onEnd: () {
                    print("Timer finished");
                  },
                ));
          }
          return const SizedBox();
        });
  }
}
