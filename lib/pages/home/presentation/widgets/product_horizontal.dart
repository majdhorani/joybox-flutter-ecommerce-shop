import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';
import 'package:spirit/routes/routes.dart';

class SProductHorizontal extends StatefulWidget {
  Product product;
  Color? discountBackgroundColor;
  Color? discountTextColor;
  Color? textColor;
  Color? categoryColor;
  Color? sellingPriceColor;
  Color? regularPriceColor;
  Color? borderColor;
  SProductHorizontal({Key? key, required this.product, this.discountBackgroundColor, this.discountTextColor, this.textColor, this.categoryColor, this.sellingPriceColor, this.regularPriceColor, this.borderColor})
      : super(key: key);

  @override
  State<SProductHorizontal> createState() => _SProductHorizontalState();
}

class _SProductHorizontalState extends State<SProductHorizontal> {
  bool inWishlist = false;

  @override
  void initState() {
    super.initState();
    checkIfProductInWishlist();
  }

  void checkIfProductInWishlist() async {
    // bool result = await wishlistItemCubit.cached(id: widget.product.id);
    // setState(() {
    //   inWishlist = result;
    // });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, Routes.routeProduct);
      },
      child: SizedBox(
        width: 120,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              width: 120,
              child: AspectRatio(
                aspectRatio: 1,
                child: Stack(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          border: Border.all(
                              width: 1, color: widget.borderColor ?? Colors.grey.withOpacity(0.2))),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: CachedNetworkImage(
                          imageUrl: '${widget.product.image}',
                          fit: BoxFit.cover,
                          placeholder: (context, url) {
                            return Container(
                              color: Colors.grey.withOpacity(0.1),
                              child: SizedBox(
                                width: double.infinity,
                                height: double.infinity,
                                child: Icon(Icons.image,
                                    size: 50,
                                    color: Colors.grey.withOpacity(0.4)),
                              ),
                            );
                          },
                          errorWidget: (context, url, error) {
                            return Container(
                              color: Colors.grey.withOpacity(0.1),
                              child: SizedBox(
                                width: double.infinity,
                                height: double.infinity,
                                child: Icon(Icons.image,
                                    size: 50,
                                    color: Colors.grey.withOpacity(0.4)),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                    if (widget.product.discount() > 0)
                      Align(
                        alignment: Alignment.bottomRight,
                        child: Container(
                          height: 21,
                          width: 50,
                          margin: const EdgeInsets.all(10),
                          alignment: Alignment.center,
                          padding: const EdgeInsets.symmetric(horizontal: 4),
                          decoration: BoxDecoration(
                              color: widget.discountBackgroundColor ?? Colors.red,
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(6))),
                          child: Text(
                            '-${widget.product.discount()}%',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                                color: widget.discountTextColor ?? Colors.white),
                          ),
                        ),
                      ),
                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                          splashColor: Colors.red.withOpacity(0.2),
                          onTap: () {
                            Navigator.pushNamed(context, Routes.routeProduct,
                                arguments: widget.product);
                          },
                          child: Container()),
                    ),
                  ],
                ),
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 8,
                      ),
                      Text(
                        widget.product.categories.first.name.toString(),
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: widget.categoryColor ?? Colors.grey,
                            fontFamily: "Oswald"),
                        maxLines: 1,
                      ),
                      Text(
                        widget.product.name ?? '',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 14,
                            color: widget.textColor,
                            fontFamily: "Oswald",
                            fontWeight: FontWeight.w300),
                        maxLines: 2,
                      ),
                      Row(
                        children: [
                          Text(
                            '${widget.product.getSellingPrice()} EUR',
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: "Oswald",
                                color: widget.sellingPriceColor,
                                fontWeight: FontWeight.bold),
                            maxLines: 2,
                          ),
                          if (widget.product.discount() > 0)
                            const SizedBox(width: 5),
                          if (widget.product.discount() > 0)
                            Expanded(
                              child: Text(
                                '${widget.product.getRegularPrice()} EUR',
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontSize: 9,
                                    color: widget.regularPriceColor ?? Colors.red,
                                    fontFamily: "Oswald",
                                    decoration: TextDecoration.lineThrough),
                                maxLines: 2,
                              ),
                            ),
                        ],
                      ),
                    ],
                  ),
                ),
                // InkWell(
                //                 onTap: () {
                //                   // if (!Session.isLoggedIn()) {
                //                   //   showDialog(
                //                   //     context: context,
                //                   //     builder: (_) => WishlistNotAuthedDialog(
                //                   //       onLogin: () {
                //                   //         Navigator.pushNamed(
                //                   //             context, Routes.routeLogin);
                //                   //       },
                //                   //     ),
                //                   //   );
                //                   //   return;
                //                   // }
                //                   // wishlistItemCubit.add(widget.product);
                //                 },
                //                 child: Icon(Iconsax.heart)),
              ],
            )
          ],
        ),
      ),
    );
  }
}
