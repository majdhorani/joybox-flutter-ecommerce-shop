import 'package:flutter/material.dart';
import 'package:spirit/pages/home/presentation/widgets/product_vertical.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';

class ProductsVertical extends StatefulWidget {
  String title;
  ProductsVertical({super.key, required this.title});

  @override
  State<ProductsVertical> createState() => _ProductsVerticalState();
}

class _ProductsVerticalState extends State<ProductsVertical> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
      Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Text(
                widget.title.toUpperCase(),
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.black.withOpacity(0.8),
                    fontFamily: "Oswald"),
                maxLines: 1,
              ),
            ),
            SizedBox(height: 8),
            LayoutBuilder(builder: (context, constraints) {
              var columns = constraints.maxWidth ~/ 173;
              return GridView.builder(
                  itemCount: 3,
                  physics: const NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: columns,
                      crossAxisSpacing: 16,
                      mainAxisSpacing: 0,
                      childAspectRatio: (0.53)),
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  shrinkWrap: true,
                  itemBuilder: (context, index) => SProductVertical(
                      product: Product(
                          id: 2,
                          name:
                              'Apple IPHone 15 Pro Max, 256GB Natural Titanium',
                          image: '',
                          description: '',
                          salePrice: 10,
                          regularPrice: 80,
                          quantity: 2,
                          images: [],
                          isAvailable: true, 
                          categories: [], 
                          similar: [], 
                          attributes: [], barcode: '')));
            }),
    ]);
  }
}