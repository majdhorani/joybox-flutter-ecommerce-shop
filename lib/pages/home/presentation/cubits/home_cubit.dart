import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/pages/home/domain/usecases/best_sellers_usecase.dart';
import 'package:spirit/pages/home/domain/usecases/flash_sales_usecase.dart';
import '../../../../core/usecases/usecase.dart';
import '../../domain/usecases/recently_added_usecase.dart';
import 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  RecentlyAddedUseCase recentlyAddedUseCase;
  BestSellersUseCase bestSellersUseCase;
  FlashSalesUseCase flashSalesUseCase;
  HomeCubit({
    required this.recentlyAddedUseCase,
    required this.bestSellersUseCase,
    required this.flashSalesUseCase
  }) : super(HomeLoadingState());

  void flashSales() async {
    // Loading
    emit(HomeLoadingState());

    // Fetch products
    var response = await flashSalesUseCase.call(NoParams());
    response.fold((failure) {
      print("best sellers failure");
      print(failure);
      if (failure is ServerFailure) {
        emit(HomeFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(HomeFailedState(message: 'Something went wrong'));
    }, (response) {
      print("best sellers success");
      emit(HomeLoadedState(products: response));
    });
  }

  void bestSellers() async {
    // Loading
    emit(HomeLoadingState());

    // Fetch products
    var response = await bestSellersUseCase.call(NoParams());
    response.fold((failure) {
      print("best sellers failure");
      print(failure);
      if (failure is ServerFailure) {
        emit(HomeFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(HomeFailedState(message: 'Something went wrong'));
    }, (response) {
      print("best sellers success");
      emit(HomeLoadedState(products: response));
    });
  }

  void recentlyAdded() async {
    // Loading
    emit(HomeLoadingState());

    // Fetch products
    var response = await recentlyAddedUseCase.call(NoParams());
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(HomeFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(HomeFailedState(message: 'Something went wrong'));
    }, (response) {
      emit(HomeLoadedState(products: response));
    });
  }
}