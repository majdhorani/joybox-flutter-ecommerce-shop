import 'package:equatable/equatable.dart';
import '../../domain/entities/slider.dart';

abstract class SlidersState extends Equatable {
  const SlidersState();

  @override
  List<Object> get props => [];
}

class SlidersLoadingState extends SlidersState {}

class SlidersLoadedState extends SlidersState {
  List<Slider> sliders;
  SlidersLoadedState({required this.sliders});
}

class SlidersFailedState extends SlidersState {
  String message;
  SlidersFailedState({required this.message});
}

