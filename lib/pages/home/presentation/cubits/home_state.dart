import 'package:equatable/equatable.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';

abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object> get props => [];
}

class HomeLoadingState extends HomeState {}

class HomeLoadedState extends HomeState {
  List<Product> products;
  HomeLoadedState({required this.products});
}

class HomeFailedState extends HomeState {
  String message;
  HomeFailedState({required this.message});
}
