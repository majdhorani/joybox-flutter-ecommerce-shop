import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../domain/usecases/sliders_usecase.dart';
import 'sliders_state.dart';

class SlidersCubit extends Cubit<SlidersState> {
  SlidersUseCase sliderUseCase;
  SlidersCubit({
    required this.sliderUseCase
  }) : super(SlidersLoadingState());

  void sliders() async {
    // Loading
    emit(SlidersLoadingState());

    // Fetch products
    var response = await sliderUseCase.call(NoParams());
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(SlidersFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(SlidersFailedState(message: 'Something went wrong'));
    }, (response) {
      emit(SlidersLoadedState(sliders: response));
    });
  }
}
