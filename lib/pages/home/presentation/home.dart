import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spirit/core/widgets/app_bar.dart';
import 'package:spirit/core/widgets/images_slider.dart';
import 'package:spirit/pages/home/presentation/widgets/banner.dart';
import 'package:spirit/pages/home/presentation/widgets/best_sellers.dart';
import 'package:spirit/pages/home/presentation/widgets/flash_sales.dart';
import 'package:spirit/pages/home/presentation/widgets/recently_added.dart';
import 'package:spirit/pages/home/presentation/widgets/products.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: Material(
        child: Column(
          children: [
            const HomeBar(),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 14),
                    ImagesSlider(),
                    // const SBanner(),
                    const SizedBox(height: 16),
                    const FlashSales(),
                    const SizedBox(height: 16),
                    const BestSellers(),
                    const SizedBox(height: 16),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: AspectRatio(
                        aspectRatio: 4,
                        child: Container(color: Colors.grey.withOpacity(0.2)),
                      ),
                    ),
                    const SizedBox(height: 16),
                    const RecentlyAdded(),
                    const SizedBox(height: 16),
                    ProductsVertical(title: 'recently_viewed'.tr(),)
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}