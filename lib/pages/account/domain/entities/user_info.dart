class UserInfo {
  String? firstName;
  String? lastName;
  String? displayName;

  UserInfo({
    this.firstName,
    this.lastName,
    this.displayName,
  });

  UserInfo.fromJson(Map<String, dynamic> json) {
    firstName = json['first_name'];
    lastName = json['last_name'];
    displayName = json['display_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['first_name'] = firstName;
    data['last_name'] = lastName;
    data['display_name'] = displayName;
    return data;
  }
}
