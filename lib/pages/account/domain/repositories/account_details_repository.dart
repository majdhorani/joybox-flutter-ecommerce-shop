import 'package:dartz/dartz.dart';
import 'package:spirit/core/usecases/usecase.dart';
import '../../../../core/errors/failures.dart';
import '../entities/user_info.dart';
abstract class AccountDetailsRepository {
  Future<Either<Failure, UserInfo>> userInfo({required NoParams params});
  Future<Either<Failure, bool>> editUserInfo({required String? firstName, required String lastName, required String displayName});
  Future<UserInfo?> cachedUserInfo();
}
