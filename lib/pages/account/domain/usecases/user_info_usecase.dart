import 'package:dartz/dartz.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';
import '../entities/user_info.dart';
import '../repositories/account_details_repository.dart';

class UserInfoUseCase extends UseCase<UserInfo, NoParams> {
  AccountDetailsRepository repository;
  UserInfoUseCase({required this.repository});

  @override
  Future<Either<Failure, UserInfo>> call(NoParams params) {
    return repository.userInfo(params: params);
  }
}


