import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';
import '../repositories/account_details_repository.dart';

class EditUserInfoUseCase extends UseCase<bool, EditUserInfoParams> {
  AccountDetailsRepository repository;
  EditUserInfoUseCase({required this.repository});

  @override
  Future<Either<Failure, bool>> call(EditUserInfoParams params) {
    return repository.editUserInfo(firstName: params.firstName, lastName: params.lastName, displayName: params.displayName);
  }
}

class EditUserInfoParams extends Equatable {
  String firstName;
  String lastName;
  String displayName;

  EditUserInfoParams({
    required this.firstName,
    required this.lastName,
    required this.displayName,
  }) : super();

  @override
  List<Object?> get props => [firstName, lastName, displayName];
}


