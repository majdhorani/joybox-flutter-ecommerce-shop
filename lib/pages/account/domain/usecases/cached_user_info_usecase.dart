import 'package:spirit/core/usecases/usecase.dart';
import '../entities/user_info.dart';
import '../repositories/account_details_repository.dart';

class CachedUserInfoUseCase {
  AccountDetailsRepository repository;
  CachedUserInfoUseCase({required this.repository});

  Future<UserInfo?> call(NoParams params) {
    return repository.cachedUserInfo();
  }
}