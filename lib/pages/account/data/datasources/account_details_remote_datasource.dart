import 'dart:convert';

import 'package:dio/dio.dart';
import '../../../../core/errors/exceptions.dart';
import '../../../../core/server/configurations.dart';
import '../../../../core/session/session.dart';
import '../../domain/entities/user_info.dart';

abstract class AccountDetailsRemoteDataSource {
  Future<UserInfo> userInfo();
  Future<bool> editUserInfo({required String? firstName, required String lastName, required String displayName});
}

class AccountDetailsRemoteDataSourceImpl implements AccountDetailsRemoteDataSource {
  Dio client;
  AccountDetailsRemoteDataSourceImpl({required this.client});

  @override
  Future<UserInfo> userInfo() async {
    try {
      var token = Session.token();
      var response = await client.get(
          ServerConfigurations.userInfo,
          options: Options(
            headers: {"token": token},
          ),
      );
      print('User Info Response');
      print(response.data);
      return UserInfo.fromJson(response.data);
    } catch (e) {
      if (e is DioError) {
        // Status Code
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }

  @override
  Future<bool> editUserInfo({required String? firstName, required String lastName, required String displayName}) async {
    try {
      var token = Session.token();
      var response = await client.post(
        ServerConfigurations.userInfo,
        options: Options(
          headers: {"token": token},
        ),
        data: jsonEncode({'first_name': firstName, 'last_name': lastName, 'display_name': displayName})
      );
      print('User Info Response');
      print(response.data);
      return response.data['data'] == true;
    } catch (e) {
      if (e is DioError) {
        // Status Code
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }

}