
import '../../../../core/session/session.dart';
import '../../domain/entities/user_info.dart';

abstract class AccountDetailsLocalDataSource {
  Future<bool> cacheUserInfo(UserInfo info);
  Future<UserInfo?> userInfo();
}

class AccountDetailsLocalDataSourceImpl implements AccountDetailsLocalDataSource {
  @override
  Future<bool> cacheUserInfo(UserInfo info) {
    return Session.cacheUserInfo(info);
  }

  @override
  Future<UserInfo?> userInfo() async {
    return Session.userInfo();
  }
}