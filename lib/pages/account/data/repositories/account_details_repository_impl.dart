import 'package:dartz/dartz.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/network/network_info.dart';
import 'package:spirit/core/usecases/usecase.dart';
import 'package:spirit/pages/account/domain/entities/user_info.dart';
import '../../../../core/errors/exceptions.dart';
import '../../domain/repositories/account_details_repository.dart';
import '../datasources/account_details_local_datasource.dart';
import '../datasources/account_details_remote_datasource.dart';

class AccountDetailsRepositoryImpl implements AccountDetailsRepository {
  AccountDetailsRemoteDataSource remoteDataSource;
  AccountDetailsLocalDataSource localDataSource;
  NetworkInfo networkInfo;
  AccountDetailsRepositoryImpl(
      {required this.remoteDataSource,
        required this.localDataSource,
        required this.networkInfo});

  @override
  Future<Either<Failure, UserInfo>> userInfo({required NoParams params}) async {
    UserInfo? userInfo = await localDataSource.userInfo();
    if(userInfo != null) {
      return Right(userInfo);
    }

    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.userInfo();
        await localDataSource.cacheUserInfo(response);
        return Right(response);
      } catch (e) {
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }

  @override
  Future<UserInfo?> cachedUserInfo() async {
    return await localDataSource.userInfo();
  }

  @override
  Future<Either<Failure, bool>> editUserInfo({required String? firstName, required String lastName, required String displayName}) async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.editUserInfo(firstName: firstName, lastName: lastName, displayName: displayName);
        await localDataSource.cacheUserInfo(UserInfo(firstName: firstName, lastName: lastName, displayName: displayName));
        return Right(response);
      } catch (e) {
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }
}
