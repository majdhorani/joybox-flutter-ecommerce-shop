import 'package:equatable/equatable.dart';

import '../../domain/entities/user_info.dart';

abstract class UserInfoState extends Equatable {
  const UserInfoState();

  @override
  List<Object> get props => [];
}

class UserInfoLoadingState extends UserInfoState {}

class UserInfoLoadedState extends UserInfoState {
  UserInfo? userInfo;
  UserInfoLoadedState({this.userInfo});
}

class UserInfoFailedState extends UserInfoState {
  String message;
  UserInfoFailedState({required this.message});
}
