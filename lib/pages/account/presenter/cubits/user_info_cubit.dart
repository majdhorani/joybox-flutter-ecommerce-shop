import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../domain/usecases/edit_user_info_usecase.dart';
import '../../domain/usecases/user_info_usecase.dart';
import 'user_info_state.dart';

class UserInfoCubit extends Cubit<UserInfoState> {
  UserInfoUseCase userInfoUseCase;
  EditUserInfoUseCase editUserInfoUseCase;
  UserInfoCubit({
    required this.userInfoUseCase,
    required this.editUserInfoUseCase
  }) : super(UserInfoLoadingState());

  void editUserInfo(EditUserInfoParams params) async {
    // Loading
    emit(UserInfoLoadingState());

    // Fetch user info
    var response = await editUserInfoUseCase.call(params);
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(UserInfoFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(UserInfoFailedState(message: 'Something went wrong'));
    }, (response) {
      emit(UserInfoLoadedState());
    });
  }

  void userInfo() async {
    // Loading
    emit(UserInfoLoadingState());

    // Fetch user info
    var response = await userInfoUseCase.call(NoParams());
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(UserInfoFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(UserInfoFailedState(message: 'Something went wrong'));
    }, (response) {
      emit(UserInfoLoadedState(userInfo: response));
    });
  }
}
