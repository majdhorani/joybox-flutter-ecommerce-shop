import 'package:flutter/material.dart';
import 'package:iconsax/iconsax.dart';
import 'package:spirit/routes/routes.dart';

class SOptions extends StatelessWidget {
  SOptions({super.key});

  List<Map<String, dynamic>> options = [
    {"icon": Iconsax.info_circle, "title": "How It Works", "route": ""},
    {"icon": Iconsax.user, "title": "Personal Info", "route": ""},
    {
      "icon": Iconsax.people,
      "title": "Recipients",
      "route": Routes.routeRecipients
    },
    {
      "icon": Iconsax.location,
      "title": "Addresses",
      "route": Routes.routeAddresses
    },
    {"icon": Iconsax.heart, "title": "Wishlist", "route": Routes.routeWishlist},
    // {"icon": Iconsax.shopping_bag, "title": "Cart", "route": Routes.routeCart},
    // {"icon": Iconsax.gift, "title": "Orders", "route": Routes.routeOrders},
    {
      "icon": Iconsax.notification,
      "title": "Notifications",
      "route": Routes.routeNotifications
    },
    {
      "icon": Iconsax.language_circle,
      "title": "Language",
      "route": Routes.routeLanguage
    },
    {
      "icon": Iconsax.message,
      "title": "Contact Us",
      "route": Routes.routeContactUs
    },
    {"icon": Iconsax.logout, "title": "Logout", "route": Routes.routeLogin},
  ];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: ListView.separated(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          var option = options[index];
          return InkWell(
            onTap: () {
              var route = option["route"];
              Navigator.pushNamed(context, route);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Icon(option["icon"], size: 18),
                    const SizedBox(width: 8),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 2),
                      child: Text(option["title"] ?? ""),
                    ),
                  ],
                ),
                const Icon(Icons.keyboard_arrow_right, size: 20),
              ],
            ),
          );
        },
        separatorBuilder: (context, index) {
          return Divider(
            color: Colors.grey.withOpacity(0.3),
          );
        },
        itemCount: options.length,
      ),
    );
  }
}
