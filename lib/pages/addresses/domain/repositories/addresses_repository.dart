import 'package:dartz/dartz.dart';
import 'package:spirit/core/usecases/usecase.dart';
import 'package:spirit/pages/addresses/domain/usecases/add_address_usecase.dart';
import '../../../../core/errors/failures.dart';
import '../../../../core/server/responses/addresses_response.dart';
import '../usecases/edit_address_usecase.dart';

abstract class AddressesRepository {
  Future<Either<Failure, AddressesResponse>> addresses({required NoParams params});
  Future<Either<Failure, bool>> editAddress({required EditAddressParams params});
  Future<Either<Failure, bool>> addAddress({required AddAddressParams params});
}
