import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';
import 'package:spirit/pages/addresses/domain/entities/address.dart';
import '../repositories/addresses_repository.dart';

class AddAddressUseCase extends UseCase<bool, AddAddressParams> {
  AddressesRepository repository;
  AddAddressUseCase({required this.repository});

  @override
  Future<Either<Failure, bool>> call(AddAddressParams params) {
    return repository.addAddress(params: params);
  }
}

class AddAddressParams extends Equatable {
  Address address;

  AddAddressParams({
    required this.address,
  }) : super();

  Map<String, dynamic> toJson() {
    return address.toJson();
  }

  @override
  List<Object?> get props => [address];
}
