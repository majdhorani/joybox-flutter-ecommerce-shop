import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';
import 'package:spirit/pages/addresses/domain/entities/address.dart';
import '../repositories/addresses_repository.dart';

class EditAddressUseCase extends UseCase<bool, EditAddressParams> {
  AddressesRepository repository;
  EditAddressUseCase({required this.repository});

  @override
  Future<Either<Failure, bool>> call(EditAddressParams params) {
    return repository.editAddress(params: params);
  }
}

class EditAddressParams extends Equatable {
  Address address;

  EditAddressParams({
    required this.address,
  }) : super();

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['address'] = address.toJson();
    return data;
  }

  @override
  List<Object?> get props => [address];
}
