import 'package:dartz/dartz.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/server/responses/addresses_response.dart';
import 'package:spirit/core/usecases/usecase.dart';
import '../repositories/addresses_repository.dart';

class AddressesUseCase extends UseCase<AddressesResponse, NoParams> {
  AddressesRepository repository;
  AddressesUseCase({required this.repository});

  @override
  Future<Either<Failure, AddressesResponse>> call(NoParams params) {
    return repository.addresses(params: params);
  }
}


