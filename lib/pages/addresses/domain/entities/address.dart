class Address {
  String? state;
  String? district;
  String? address;
  String? building;
  String? floor;
  String? apartment;
  String? latitude;
  String? longitude;

  Address(
      {this.state,
      this.district,
      this.address,
      this.building,
      this.floor,
      this.apartment,
      this.latitude,
      this.longitude});

  Address.fromJson(Map<String, dynamic> json) {
    state = json['state'];
    district = json['district'];
    address = json['address'];
    building = json['building'];
    floor = json['floor'];
    apartment = json['apartment'];
    latitude = json['latitude'];
    longitude = json['longitude'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['state'] = state;
    data['district'] = district;
    data['address'] = address;
    data['building'] = building;
    data['floor'] = floor;
    data['apartment'] = apartment;
    if (latitude != null && longitude != null) {
      data['latitude'] = latitude;
      data['longitude'] = longitude;
    }
    return data;
  }
}
