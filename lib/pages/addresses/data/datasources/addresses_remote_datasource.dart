import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:spirit/pages/addresses/domain/usecases/add_address_usecase.dart';
import 'package:spirit/pages/addresses/domain/usecases/edit_address_usecase.dart';
import '../../../../../core/errors/exceptions.dart';
import '../../../../core/server/responses/addresses_response.dart';
import 'dart:developer';

abstract class AddressesRemoteDataSource {
  Future<AddressesResponse> addresses();
  Future<bool> editAddress({required EditAddressParams params});
  Future<bool> addAddress({required AddAddressParams params});
} 

class AddressesRemoteDataSourceImpl implements AddressesRemoteDataSource {
  Dio client;
  AddressesRemoteDataSourceImpl({required this.client});

  @override
  Future<AddressesResponse> addresses() async {
    try {
      String jsonString = await rootBundle.loadString('assets/json/addresses.json');
      Map<String, dynamic> jsonData = jsonDecode(jsonString);
      return AddressesResponse.fromJson(jsonData);
    } catch (e) {
      print('error while parsing addresses json $e');
      if (e is DioError) {
        // Status Code
        print('error in checkout');
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }

  @override
  Future<bool> editAddress({required EditAddressParams params}) async {
    try {
      return Future.value(true);
    } catch (e) {
      print(e);
      if (e is DioError) {
        // Status Code
        print('error in checkout');
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }
  
  @override
  Future<bool> addAddress({required AddAddressParams params}) async {
    try {
      return Future.value(true);
    } catch (e) {
      log(e.toString());
      if (e is DioError) {
        // Status Code
        print('error in add address');
        log(e.response.toString());
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }
}
