import 'package:dartz/dartz.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/network/network_info.dart';
import 'package:spirit/core/server/responses/addresses_response.dart';
import 'package:spirit/core/usecases/usecase.dart';
import 'package:spirit/pages/addresses/domain/usecases/add_address_usecase.dart';
import 'package:spirit/pages/addresses/domain/usecases/edit_address_usecase.dart';
import '../../../../core/errors/exceptions.dart';
import '../../domain/repositories/addresses_repository.dart';
import '../datasources/addresses_local_datasource.dart';
import '../datasources/addresses_remote_datasource.dart';

class AddressesRepositoryImpl implements AddressesRepository {
  AddressesRemoteDataSource remoteDataSource;
  AddressesLocalDataSource localDataSource;
  NetworkInfo networkInfo;
  AddressesRepositoryImpl(
      {required this.remoteDataSource,
        required this.localDataSource,
        required this.networkInfo});

  @override
  Future<Either<Failure, AddressesResponse>> addresses({required NoParams params}) async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.addresses();
        // if(response.billingAddress != null) {
        //   await localDataSource.cacheBillingAddress(response.billingAddress!);
        // }

        // if(response.shippingAddress != null) {
        //   await localDataSource.cacheShippingAddress(response.shippingAddress!);
        // }
        return Right(response);
      } catch (e) {
        if (e is ServerException) { 
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> editAddress({required EditAddressParams params}) async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.editAddress(params: params);
        // await localDataSource.cacheBillingAddress(params.billingAddress);
        // await localDataSource.cacheShippingAddress(params.shippingAddress);
        return Right(response);
      } catch (e) {
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }  

  @override
  Future<Either<Failure, bool>> addAddress({required AddAddressParams params}) async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.addAddress(params: params);
        return Right(response);
      } catch (e) {
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }
}
