import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/extensions/text_editing_controller_extensions.dart';
import 'package:spirit/core/widgets/button.dart';
import 'package:spirit/core/widgets/general_app_bar.dart';
import 'package:spirit/core/widgets/loading.dart';
import 'package:spirit/core/widgets/textfield.dart';
import 'package:spirit/di.dart';
import 'package:spirit/pages/addresses/domain/entities/address.dart';
import 'package:spirit/pages/addresses/domain/usecases/add_address_usecase.dart';
import 'package:spirit/pages/addresses/presenter/cubits/add_address_cubit.dart';
import 'package:spirit/pages/addresses/presenter/cubits/add_address_state.dart';

class AddressPage extends StatefulWidget {
  Address? address;
  AddressPage({super.key, this.address});

  @override
  State<AddressPage> createState() => _AddressPageState();
}

class _AddressPageState extends State<AddressPage> {
  AddAddressCubit addAddressCubit = sl<AddAddressCubit>();
  // Controllers
  var cityController = TextEditingController();
  var addressController = TextEditingController();
  var districtController = TextEditingController();
  var buildingController = TextEditingController();
  var floorController = TextEditingController();
  var apartmentController = TextEditingController();

  String? cityError;
  String? addressError;
  String? districtError;
  String? buildingError;
  String? floorError;
  String? apartmentError;

  @override
  void initState() {
    super.initState();
  }

  void validate() {
    setState(() {
      cityError = null;
      addressError = null;
      districtError = null;
      buildingError = null;
      floorError = null;
      apartmentError = null;

      if (cityController.isEmpty()) {
        cityError = "field_should_not_be_empty".tr();
      }

      if (addressController.isEmpty()) {
        addressError = "field_should_not_be_empty".tr();
      }

      if (districtController.isEmpty()) {
        districtError = "field_should_not_be_empty".tr();
      }

      if (buildingController.isEmpty()) {
        buildingError = "field_should_not_be_empty".tr();
      }

      if (floorController.isEmpty()) {
        floorError = "field_should_not_be_empty".tr();
      }

      if (apartmentController.isEmpty()) {
        apartmentError = "field_should_not_be_empty".tr();
      }
    });
  }

  bool isValid() {
    return (
      cityError == null && 
      addressError == null && 
      districtError == null && 
      buildingError == null && 
      floorError == null && 
      apartmentError == null
    );
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: Material(
        child: SafeArea(
          child: Column(
            children: [
              SGeneralAppBar(
                title: "add_address".tr(),
                showBackButton: true,
              ),
              BlocConsumer<AddAddressCubit, AddAddressState>(
                  bloc: addAddressCubit,
                  listener: (BuildContext context, AddAddressState state) {  
                    if(state is AddAddressSuccessState) {
                      Navigator.pop(context, true);
                    }
                  },
                  builder: (context, state) {
                    if (state is AddAddressLoadingState) {
                      return HLoading(isWhite: true);
                    }

                    return Expanded(
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 24),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(
                                height: 16,
                              ),
                              Text(
                                'Add address that you will use to send orders to.'
                                    .tr(),
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.black.withOpacity(0.6),
                                    fontFamily: "Oswald"),
                              ),
                              const SizedBox(height: 16),
                              // if (state is RegisterFailedState)
                              //   ErrorMessage(
                              //       message: state.message ??
                              //           'could_not_register_please_try_again'
                              //               .tr()),
                              // 	id	user_id	state	district	address	building	floor	apartment	latitude	longitude

                              HTextField(
                                title: 'city'.tr(),
                                controller: cityController,
                                error: cityError,
                              ),
                              const SizedBox(height: 14),
                              HTextField(
                                title: 'district'.tr(),
                                controller: districtController,
                                error: districtError,
                              ),
                              const SizedBox(height: 14),
                              HTextField(
                                title: 'address'.tr(),
                                controller: addressController,
                                error: addressError,
                              ),
                              const SizedBox(height: 14),
                              HTextField(
                                title: 'building'.tr(),
                                controller: buildingController,
                                error: buildingError,
                              ),
                              const SizedBox(height: 14),
                              HTextField(
                                title: 'floor'.tr(),
                                controller: floorController,
                                error: floorError,
                              ),
                              const SizedBox(height: 14),
                              HTextField(
                                title: 'apartment'.tr(),
                                controller: apartmentController,
                                error: apartmentError
                              ),
                              Divider(
                                  height: 38,
                                  color: Colors.grey.withOpacity(0.2)),
                              HButton(
                                  title: 'save'.tr().toUpperCase(),
                                  backgroundColor: Colors.black,
                                  rounded: false,
                                  onPressed: () {
                                    validate();

                                    if(!isValid()) {
                                      return;
                                    }

                                    var params = AddAddressParams(
                                        address: Address(
                                            address: addressController.text,
                                            apartment: apartmentController.text,
                                            building: buildingController.text,
                                            district: districtController.text,
                                            floor: floorController.text,
                                            state: cityController.text));

                                            // Submit details to server
                                    addAddressCubit.add(params);
                                  }),
                              const SizedBox(height: 16),
                            ],
                          ),
                        ),
                      ),
                    );
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
