import 'package:equatable/equatable.dart';

abstract class AddAddressState extends Equatable {
  const AddAddressState();

  @override
  List<Object> get props => [];
}

class AddAddressInitialState extends AddAddressState {}

class AddAddressLoadingState extends AddAddressState {}

class AddAddressSuccessState extends AddAddressState {
  const AddAddressSuccessState();
}

class AddAddressFailedState extends AddAddressState {
  String message;
  AddAddressFailedState({required this.message});
}
