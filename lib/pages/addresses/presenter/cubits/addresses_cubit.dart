import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../domain/usecases/addresses_usecase.dart';
import 'addresses_state.dart';

class AddressesCubit extends Cubit<AddressesState> {
  AddressesUseCase addressesUseCase;
  AddressesCubit({
    required this.addressesUseCase
  }) : super(AddressesLoadingState());

  void addresses() async {
    // Loading
    emit(AddressesLoadingState());

    // Fetch addresses
    var response = await addressesUseCase.call(NoParams());
    response.fold((failure) {
      print('addresses state error');
      print(failure);
      if (failure is ServerFailure) {
        emit(AddressesFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(AddressesFailedState(message: 'Something went wrong'));
    }, (response) {
      emit(AddressesLoadedState(addresses: response.data));
    });
  }
}
