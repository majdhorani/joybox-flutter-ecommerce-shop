import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import '../../domain/usecases/edit_address_usecase.dart';
import 'edit_address_state.dart';

class EditAddressCubit extends Cubit<EditAddressState> {
  EditAddressUseCase editAddressUseCase;

  EditAddressCubit({required this.editAddressUseCase})
      : super(EditAddressInitialState());

  void edit(EditAddressParams params) async {
    // Loading
    emit(EditAddressLoadingState());

    // Fetch products
    var response = await editAddressUseCase.call(params);
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(EditAddressFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(EditAddressFailedState(message: 'Something went wrong'));
    }, (response) {
      emit(const EditAddressSuccessState());
    });
  }
}
