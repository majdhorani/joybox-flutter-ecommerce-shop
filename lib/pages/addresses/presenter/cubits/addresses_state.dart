import 'package:equatable/equatable.dart';
import '../../domain/entities/address.dart';

abstract class AddressesState extends Equatable {
  const AddressesState();

  @override
  List<Object> get props => [];
}

class AddressesLoadingState extends AddressesState {}

class AddressesLoadedState extends AddressesState {
  List<Address> addresses;
  AddressesLoadedState({required this.addresses});
}

class AddressesFailedState extends AddressesState {
  String message;
  AddressesFailedState({required this.message});
}
