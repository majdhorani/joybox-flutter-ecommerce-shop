import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/pages/addresses/domain/usecases/add_address_usecase.dart';
import 'package:spirit/pages/addresses/presenter/cubits/add_address_state.dart';

class AddAddressCubit extends Cubit<AddAddressState> {
  AddAddressUseCase addAddressUseCase;

  AddAddressCubit({required this.addAddressUseCase})
      : super(AddAddressInitialState());

  void add(AddAddressParams params) async {
    // Loading
    emit(AddAddressLoadingState());

    // Fetch products
    var response = await addAddressUseCase.call(params);
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(AddAddressFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(AddAddressFailedState(message: 'Something went wrong'));
    }, (response) {
      emit(const AddAddressSuccessState());
    });
  }
}
