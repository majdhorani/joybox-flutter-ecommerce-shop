import 'package:equatable/equatable.dart';

abstract class EditAddressState extends Equatable {
  const EditAddressState();

  @override
  List<Object> get props => [];
}

class EditAddressInitialState extends EditAddressState {}

class EditAddressLoadingState extends EditAddressState {}

class EditAddressSuccessState extends EditAddressState {
  const EditAddressSuccessState();
}

class EditAddressFailedState extends EditAddressState {
  String message;
  EditAddressFailedState({required this.message});
}
