import 'package:flutter/material.dart';
import 'package:spirit/pages/addresses/domain/entities/address.dart';
import 'package:spirit/pages/addresses/presenter/widgets/address_item.dart';

class AddressesList extends StatelessWidget {
  List<Address> addresses;
  Function(Address address) onTap;
  AddressesList({super.key, required this.addresses, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: ListView.separated(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        separatorBuilder: (context, index) {
          Address address = addresses[index];
          return AddressItem(
            address: address, 
            onTap: onTap);
        },
        itemBuilder: (context, index) {
          if (index == 0) {
            return const SizedBox();
          }
          return Divider(
            color: Colors.grey.withOpacity(0.3),
          );
        },
        itemCount: addresses.length + 1,
      ),
    );
  }
}
