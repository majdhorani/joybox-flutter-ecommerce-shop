import 'package:flutter/material.dart';
import 'package:iconsax/iconsax.dart';
import 'package:spirit/pages/addresses/domain/entities/address.dart';

class AddressItem extends StatefulWidget {
  Address address;
  Function(Address address) onTap;
  AddressItem({super.key, required this.address, required this.onTap});

  @override
  State<AddressItem> createState() => _AddressItemState();
}

class _AddressItemState extends State<AddressItem> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
            onTap: () {
              widget.onTap(widget.address);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 2),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // const Text("Country: ${"Syria"}",
                      //     style: TextStyle(
                      //         color: Colors.black,
                      //         fontSize: 14,
                      //         fontWeight: FontWeight.w400)),
                      Text("City: ${widget.address.state?.trim()}",
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w400)),
                      Text("District: ${widget.address.district?.trim()}",
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w400)),
                      Text("Address: ${widget.address.address?.trim()}",
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w400)),
                      Text("Building: ${widget.address.building?.trim()}",
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w400)),
                      Text("Apartment: ${widget.address.apartment?.trim()}",
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w400)),
                      Text("Floor: ${widget.address.floor?.trim()}",
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w400))
                    ],
                  ),
                ),
                Row(
                  children: [
                    Material(
                      color: Colors.black,
                      child: Padding(
                        padding: const EdgeInsets.all(6),
                        child: InkWell(
                          onTap: () {

                          },
                          child: const Icon(Iconsax.edit,
                              size: 17, color: Colors.white),
                        ),
                      ),
                    ),
                    const SizedBox(width: 8),
                    Material(
                      color: Colors.red,
                      child: Padding(
                        padding: const EdgeInsets.all(6),
                        child: InkWell(
                          onTap: () {

                          },
                          child: const Icon(Iconsax.trash,
                              size: 17, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          );
  }
}