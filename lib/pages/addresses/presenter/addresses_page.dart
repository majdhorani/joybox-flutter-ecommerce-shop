import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:iconsax/iconsax.dart';
import 'package:spirit/core/widgets/button.dart';
import 'package:spirit/core/widgets/general_app_bar.dart';
import 'package:spirit/core/widgets/loading.dart';
import 'package:spirit/di.dart';
import 'package:spirit/pages/addresses/presenter/cubits/addresses_cubit.dart';
import 'package:spirit/pages/addresses/presenter/cubits/addresses_state.dart';
import 'package:spirit/pages/addresses/presenter/widgets/addresses_list.dart';
import 'package:spirit/pages/addresses/presenter/widgets/addresses_empty.dart';
import 'package:spirit/routes/routes.dart';

class AddressesPage extends StatefulWidget {
  // Selection true means, select item and return it back to the previous screen.
  bool selection;
  AddressesPage({super.key, required this.selection});

  @override
  State<AddressesPage> createState() => _AddressesPageState();
}

class _AddressesPageState extends State<AddressesPage> {
  AddressesCubit addressesCubit = sl<AddressesCubit>();

  @override
  void initState() {
    addressesCubit.addresses();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: Material(
        child: SafeArea(
          child: Column(
            children: [
              SGeneralAppBar(
                title: "addresses".tr(),
                showBackButton: true,
              ),
              BlocBuilder<AddressesCubit, AddressesState>(
                  bloc: addressesCubit,
                  builder: (context, state) {
                    if (state is AddressesLoadingState) {
                      return HLoading(isWhite: true);
                    }

                    if (state is AddressesLoadedState) {
                      if (state.addresses.isEmpty) {
                        return AddressesEmpty(onAdd: () async {
                          var result = await Navigator.pushNamed(
                              context, Routes.routeAddAddress);
                          if (result != null) {
                            if (result is bool && result) {
                              addressesCubit.addresses();
                            }
                          }
                        });
                      }
                      return Expanded(
                        child: Column(
                          children: [
                            Expanded(
                              child: SingleChildScrollView(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      const SizedBox(
                                        height: 8,
                                      ),
                                      AddressesList(
                                        addresses: state.addresses, 
                                        onTap: (address) {  
                                          if(widget.selection) {
                                            Navigator.pop(context, address);
                                          }
                                        }),
                                      const SizedBox(height: 16),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            const Divider(height: 1),
                            Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 16, vertical: 16),
                              child: SizedBox(
                                child: HButton(
                                    backgroundColor: Colors.black,
                                    iconData: Iconsax.location_add,
                                    iconColor: Colors.white,
                                    title: "add_address".tr().toUpperCase(),
                                    onPressed: () async {
                                      var result = await Navigator.pushNamed(
                                          context, Routes.routeAddAddress);
                                      if (result != null) {
                                        if (result is bool && result) {
                                          addressesCubit.addresses();
                                        }
                                      }
                                    }),
                              ),
                            ),
                          ],
                        ),
                      );
                    }
                    return const SizedBox();
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
