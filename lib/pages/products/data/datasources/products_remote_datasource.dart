import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:spirit/core/server/configurations.dart';
import '../../../../core/errors/exceptions.dart';
import '../../../../core/server/responses/products_response.dart';
import '../../../../core/session/session.dart';
import '../../domain/usecases/products_usecase.dart';

abstract class ProductsRemoteDataSource {
  Future<ProductsResponse> products({required ProductsParams params});
}

class ProductsRemoteDataSourceImpl implements ProductsRemoteDataSource {
  Dio client;
  ProductsRemoteDataSourceImpl({required this.client});

  @override
  Future<ProductsResponse> products({required ProductsParams params}) async {
    try {
      String jsonString = await rootBundle.loadString('assets/json/products.json');
      Map<String, dynamic> jsonData = jsonDecode(jsonString);
      var result = ProductsResponse.fromJson(jsonData);
      return result;
    } catch (e) {
      log(e.toString());
      if (e is DioError) {
        // Status Code
        log(e.response.toString());
        print(e.response?.realUri);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }
}
