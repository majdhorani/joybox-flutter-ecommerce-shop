import 'package:dartz/dartz.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/network/network_info.dart';
import 'package:spirit/core/server/responses/products_response.dart';
import '../../../../core/errors/exceptions.dart';
import '../../domain/repositories/products_repository.dart';
import '../../domain/usecases/products_usecase.dart';
import '../datasources/products_local_datasource.dart';
import '../datasources/products_remote_datasource.dart';

class ProductsRepositoryImpl implements ProductsRepository {
  ProductsRemoteDataSource remoteDataSource;
  ProductsLocalDataSource localDataSource;
  NetworkInfo networkInfo;
  ProductsRepositoryImpl({required this.remoteDataSource, required this.localDataSource, required this.networkInfo});

  @override
  Future<Either<Failure, ProductsResponse>> products({required ProductsParams params}) async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.products(params: params);
        print('LENGTHH ${response.products.length}');
        return Right(response);
      } catch (e) {
        print(e);
        if (e is ServerException) {
          return Left(ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }

}