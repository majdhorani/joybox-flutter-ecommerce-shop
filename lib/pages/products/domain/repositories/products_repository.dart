import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../../../../core/server/responses/products_response.dart';
import '../usecases/products_usecase.dart';

abstract class ProductsRepository {
  Future<Either<Failure,ProductsResponse>> products({required ProductsParams params});
}