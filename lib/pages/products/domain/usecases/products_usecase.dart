import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';
import '../../../../core/server/responses/products_response.dart';
import '../repositories/products_repository.dart';

class ProductsUseCase extends UseCase<ProductsResponse, ProductsParams> {
  ProductsRepository repository;
  ProductsUseCase({required this.repository});

  @override
  Future<Either<Failure, ProductsResponse>> call(ProductsParams params) {
    return repository.products(params: params);
  }
}

/// Whatever data required for your datasources
class ProductsParams extends Equatable {
  String? title;
  String? categoryId;
  String? name;
  List<String>? attrs;
  String? minPrice;
  String? maxPrice;
  int page;
  ProductsParams(
      {this.title,
      this.categoryId,
      this.name,
      this.attrs,
      this.minPrice,
      this.maxPrice,
      this.page = 0})
      : super();

  List<String> getAttributes() {
    return (attrs ?? []);
  }

  @override
  List<Object?> get props => [categoryId, name];
}
