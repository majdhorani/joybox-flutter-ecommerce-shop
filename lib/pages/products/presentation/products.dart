import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:spirit/core/theme/app_colors.dart';
import 'package:spirit/core/widgets/general_app_bar.dart';
import 'package:spirit/di.dart';
import 'package:spirit/pages/home/presentation/widgets/product_vertical.dart';
import 'package:spirit/pages/product/domain/entities/attribute.dart';
import 'package:spirit/pages/products/domain/usecases/products_usecase.dart';
import 'package:spirit/pages/products/presentation/cubits/products_cubit.dart';
import 'package:spirit/pages/products/presentation/cubits/products_state.dart';
import 'package:spirit/pages/products/presentation/widget/filter_dialog.dart';
import 'package:spirit/pages/products/presentation/widget/pagination.dart';

class ProductsPage extends StatefulWidget {
  ProductsParams arguments;
  ProductsPage({Key? key, required this.arguments}) : super(key: key);

  @override
  State<ProductsPage> createState() => _ProductsPageState();
}

class _ProductsPageState extends State<ProductsPage> {
  ProductsCubit productsCubit = sl<ProductsCubit>();

  // Filter
  String? searchKeyword;
  List<Attribute>? attributes;
  String? minPrice;
  String? maxPrice;
  int page = 0;

  // Search keywords
  var searchKeywords = [
    ProductsParams(title: 'sale'.tr(), minPrice: '1'),
    ProductsParams(
        title: 'aed_10_-_30_items'.tr(), minPrice: '10', maxPrice: '30'),
    ProductsParams(title: 'bundle_offer'.tr(), categoryId: '37'),
    ProductsParams(title: 'crazy_deals'.tr(), categoryId: '42'),
    ProductsParams(title: 'audio_and_bluetooth'.tr(), categoryId: '34'),
    ProductsParams(title: 'battery'.tr(), categoryId: '35'),
    ProductsParams(title: 'cases_and_covers'.tr(), categoryId: '28'),
    ProductsParams(title: 'power_bank'.tr(), categoryId: '46'),
    ProductsParams(
        title: 'screen_and_camera_protection'.tr(), categoryId: '31'),
  ];

  @override
  void initState() {
    super.initState();
    attributes = [];
    widget.arguments.attrs?.forEach((element) {
      attributes?.add(Attribute(
          attrName: "brand", valueName: element));
    });
    print("initial attrs");
    print(attributes);
    FlutterNativeSplash.remove();
    productsCubit.products(ProductsParams(
        categoryId: widget.arguments.categoryId,
        name: widget.arguments.name,
        attrs: widget.arguments.attrs,
        minPrice: widget.arguments.minPrice,
        maxPrice: widget.arguments.maxPrice,
        page: page));
    searchKeyword = widget.arguments.name;
  }

  void fetchProducts() {
    print('fetching');
    print(ProductsParams(
        categoryId: widget.arguments.categoryId,
        name: searchKeyword,
        attrs: (attributes ?? []).map((attr) => attr.valueName ?? '').toList(),
        minPrice: minPrice,
        maxPrice: maxPrice,
        page: page));
    productsCubit.products(ProductsParams(
        categoryId: widget.arguments.categoryId,
        name: searchKeyword,
        attrs: (attributes ?? []).map((attr) => attr.valueName ?? '').toList(),
        minPrice: minPrice,
        maxPrice: maxPrice,
        page: page));
  }

  Widget _buildFilters() {
    return BlocBuilder(
        bloc: productsCubit,
        builder: (context, state) {
          if (state is ProductsLoadedState) {
            return Container(
              height: 34,
              color: const Color(0xFFD9D9D9),
              margin: const EdgeInsets.symmetric(horizontal: 30),
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (_) => FiltersDialog(
                            minPrice: minPrice,
                            maxPrice: maxPrice,
                            attributes: state.getNestedAttributes(),
                            onFilter: (List<Attribute> attributes,
                                String? minPrice, String? maxPrice) {
                              print('min price: $minPrice');
                              print('min price: $maxPrice');
                              this.attributes = attributes;
                              this.minPrice = minPrice;
                              this.maxPrice = maxPrice;
                              fetchProducts();
                            }),
                      );
                    },
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          'assets/svg/filter.svg',
                          height: 16,
                        ),
                        const SizedBox(
                          width: 12,
                        ),
                        Text(
                          'filters'.tr(),
                          style: const TextStyle(
                              color: Color(0xFF343E48), fontSize: 12),
                        )
                      ],
                    ),
                  ),
                  // Row(
                  //   children: [
                  //     Text(
                  //       'relevance'.tr(),
                  //       style: const TextStyle(
                  //           color: Color(0xFF343E48), fontSize: 12),
                  //     ),
                  //     const SizedBox(
                  //       width: 12,
                  //     ),
                  //     SvgPicture.asset('assets/svg/relevance.svg'),
                  //   ],
                  // )
                ],
              ),
            );
          }
          return const SizedBox();
        });
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      backgroundColor: Colors.white,
      child: Material(
        child: SafeArea(
          child: Column(
            children: [
              SGeneralAppBar(
                showBackButton: true,
                title: widget.arguments.title ?? "products".tr(),
              ),
              // Products
              BlocConsumer(
                  bloc: productsCubit,
                  listener: (context, state) {},
                  builder: (context, state) {
                    print('products state: $state');
                    if (state is ProductsLoadingState) {
                      return Expanded(
                          child: Container(
                              color: AppColors.white,
                              child: const SizedBox(
                                  width: 30,
                                  child: SpinKitCircle(
                                    color: Colors.grey,
                                  ))));
                    }

                    if (state is ProductsLoadedState) {
                      return Expanded(
                          child: state.products.isEmpty
                              ? Center(child: Text('empty'.tr()))
                              : SingleChildScrollView(
                                  child: Column(children: [
                                  LayoutBuilder(
                                      builder: (context, constraints) {
                                    var columns = constraints.maxWidth ~/ 173;
                                    return GridView.builder(
                                        itemCount: state.products.length,
                                        physics:
                                            const NeverScrollableScrollPhysics(),
                                        gridDelegate:
                                            SliverGridDelegateWithFixedCrossAxisCount(
                                                crossAxisCount: columns,
                                                crossAxisSpacing: 15,
                                                mainAxisSpacing: 0,
                                                childAspectRatio: (0.48)),
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 16, horizontal: 32),
                                        shrinkWrap: true,
                                        itemBuilder: (context, index) =>
                                            SProductVertical(
                                                product:
                                                    state.products[index]));
                                  }),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  HPagination(
                                      pages: state.pages,
                                      page: page,
                                      totalCount: state.totalCount,
                                      onChange: (int page) {
                                        this.page = page;
                                        fetchProducts();
                                      }),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                ])));
                    }
                    return const SizedBox();
                  })
            ],
          ),
        ),
      ),
    );
  }
}
