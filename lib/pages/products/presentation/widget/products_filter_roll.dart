import 'package:flutter/material.dart';
import '../../../../core/theme/app_colors.dart';
import '../../domain/usecases/products_usecase.dart';

class HProductsFilterRoll extends StatefulWidget {
  ProductsParams filter;
  Function(ProductsParams filter) onPressed;
  HProductsFilterRoll({Key? key, required this.filter, required this.onPressed})
      : super(key: key);

  @override
  State<HProductsFilterRoll> createState() => _HProductsFilterRollState();
}

class _HProductsFilterRollState extends State<HProductsFilterRoll> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        widget.onPressed(widget.filter);
      },
      child: Container(
        height: 30,
        padding: const EdgeInsets.symmetric(horizontal: 12),
        decoration: const BoxDecoration(
            color: AppColors.red,
            borderRadius: BorderRadius.all(Radius.circular(50))),
        child: Center(
            child: Text(widget.filter.title ?? '',
                style: const TextStyle(fontSize: 12, color: AppColors.white))),
      ),
    );
  }
}
