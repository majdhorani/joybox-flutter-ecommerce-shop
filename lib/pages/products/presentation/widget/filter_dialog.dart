import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../core/theme/app_colors.dart';
import '../../../../core/widgets/button.dart';
import '../../../../core/widgets/textfield.dart';
import '../../../product/domain/entities/attribute.dart';

class FiltersDialog extends StatefulWidget {
  String? minPrice;
  String? maxPrice;
  Function(List<Attribute> attributes, String? minPrice, String? maxPrice) onFilter;
  Map<String, List<Attribute>> attributes;
  FiltersDialog({Key? key, required this.attributes, this.minPrice, this.maxPrice, required this.onFilter}) : super(key: key);

  @override
  State<FiltersDialog> createState() => _FiltersDialogState();
}

class _FiltersDialogState extends State<FiltersDialog> {
  // RangeValues _currentRangeValues = const RangeValues(40, 20000);
  TextEditingController minPriceController = TextEditingController(text: '10');
  TextEditingController maxPriceController = TextEditingController(text: '10000');
  bool maxPriceEnabled = false;
  bool minPriceEnabled = false;

  @override
  void initState() {
    if(widget.minPrice != null) {
      minPriceController.text = widget.minPrice!;
    }
    if(widget.maxPrice != null) {
      maxPriceController.text = widget.maxPrice!;
    }
    super.initState();
  }

  List<Attribute> getSelectedAttributes() {
    List<Attribute> selected = [];
    for (var attrs in widget.attributes.values) {
      for (var attr in attrs) {
        if(attr.isSelected) {
          selected.add(attr);
        }
      }
    }
    return selected;
  }

  Widget _buildAttributes(String name, List<Attribute> attributes) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
      Text(name.toUpperCase(), style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14),),
      const SizedBox(height: 10,),
      Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ...attributes.map((attr) => Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: InkWell(
                onTap: () {
                  setState(() {
                    attr.isSelected = !attr.isSelected;
                  });
                },
                child: Row(children: [
                  SizedBox(
                    width: 24,
                    height: 24,
                    child: Checkbox(
                        checkColor: AppColors.white,
                        activeColor: AppColors.red,
                        value: attr.isSelected,
                        onChanged: (value) {
                          setState(() {
                            if(value != null) {
                              attr.isSelected = value;
                            }
                          });
                        }),
                  ),
                  const SizedBox(width: 10),
                  Text(attr.valueName ?? '', style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 10),),
                  const SizedBox(width: 2),
                  Text('(${attr.productCount})', style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 10, color: AppColors.grey800),)
                ]),
              ),
            )).toList(),
      ]),
          const Divider(height: 10,)
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(32.0))),
      contentPadding: const EdgeInsets.only(top: 10.0),
      content: Container(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: Stack(
          // mainAxisSize: MainAxisSize.min,
          // mainAxisAlignment: MainAxisAlignment.start,
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 90),
              child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      if(widget.attributes.keys.isNotEmpty)
                        const SizedBox(height: 20),
                      ...widget.attributes.keys.where((key) => key == "Brands").map((key) => _buildAttributes(key, widget.attributes[key] ?? [])).toList(),

                      // Minimum Price
                      const SizedBox(height: 20),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('minimum_price'.tr(), style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14),),
                          SizedBox(
                              width: 24,
                              height: 24,
                              child: Checkbox(
                                  value: minPriceEnabled,
                                  checkColor: AppColors.white,
                                  activeColor: AppColors.red,
                                  onChanged: (value) {
                                    setState(() {
                                      minPriceEnabled = value ?? false;
                                    });
                                  }))
                        ],
                      ),
                      const SizedBox(height: 10,),
                      HTextField(
                        controller: minPriceController,
                        disabled: !minPriceEnabled,
                        type: TextInputType.number,
                      ),

                      // Maximum Price
                      const SizedBox(height: 20),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('maximum_price'.tr(), style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
                          SizedBox(
                              width: 24,
                              height: 24,
                              child: Checkbox(
                                  value: maxPriceEnabled,
                                  checkColor: AppColors.white,
                                  activeColor: AppColors.red,
                                  onChanged: (value) {
                                    setState(() {
                                      maxPriceEnabled = value ?? false;
                                    });
                                  }))
                        ],
                      ),
                      const SizedBox(height: 10),
                      HTextField(
                        controller: maxPriceController,
                        disabled: !maxPriceEnabled,
                        type: TextInputType.number
                      )
                    ],
                  )),
            ),
            Positioned(
              bottom: 20,
              left: 0,
              right: 0,
              child: HButton(
                  title: 'filter'.tr(),
                  rounded: true,
                  onPressed: () {
                    Navigator.pop(context);
                    widget.onFilter(
                        getSelectedAttributes(),
                        minPriceEnabled ? minPriceController.text : (maxPriceEnabled && !minPriceEnabled ? '0' : null),
                        maxPriceEnabled ? maxPriceController.text : null
                    );
                  }),
            ),
            const SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}
