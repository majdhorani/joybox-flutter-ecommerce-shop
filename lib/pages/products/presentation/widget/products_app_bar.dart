import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../core/theme/app_colors.dart';
import '../../../../core/widgets/rtl.dart';

class HProductsAppBar extends StatefulWidget implements PreferredSizeWidget {
  Function onSearch;
  Function onClearSearch;
  String? searchKeyword;
  HProductsAppBar({Key? key, required this.onSearch, required this.onClearSearch, required this.searchKeyword}) : super(key: key);

  @override
  State<HProductsAppBar> createState() => _HProductsAppBarState();

  @override
  Size get preferredSize => const Size.fromHeight(86);
}

class _HProductsAppBarState extends State<HProductsAppBar> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: MediaQuery.of(context).padding.top + 10),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32),
          child: Column(
            children: [
              SizedBox(
                height: 47,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      child: HRtl(
                        child: SvgPicture.asset(
                          'assets/svg/back.svg',
                          height: 23,
                        ),
                      ),
                      onTap: () {
                        Navigator.maybePop(context);
                      },
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'search_result'.tr(),
                          style: const TextStyle(fontSize: 12),
                        ),
                        if(widget.searchKeyword != null)
                          const SizedBox(height: 4),
                        if(widget.searchKeyword != null)
                          InkWell(
                            onTap: () {
                              widget.onClearSearch();
                            },
                            child: Row(
                              children: [
                                Text(
                                  '${widget.searchKeyword}',
                                  style: const TextStyle(fontSize: 12, color: AppColors.black, fontWeight: FontWeight.bold, decoration: TextDecoration.underline,),
                                ),
                                const SizedBox(width: 4),
                                const Icon(Icons.close, size: 12,)
                              ],
                            ),
                          ),
                      ],
                    ),
                    InkWell(
                        child: SvgPicture.asset(
                          'assets/svg/search.svg',
                          height: 25,
                        ),
                        onTap: () {
                          widget.onSearch();
                        }),
                  ],
                ),
              ),
              const SizedBox(
                height: 28,
              ),
              const Divider(height: 0, thickness: 1, color: Color(0xFFDADADA))
            ],
          ),
        )
      ],
    );
  }
}
