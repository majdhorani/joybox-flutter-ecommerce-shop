import 'package:flutter/material.dart';
import 'package:spirit/core/theme/app_colors.dart';

class HPagination extends StatefulWidget {
  Function(int page) onChange;
  int page;
  int pages;
  int totalCount;
  HPagination(
      {Key? key,
      required this.pages,
      required this.page,
      required this.onChange,
      required this.totalCount})
      : super(key: key);

  @override
  State<HPagination> createState() => _HPaginationState();
}

class _HPaginationState extends State<HPagination> {
  late int page;

  @override
  void initState() {
    page = widget.page;
    super.initState();
  }

  Widget _active(int page) {
    return Container(
        height: 40,
        width: 40,
        decoration: BoxDecoration(
          color: AppColors.black,
          borderRadius: const BorderRadius.all(
            Radius.circular(2),
          ),
          border: Border.all(
            width: 3,
            color: Colors.black,
            style: BorderStyle.solid,
          ),
        ),
        child: Center(
            child: Text(
          (page + 1).toString(),
          style: const TextStyle(color: Colors.white, fontSize: 12),
        )));
  }

  Widget _inactive(int page) {
    return InkWell(
      onTap: () {
        setState(() {
          this.page = page;
        });
        widget.onChange(page);
      },
      child: Container(
          height: 40,
          width: 40,
          decoration: BoxDecoration(
            color: const Color(0xFFFFFFFF),
            borderRadius: const BorderRadius.all(
              Radius.circular(2),
            ),
            border: Border.all(
              width: 1,
              color: const Color(0xFFDADADA),
              style: BorderStyle.solid,
            ),
          ),
          child: Center(
              child: Text(
            (page + 1).toString(),
            style: const TextStyle(color: Color(0xFF9A9A9A)),
          ))),
    );
  }

  @override
  Widget build(BuildContext context) {
    int totalCount = widget.totalCount;
    int startVisibleCount = ((page) * 40) + 1;
    int endVisibleCount = (((page) * 40) + 1) + 40;
    if (endVisibleCount > totalCount) {
      endVisibleCount = totalCount;
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        children: [
          const Divider(
            height: 0,
            thickness: 1,
            color: Color(0xFFDADADA),
          ),
          const SizedBox(height: 15),
          Text(
              'Showing $startVisibleCount- $endVisibleCount of ${widget.totalCount} results',
              style: const TextStyle(fontSize: 12)),
          if (widget.totalCount > 40)
            const SizedBox(
              height: 10,
            ),
          if (widget.totalCount > 40)
            Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  if (page == 0)
                    Row(
                      children: [
                        _active(page),
                        const SizedBox(width: 4),
                        _inactive(page + 1),
                        const SizedBox(width: 4),
                        _inactive(page + 2),
                      ],
                    ),
                  if (page > 0 && page < widget.pages - 1)
                    Row(
                      children: [
                        _inactive(page - 1),
                        const SizedBox(width: 4),
                        _active(page),
                        const SizedBox(width: 4),
                        _inactive(page + 1),
                      ],
                    ),
                  const SizedBox(width: 4),
                  if (page == widget.pages - 1)
                    Row(
                      children: [
                        _inactive(page - 2),
                        const SizedBox(width: 4),
                        _inactive(page + 1),
                        const SizedBox(width: 4),
                        _active(page),
                      ],
                    ),
                ]),
          const SizedBox(height: 8),
        ],
      ),
    );
  }
}
