import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import '../../domain/usecases/products_usecase.dart';
import 'products_state.dart';

class ProductsCubit extends Cubit<ProductsState> {
  ProductsUseCase productsUseCase;
  ProductsCubit({required this.productsUseCase}) : super(ProductsLoadingState());

  void products(ProductsParams params) async {
    // Loading
    emit(ProductsLoadingState());

    // Fetch products
    var response =
        await productsUseCase.call(params);
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(ProductsFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(ProductsFailedState(message: 'Something went wrong'));
    }, (response) {
      // Loaded
      print('products size ${response.products.length}');
      emit(ProductsLoadedState(
          products: response.products,
          totalCount: response.totalCount,
          pages: response.pages()
      ));
    });
  }
}
