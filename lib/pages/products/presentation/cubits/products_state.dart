import 'package:equatable/equatable.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';
import '../../../product/domain/entities/attribute.dart';


abstract class ProductsState extends Equatable {
  const ProductsState();

  @override
  List<Object> get props => [];
}

class ProductsLoadingState extends ProductsState {}

class ProductsLoadedState extends ProductsState {
  List<Product> products;
  int totalCount;
  int pages;

  ProductsLoadedState({
    required this.products,
    required this.totalCount,
    required this.pages
  });

  Map<String, List<Attribute>> getNestedAttributes() {
    Map<String, List<Attribute>> result = {};

    // for (var attribute in attributes) {
    //   if(attribute.attrNameEn == null) {
    //     continue;
    //   }

    //   if(result.containsKey(attribute.attrNameEn)) {
    //     result[attribute.attrNameEn]?.add(attribute);
    //   } else {
    //     result[attribute.attrNameEn!] = [attribute];
    //   }
    // }
    return result;
  }
}

class ProductsFailedState extends ProductsState {
  String message;
  ProductsFailedState({required this.message});
}