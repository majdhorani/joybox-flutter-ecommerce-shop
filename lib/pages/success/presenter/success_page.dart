import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spirit/core/widgets/button.dart';
import 'package:spirit/core/widgets/general_app_bar.dart';

class SuccessPage extends StatefulWidget {
  const SuccessPage({Key? key}) : super(key: key);

  @override
  State<SuccessPage> createState() => _SuccessPageState();
}

class _SuccessPageState extends State<SuccessPage> {
  @override
  void initState() {
    super.initState();
    // handle();
  }

  // void handle() async {
  //   await Future.delayed(const Duration(seconds: 3));
  //   Navigator.maybePop(context);
  // }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: Material(
      child: SafeArea(
          child: Column(
            children: [
              SGeneralAppBar(
              title: "order_status".tr()
            ),
            const SizedBox(height: 16),
              Expanded(
      child: Stack(
        children: [
          SizedBox(
            width: double.infinity,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(height: 34),
                  const Icon(Icons.check_circle, color:  Colors.green, size: 100),
                  const SizedBox(height: 34),
                  Text(
                    'order_placed_successfully'.tr(),
                    style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                  ),
                  const SizedBox(height: 8),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 32),
                    child: Text(
                        'order_placed_successfully_description'.tr(),
                        style:
                            const TextStyle(fontSize: 16),
                        textAlign: TextAlign.center),
                  ),
                ]),
          ),
          Positioned(
              bottom: 68,
              left: 30,
              right: 30,
              child: HButton(
                  title: 'view_order'.tr().toUpperCase(),
                  backgroundColor: Colors.black,
                   rounded: false, onPressed: () {
                    Navigator.maybePop(context);
              }))
        ],
      ),
    )
            ],
          )),
    ));
  }
}
