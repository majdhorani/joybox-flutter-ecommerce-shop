import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spirit/core/widgets/bottom_navigation_bar.dart';
import 'package:spirit/di.dart';
import 'package:spirit/pages/account/presenter/account.dart';
import 'package:spirit/pages/addresses/domain/entities/address.dart';
import 'package:spirit/pages/addresses/presenter/address_page.dart';
import 'package:spirit/pages/cart/presentation/cart.dart';
import 'package:spirit/pages/cart/presentation/cubits/cart_cubit.dart';
import 'package:spirit/pages/categories/presentation/categories.dart';
import 'package:spirit/pages/categories/presentation/cubits/categories_cubit.dart';
import 'package:spirit/pages/checkout/presenter/checkout_page.dart';
import 'package:spirit/pages/contact_us/presentation/contact_us_page.dart';
import 'package:spirit/pages/home/presentation/cubits/home_cubit.dart';
import 'package:spirit/pages/home/presentation/home.dart';
import 'package:spirit/pages/orders/domain/entities/order.dart';
import 'package:spirit/pages/orders/presentation/cubit/orders_cubit.dart';
import 'package:spirit/pages/orders/presentation/order_page.dart';
import 'package:spirit/pages/orders/presentation/orders_page.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';
import 'package:spirit/pages/product/presenter/product.dart';
import 'package:spirit/pages/products/domain/usecases/products_usecase.dart';
import 'package:spirit/pages/products/presentation/products.dart';
import 'package:spirit/pages/addresses/presenter/addresses_page.dart';
import 'package:spirit/pages/recipients/domain/entities/recipient.dart';
import 'package:spirit/pages/recipients/presenter/recipient_page.dart';
import 'package:spirit/pages/recipients/presenter/recipients_page.dart';
import 'package:spirit/pages/search/presenter/screens/search_page.dart';
import 'package:spirit/pages/success/presenter/success_page.dart';
import 'package:spirit/routes/routes.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({super.key});

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  final int _pageIndex = 0;

  Widget _getPage(int index) {
    switch (index) {
      case 0:
        return const HomePage();
      case 1:
        return const CategoriesPage();
      case 2:
        return const CartPage();
      case 3:
        return const OrdersPage();
      case 4:
        return const AccountPage();
      default:
        return const SizedBox();
    }
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
        tabBar: SBottomNavigationBar(
          currentIndex: _pageIndex,
          onTap: (int index) {
            if (index == 1) {
              sl<CategoriesCubit>().categories();
            }
            if (index == 2) {
              sl<CartCubit>().cart();
            }
            if (index == 3) {
              sl<OrdersCubit>().orders();
            }
          },
        ),
        tabBuilder: (BuildContext context, int index) {
          return CupertinoTabView(
            onGenerateRoute: (settings) {
              var name = settings.name;
              switch (name) {
                case Routes.routeProduct:
                  return MaterialPageRoute(builder: (_) {
                    Product product = settings.arguments as Product;
                    return ProductPage(
                      id: product.id.toString(),
                    );
                  });
                case Routes.routeProducts:
                  return MaterialPageRoute(builder: (_) {
                    ProductsParams params =
                        settings.arguments as ProductsParams;
                    return ProductsPage(
                      arguments: params,
                    );
                  });
                case Routes.routeAddresses:
                  bool? selection = settings.arguments as bool?;
                  return MaterialPageRoute(builder: (_) {
                    return AddressesPage(selection: selection ?? false);
                  });
                case Routes.routeAddAddress:
                  return MaterialPageRoute(builder: (_) {
                    return AddressPage();
                  });
                case Routes.routeEditAddress:
                  Address address = settings.arguments as Address;
                  return MaterialPageRoute(builder: (_) {
                    return AddressPage(address: address);
                  });
                case Routes.routeRecipients:
                  bool? selection = settings.arguments as bool?;
                  return MaterialPageRoute(builder: (_) {
                    return RecipientsPage(selection: selection ?? false);
                  });
                case Routes.routeAddRecipient:
                  return MaterialPageRoute(builder: (_) {
                    return RecipientPage();
                  });
                case Routes.routeEditRecipient:
                  Recipient recipient = settings.arguments as Recipient;
                  return MaterialPageRoute(builder: (_) {
                    return RecipientPage(recipient: recipient);
                  });
                case Routes.routeCheckout:
                  return MaterialPageRoute(builder: (_) {
                    String? coupon = settings.arguments as String?;
                    return CheckoutPage(coupon: coupon);
                  });
                case Routes.routeOrder:
                  return MaterialPageRoute(builder: (_) {
                    MyOrder order = settings.arguments as MyOrder;
                    return OrderPage(order: order);
                  });
                case Routes.routeContactUs:
                  return MaterialPageRoute(builder: (_) {
                    return const ContactUsPage();
                  });
                case Routes.routeSearch:
                  return MaterialPageRoute(builder: (_) {
                    return const SearchPage();
                  });
                case Routes.routeSuccess:
                  return MaterialPageRoute(builder: (_) {
                    return const SuccessPage();
                  });
              }
              return null;
            },
            builder: (BuildContext context) {
              return _getPage(index);
            },
          );
        });
  }
}
