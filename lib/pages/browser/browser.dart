import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import '../../core/theme/app_colors.dart';

class BrowserPage extends StatefulWidget {
  String url;
  BrowserPage({Key? key, required this.url}) : super(key: key);

  @override
  State<BrowserPage> createState() => _BrowserPageState();
}

class _BrowserPageState extends State<BrowserPage> {
  late var controller = WebViewController();
  double progress = 0;

  @override
  void initState() {
    super.initState();

    controller = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            print(progress);
            setState(() {
              this.progress = progress / 100;
            });
          },
          onPageStarted: (String url) {
            if(url.contains("success") || url.contains("failed")) {
              Navigator.maybePop(context);
            }
          },
          onPageFinished: (String url) {},
          onWebResourceError: (WebResourceError error) {},
          onNavigationRequest: (NavigationRequest request) {
            return NavigationDecision.navigate;
          }
        ),
      )
      ..loadRequest(Uri.parse(widget.url));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.black,
          leading: IconButton(icon: const Icon(Icons.close), onPressed: () {
            Navigator.maybePop(context);
          },),
          actions: [
            InkWell(
              onTap: () {
                Navigator.maybePop(context);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Center(child: Text('done'.tr(), style: const TextStyle(fontWeight: FontWeight.bold),),),
              ),
            )
        ],),
        body: SafeArea(
            child: Stack(
              children: [
                WebViewWidget(controller: controller),
                // InAppWebView(
                //   key: webViewKey,
                //   initialUrlRequest:
                //   URLRequest(url: Uri.parse(widget.url)),
                //   initialOptions: options,
                //   pullToRefreshController: pullToRefreshController,
                //   onWebViewCreated: (controller) {
                //     webViewController = controller;
                //   },
                //   onLoadStart: (controller, url) {
                //     setState(() {
                //       widget.url = url.toString();
                //       urlController.text = widget.url;
                //     });
                //   },
                //   androidOnPermissionRequest: (controller, origin, resources) async {
                //     return PermissionRequestResponse(
                //         resources: resources,
                //         action: PermissionRequestResponseAction.GRANT);
                //   },
                //   shouldOverrideUrlLoading: (controller, navigationAction) async {
                //     var uri = navigationAction.request.url!;
                //
                //     if (![ "http", "https", "file", "chrome",
                //       "data", "javascript", "about"].contains(uri.scheme)) {
                //       if (await canLaunch(widget.url)) {
                //         // Launch the App
                //         await launch(
                //           widget.url,
                //         );
                //         // and cancel the request
                //         return NavigationActionPolicy.CANCEL;
                //       }
                //     }
                //
                //     return NavigationActionPolicy.ALLOW;
                //   },
                //   onLoadStop: (controller, url) async {
                //     pullToRefreshController.endRefreshing();
                //     setState(() {
                //       widget.url = url.toString();
                //       urlController.text = widget.url;
                //     });
                //   },
                //   onLoadError: (controller, url, code, message) {
                //     pullToRefreshController.endRefreshing();
                //   },
                //   onProgressChanged: (controller, progress) {
                //     if (progress == 100) {
                //       pullToRefreshController.endRefreshing();
                //     }
                //     setState(() {
                //       this.progress = progress / 100;
                //       urlController.text = widget.url;
                //     });
                //   },
                //   onUpdateVisitedHistory: (controller, url, androidIsReload) {
                //     setState(() {
                //       widget.url = url.toString();
                //       urlController.text = widget.url;
                //     });
                //   },
                //   onConsoleMessage: (controller, consoleMessage) {
                //     print(consoleMessage);
                //   },
                // ),
                progress < 1.0
                    ? LinearProgressIndicator(value: progress, color: AppColors.red, minHeight: 3)
                    : Container(),
              ],
            ))
    );
  }
}
