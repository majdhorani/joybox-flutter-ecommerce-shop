class Recipient {
  String? firstName;
  String? lastName;
  String? phone;

  Recipient(
      {this.firstName,
      this.lastName,
      this.phone});

  Recipient.fromJson(Map<String, dynamic> json) {
    firstName = json['first_name'];
    lastName = json['last_name'];
    phone = json['phone'];
    
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['first_name'] = firstName;
    data['last_name'] = lastName;
    data['phone'] = phone;
    return data;
  }
}
