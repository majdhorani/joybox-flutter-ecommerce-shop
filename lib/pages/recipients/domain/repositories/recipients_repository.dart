import 'package:dartz/dartz.dart';
import 'package:spirit/core/server/responses/recipients_response.dart';
import 'package:spirit/core/usecases/usecase.dart';
import 'package:spirit/pages/recipients/domain/usecases/add_recipient_usecase.dart';
import '../../../../core/errors/failures.dart';
import '../usecases/edit_address_usecase.dart';

abstract class RecipientsRepository {
  Future<Either<Failure, RecipientsResponse>> recipients({required NoParams params});
  Future<Either<Failure, bool>> editRecipient({required EditRecipientParams params});
  Future<Either<Failure, bool>> addRecipient({required AddRecipientParams params});
}
