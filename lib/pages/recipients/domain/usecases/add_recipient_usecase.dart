import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';
import 'package:spirit/pages/recipients/domain/entities/recipient.dart';
import '../repositories/recipients_repository.dart';

class AddRecipientUseCase extends UseCase<bool, AddRecipientParams> {
  RecipientsRepository repository;
  AddRecipientUseCase({required this.repository});

  @override
  Future<Either<Failure, bool>> call(AddRecipientParams params) {
    return repository.addRecipient(params: params);
  }
}

class AddRecipientParams extends Equatable {
  Recipient recipient;

  AddRecipientParams({
    required this.recipient,
  }) : super();

  Map<String, dynamic> toJson() {
    return recipient.toJson();
  }

  @override
  List<Object?> get props => [recipient];
}
