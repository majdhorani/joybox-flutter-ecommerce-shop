import 'package:dartz/dartz.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/server/responses/recipients_response.dart';
import 'package:spirit/core/usecases/usecase.dart';
import '../repositories/recipients_repository.dart';

class RecipientsUseCase extends UseCase<RecipientsResponse, NoParams> {
  RecipientsRepository repository;
  RecipientsUseCase({required this.repository});

  @override
  Future<Either<Failure, RecipientsResponse>> call(NoParams params) {
    return repository.recipients(params: params);
  }
}


