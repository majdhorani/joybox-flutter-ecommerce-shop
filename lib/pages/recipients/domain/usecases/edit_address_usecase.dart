import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';
import 'package:spirit/pages/addresses/domain/entities/address.dart';
import '../repositories/recipients_repository.dart';

class EditRecipientUseCase extends UseCase<bool, EditRecipientParams> {
  RecipientsRepository repository;
  EditRecipientUseCase({required this.repository});

  @override
  Future<Either<Failure, bool>> call(EditRecipientParams params) {
    return repository.editRecipient(params: params);
  }
}

class EditRecipientParams extends Equatable {
  Address address;

  EditRecipientParams({
    required this.address,
  }) : super();

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['address'] = address.toJson();
    return data;
  }

  @override
  List<Object?> get props => [address];
}
