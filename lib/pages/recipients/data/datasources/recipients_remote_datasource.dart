import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:spirit/core/server/responses/recipients_response.dart';
import 'package:spirit/pages/recipients/domain/usecases/add_recipient_usecase.dart';
import 'package:spirit/pages/recipients/domain/usecases/edit_address_usecase.dart';
import '../../../../../core/errors/exceptions.dart';
import 'dart:developer';

abstract class RecipientsRemoteDataSource {
  Future<RecipientsResponse> recipients();
  Future<bool> editRecipient({required EditRecipientParams params});
  Future<bool> addRecipient({required AddRecipientParams params});
}

class RecipientsRemoteDataSourceImpl implements RecipientsRemoteDataSource {
  Dio client;
  RecipientsRemoteDataSourceImpl({required this.client});

  @override
  Future<RecipientsResponse> recipients() async {
    try {
      String jsonString = await rootBundle.loadString('assets/json/recipients.json');
      Map<String, dynamic> jsonData = jsonDecode(jsonString);
      return RecipientsResponse.fromJson(jsonData);
    } catch (e) {
      print('error while parsing addresses json $e');
      if (e is DioError) {
        // Status Code
        print('error in checkout');
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }

  @override
  Future<bool> editRecipient({required EditRecipientParams params}) async {
    try {
      return Future.value(true);
    } catch (e) {
      print(e);
      if (e is DioError) {
        // Status Code
        print('error in checkout');
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }
  
  @override
  Future<bool> addRecipient({required AddRecipientParams params}) async {
    try {
      return Future.value(true);
    } catch (e) {
      log(e.toString());
      if (e is DioError) {
        // Status Code
        print('error in add recipient');
        log(e.response.toString());
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }
}
