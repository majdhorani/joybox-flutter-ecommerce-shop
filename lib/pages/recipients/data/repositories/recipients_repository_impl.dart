import 'package:dartz/dartz.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/network/network_info.dart';
import 'package:spirit/core/server/responses/recipients_response.dart';
import 'package:spirit/core/usecases/usecase.dart';
import 'package:spirit/pages/recipients/domain/usecases/add_recipient_usecase.dart';
import 'package:spirit/pages/recipients/domain/usecases/edit_address_usecase.dart';
import '../../../../core/errors/exceptions.dart';
import '../../domain/repositories/recipients_repository.dart';
import '../datasources/recipients_local_datasource.dart';
import '../datasources/recipients_remote_datasource.dart';

class RecipientsRepositoryImpl implements RecipientsRepository {
  RecipientsRemoteDataSource remoteDataSource;
  RecipientsLocalDataSource localDataSource;
  NetworkInfo networkInfo;
  RecipientsRepositoryImpl(
      {required this.remoteDataSource,
        required this.localDataSource,
        required this.networkInfo});

  @override
  Future<Either<Failure, RecipientsResponse>> recipients({required NoParams params}) async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.recipients();
        // if(response.billingAddress != null) {
        //   await localDataSource.cacheBillingAddress(response.billingAddress!);
        // }

        // if(response.shippingAddress != null) {
        //   await localDataSource.cacheShippingAddress(response.shippingAddress!);
        // }
        return Right(response);
      } catch (e) {
        if (e is ServerException) { 
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> editRecipient({required EditRecipientParams params}) async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.editRecipient(params: params);
        // await localDataSource.cacheBillingAddress(params.billingAddress);
        // await localDataSource.cacheShippingAddress(params.shippingAddress);
        return Right(response);
      } catch (e) {
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }  

  @override
  Future<Either<Failure, bool>> addRecipient({required AddRecipientParams params}) async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.addRecipient(params: params);
        return Right(response);
      } catch (e) {
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }
}
