import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/extensions/text_editing_controller_extensions.dart';
import 'package:spirit/core/widgets/button.dart';
import 'package:spirit/core/widgets/general_app_bar.dart';
import 'package:spirit/core/widgets/loading.dart';
import 'package:spirit/core/widgets/textfield.dart';
import 'package:spirit/di.dart';
import 'package:spirit/pages/recipients/domain/entities/recipient.dart';
import 'package:spirit/pages/recipients/domain/usecases/add_recipient_usecase.dart';
import 'package:spirit/pages/recipients/presenter/cubits/add_recipient_cubit.dart';
import 'package:spirit/pages/recipients/presenter/cubits/add_recipient_state.dart';

class RecipientPage extends StatefulWidget {
  Recipient? recipient;
  RecipientPage({super.key, this.recipient});

  @override
  State<RecipientPage> createState() => _RecipientPageState();
}

class _RecipientPageState extends State<RecipientPage> {
  AddRecipientCubit addRecipientCubit = sl<AddRecipientCubit>();
  // Controllers
  var firstNameController = TextEditingController();
  var lastNameController = TextEditingController();
  var phoneController = TextEditingController();

  String? firstNameError;
  String? lastNameError;
  String? phoneError;

  @override
  void initState() {
    super.initState();
  }

  void validate() {
    setState(() {
      firstNameError = null;
      lastNameError = null;
      phoneError = null;

      if (firstNameController.isEmpty()) {
        firstNameError = "field_should_not_be_empty".tr();
      }

      if (lastNameController.isEmpty()) {
        lastNameError = "field_should_not_be_empty".tr();
      }

      if (phoneController.isEmpty()) {
        phoneError = "field_should_not_be_empty".tr();
      }
    });
  }

  bool isValid() {
    return (
      firstNameError == null && 
      lastNameError == null && 
      phoneError == null
    );
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: Material(
        child: SafeArea(
          child: Column(
            children: [
              SGeneralAppBar(
                title: "add_recipient".tr(),
                showBackButton: true,
              ),
              BlocConsumer<AddRecipientCubit, AddRecipientState>(
                  bloc: addRecipientCubit,
                  listener: (BuildContext context, AddRecipientState state) {  
                    if(state is AddRecipientSuccessState) {
                      Navigator.pop(context, true);
                    }
                  },
                  builder: (context, state) {
                    if (state is AddRecipientLoadingState) {
                      return HLoading(isWhite: true);
                    }

                    return Expanded(
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 24),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(
                                height: 16,
                              ),
                              Text(
                                'Add recipient who will receive the orders.'
                                    .tr(),
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.black.withOpacity(0.6),
                                    fontFamily: "Oswald"),
                              ),
                              const SizedBox(height: 16),
                              // if (state is RegisterFailedState)
                              //   ErrorMessage(
                              //       message: state.message ??
                              //           'could_not_register_please_try_again'
                              //               .tr()),
                              // 	id	user_id	state	district	address	building	floor	apartment	latitude	longitude

                              HTextField(
                                title: 'first_name'.tr(),
                                controller: firstNameController,
                                error: firstNameError,
                              ),
                              const SizedBox(height: 14),
                              HTextField(
                                title: 'last_name'.tr(),
                                controller: lastNameController,
                                error: lastNameError,
                              ),
                              const SizedBox(height: 14),
                              HTextField(
                                title: 'phone'.tr(),
                                controller: phoneController,
                                error: phoneError,
                              ),                              
                              const SizedBox(height: 14),
                              Divider(
                                  height: 38,
                                  color: Colors.grey.withOpacity(0.2)),
                              HButton(
                                  title: 'save'.tr().toUpperCase(),
                                  backgroundColor: Colors.black,
                                  rounded: false,
                                  onPressed: () {
                                    validate();

                                    if(!isValid()) {
                                      return;
                                    }

                                    var params = AddRecipientParams(
                                        recipient: Recipient(
                                            firstName: firstNameController.text,
                                            lastName: lastNameController.text,
                                            phone: phoneController.text));

                                            // Submit details to server
                                    addRecipientCubit.add(params);
                                  }),
                              const SizedBox(height: 16),
                            ],
                          ),
                        ),
                      ),
                    );
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
