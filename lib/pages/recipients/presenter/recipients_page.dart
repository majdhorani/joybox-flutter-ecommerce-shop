import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:iconsax/iconsax.dart';
import 'package:spirit/core/widgets/button.dart';
import 'package:spirit/core/widgets/general_app_bar.dart';
import 'package:spirit/core/widgets/loading.dart';
import 'package:spirit/di.dart';
import 'package:spirit/pages/recipients/domain/entities/recipient.dart';
import 'package:spirit/pages/recipients/presenter/cubits/recipients_cubit.dart';
import 'package:spirit/pages/recipients/presenter/cubits/recipients_state.dart';
import 'package:spirit/pages/recipients/presenter/widgets/recipients_empty.dart';
import 'package:spirit/pages/recipients/presenter/widgets/recipients_list.dart';
import 'package:spirit/routes/routes.dart';

class RecipientsPage extends StatefulWidget {
  bool selection;
  RecipientsPage({super.key, required this.selection});

  @override
  State<RecipientsPage> createState() => _RecipientsPageState();
}

class _RecipientsPageState extends State<RecipientsPage> {
  RecipientsCubit recipientsCubit = sl<RecipientsCubit>();

  @override
  void initState() {
    recipientsCubit.recipients();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: Material(
        child: SafeArea(
          child: Column(
            children: [
              SGeneralAppBar(
                title: "recipients".tr(),
                showBackButton: true,
              ),
              BlocBuilder<RecipientsCubit, RecipientsState>(
                  bloc: recipientsCubit,
                  builder: (context, state) {
                    if (state is RecipientsLoadingState) {
                      return HLoading(isWhite: true);
                    }

                    if (state is RecipientsLoadedState) {
                      if (state.recipients.isEmpty) {
                        return RecipientsEmpty(onAdd: () async {
                          var result = await Navigator.pushNamed(
                              context, Routes.routeAddRecipient);
                          if (result != null) {
                            if (result is bool && result) {
                              recipientsCubit.recipients();
                            }
                          }
                        });
                      }
                      return Expanded(
                        child: Column(
                          children: [
                            Expanded(
                              child: SingleChildScrollView(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      const SizedBox(
                                        height: 8,
                                      ),
                                      RecipientsList(
                                          recipients: state.recipients,
                                          onTap: (Recipient recipient) {
                                            Navigator.pop(context, recipient);
                                          }),
                                      const SizedBox(height: 16),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            const Divider(height: 1),
                            Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 16, vertical: 16),
                              child: SizedBox(
                                child: HButton(
                                    backgroundColor: Colors.black,
                                    iconData: Iconsax.location_add,
                                    iconColor: Colors.white,
                                    title: "add_recipient".tr().toUpperCase(),
                                    onPressed: () async {
                                      var result = await Navigator.pushNamed(
                                          context, Routes.routeAddRecipient);
                                      if (result != null) {
                                        if (result is bool && result) {
                                          recipientsCubit.recipients();
                                        }
                                      }
                                    }),
                              ),
                            ),
                          ],
                        ),
                      );
                    }
                    return const SizedBox();
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
