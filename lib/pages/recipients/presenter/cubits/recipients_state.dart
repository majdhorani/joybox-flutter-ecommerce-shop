import 'package:equatable/equatable.dart';
import '../../domain/entities/recipient.dart';

abstract class RecipientsState extends Equatable {
  const RecipientsState();

  @override
  List<Object> get props => [];
}

class RecipientsLoadingState extends RecipientsState {}

class RecipientsLoadedState extends RecipientsState {
  List<Recipient> recipients;
  RecipientsLoadedState({required this.recipients});
}

class RecipientsFailedState extends RecipientsState {
  String message;
  RecipientsFailedState({required this.message});
}
