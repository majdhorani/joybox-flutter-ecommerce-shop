import 'package:equatable/equatable.dart';

abstract class AddRecipientState extends Equatable {
  const AddRecipientState();

  @override
  List<Object> get props => [];
}

class AddRecipientInitialState extends AddRecipientState {}

class AddRecipientLoadingState extends AddRecipientState {}

class AddRecipientSuccessState extends AddRecipientState {
  const AddRecipientSuccessState();
}

class AddRecipientFailedState extends AddRecipientState {
  String message;
  AddRecipientFailedState({required this.message});
}
