import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../domain/usecases/recipients_usecase.dart';
import 'recipients_state.dart';

class RecipientsCubit extends Cubit<RecipientsState> {
  RecipientsUseCase recipientsUseCase;
  RecipientsCubit({
    required this.recipientsUseCase
  }) : super(RecipientsLoadingState());

  void recipients() async {
    // Loading
    emit(RecipientsLoadingState());

    // Fetch addresses
    var response = await recipientsUseCase.call(NoParams());
    response.fold((failure) {
      print('addresses state error');
      print(failure);
      if (failure is ServerFailure) {
        emit(RecipientsFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(RecipientsFailedState(message: 'Something went wrong'));
    }, (response) {
      emit(RecipientsLoadedState(recipients: response.data));
    });
  }
}
