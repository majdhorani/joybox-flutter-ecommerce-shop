import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/pages/recipients/domain/usecases/add_recipient_usecase.dart';
import 'package:spirit/pages/recipients/presenter/cubits/add_recipient_state.dart';

class AddRecipientCubit extends Cubit<AddRecipientState> {
  AddRecipientUseCase addRecipientUseCase;

  AddRecipientCubit({required this.addRecipientUseCase})
      : super(AddRecipientInitialState());

  void add(AddRecipientParams params) async {
    // Loading
    emit(AddRecipientLoadingState());

    // Fetch products
    var response = await addRecipientUseCase.call(params);
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(AddRecipientFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(AddRecipientFailedState(message: 'Something went wrong'));
    }, (response) {
      emit(const AddRecipientSuccessState());
    });
  }
}
