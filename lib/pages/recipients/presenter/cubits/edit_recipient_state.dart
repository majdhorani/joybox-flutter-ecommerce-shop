import 'package:equatable/equatable.dart';

abstract class EditRecipientState extends Equatable {
  const EditRecipientState();

  @override
  List<Object> get props => [];
}

class EditRecipientInitialState extends EditRecipientState {}

class EditRecipientLoadingState extends EditRecipientState {}

class EditRecipientSuccessState extends EditRecipientState {
  const EditRecipientSuccessState();
}

class EditRecipientFailedState extends EditRecipientState {
  String message;
  EditRecipientFailedState({required this.message});
}
