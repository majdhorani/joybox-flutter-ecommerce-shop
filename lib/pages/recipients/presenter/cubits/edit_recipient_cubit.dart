import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import '../../domain/usecases/edit_address_usecase.dart';
import 'edit_recipient_state.dart';

class EditRecipientCubit extends Cubit<EditRecipientState> {
  EditRecipientUseCase editRecipientUseCase;

  EditRecipientCubit({required this.editRecipientUseCase})
      : super(EditRecipientInitialState());

  void edit(EditRecipientParams params) async {
    // Loading
    emit(EditRecipientLoadingState());

    // Fetch products
    var response = await editRecipientUseCase.call(params);
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(EditRecipientFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(EditRecipientFailedState(message: 'Something went wrong'));
    }, (response) {
      emit(const EditRecipientSuccessState());
    });
  }
}
