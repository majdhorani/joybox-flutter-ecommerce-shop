import 'package:flutter/material.dart';
import 'package:spirit/pages/recipients/domain/entities/recipient.dart';
import 'package:spirit/pages/recipients/presenter/widgets/recipient_item.dart';

class RecipientsList extends StatelessWidget {
  List<Recipient> recipients;
  Function(Recipient recipient) onTap;
  RecipientsList({super.key, required this.recipients, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: ListView.separated(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        separatorBuilder: (context, index) {
          Recipient recipient = recipients[index];
          return RecpientItem(
            recipient: recipient, 
            onTap: onTap);
        },
        itemBuilder: (context, index) {
          if (index == 0) {
            return const SizedBox();
          }
          return Divider(
            color: Colors.grey.withOpacity(0.3),
          );
        },
        itemCount: recipients.length + 1,
      ),
    );
  }
}
