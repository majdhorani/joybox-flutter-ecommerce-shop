import 'package:flutter/material.dart';
import 'package:iconsax/iconsax.dart';
import 'package:spirit/pages/recipients/domain/entities/recipient.dart';

class RecpientItem extends StatefulWidget {
  Recipient recipient;
  Function(Recipient recipient) onTap;
  RecpientItem({super.key, required this.recipient, required this.onTap});

  @override
  State<RecpientItem> createState() => _RecpientItemState();
}

class _RecpientItemState extends State<RecpientItem> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
            onTap: () {
              widget.onTap(widget.recipient);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 2),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // const Text("Country: ${"Syria"}",
                      //     style: TextStyle(
                      //         color: Colors.black,
                      //         fontSize: 14,
                      //         fontWeight: FontWeight.w400)),
                      Text("First name: ${widget.recipient.firstName?.trim()}",
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w400)),
                      Text("Last name: ${widget.recipient.lastName?.trim()}",
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w400)),
                      Text("Phone: ${widget.recipient.phone?.trim()}",
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w400)),
                    ],
                  ),
                ),
                Row(
                  children: [
                    Material(
                      color: Colors.black,
                      child: Padding(
                        padding: const EdgeInsets.all(6),
                        child: InkWell(
                          onTap: () {

                          },
                          child: const Icon(Iconsax.edit,
                              size: 17, color: Colors.white),
                        ),
                      ),
                    ),
                    const SizedBox(width: 8),
                    Material(
                      color: Colors.red,
                      child: Padding(
                        padding: const EdgeInsets.all(6),
                        child: InkWell(
                          onTap: () {

                          },
                          child: const Icon(Iconsax.trash,
                              size: 17, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          );
  }
}