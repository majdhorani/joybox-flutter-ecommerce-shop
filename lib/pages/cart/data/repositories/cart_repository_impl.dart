import 'package:dartz/dartz.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/network/network_info.dart';
import 'package:spirit/core/server/responses/calculate_response.dart';
import '../../../../core/errors/exceptions.dart';
import '../../../../core/server/responses/cart_response.dart';
import '../../../auth/data/datasources/auth_local_datasource.dart';
import '../../../product/domain/entities/product.dart';
import '../../domain/repositories/cart_repository.dart';
import '../../domain/usecases/calculate_usecase.dart';
import '../datasources/cart_local_datasource.dart';
import '../datasources/cart_remote_datasource.dart';

class CartRepositoryImpl implements CartRepository {
  CartRemoteDataSource remoteDataSource;
  CartLocalDataSource localDataSource;
  AuthLocalDataSource authLocalDataSource;

  NetworkInfo networkInfo;
  CartRepositoryImpl(
      {
        required this.remoteDataSource,
        required this.localDataSource,
        required this.authLocalDataSource,
        required this.networkInfo
      });

  @override
  Future<Either<Failure, bool>> addToCart(
      {required Product product}) async {
    if (await networkInfo.isConnected) {
      try {
        if(!authLocalDataSource.isLoggedIn()) {
          return Right(await localDataSource.add(product));
        }

        var response = await remoteDataSource.add(product);
        await localDataSource.add(product);
        return Right(response);
      } catch (e) {
        print(e);
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> removeFromCart({required String id}) async {
    if (await networkInfo.isConnected) {
      try {
        if(!authLocalDataSource.isLoggedIn()) {
          return Right(await localDataSource.remove(id));
        }

        var response = await remoteDataSource.remove(id);
        await localDataSource.remove(id);
        return Right(response);
      } catch (e) {
        print(e);
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }

  @override
  Future<Either<Failure, CartResponse>> cart() async {
    if (await networkInfo.isConnected) {
      try {
        if(!authLocalDataSource.isLoggedIn()) {
          print("local cart");
          var cart = await localDataSource.cart();
          for (var element in cart) {
            print(element.quantity);
          }
          return Right(CartResponse(products: await localDataSource.cart()));
        }

        var response = await remoteDataSource.cart();
        await localDataSource.cache(response.products);
        return Right(response);
      } catch (e) {
        print(e);
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }

  @override
  Future<bool> cached({required String id}) {
    return localDataSource.cached(id);
  }

  @override
  Future<Either<Failure, CalculateResponse>> calculate({required CalculateParams params}) async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.calculate(params);
        return Right(response);
      } catch (e) {
        print(e);
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }
}
