import 'package:spirit/pages/product/domain/entities/product.dart';
import '../../../../core/session/session.dart';

abstract class CartLocalDataSource {
  Future<bool> add(Product product);
  Future<bool> remove(String id);
  Future<bool> cached(String id);
  Future<List<Product>> cart();
  Future<bool> cache(List<Product> products);
  Future<bool> clear();
}

class CartLocalDataSourceImpl implements CartLocalDataSource {
  @override
  Future<bool> add(Product product) async {
    print('add product to cart cache ${product.id}');
    return Session.addToeCart(product);
  }

  @override
  Future<bool> remove(String id) {
    return Session.removeFromCart(id);
  }

  @override
  Future<List<Product>> cart() async {
    return Session.cart();
  }

  @override
  Future<bool> cache(List<Product> products) {
    return Session.cacheCart(products);
  }

  @override
  Future<bool> cached(String id) async {
    var cart = Session.cart();
    for (var product in cart) {
      if(product.id == id) {
        return true;
      }
    }
    return false;
  }

  @override
  Future<bool> clear() async {
    return await cache([]);
  }
}
