import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import '../../../../core/errors/exceptions.dart';
import '../../../../core/server/responses/calculate_response.dart';
import '../../../../core/server/responses/cart_response.dart';
import '../../../product/domain/entities/product.dart';
import '../../domain/usecases/calculate_usecase.dart';

abstract class CartRemoteDataSource {
  Future<bool> add(Product product);
  Future<bool> remove(String id);
  Future<CartResponse> cart();
  Future<CalculateResponse> calculate(CalculateParams params);
}

class CartRemoteDataSourceImpl implements CartRemoteDataSource {
  Dio client;
  CartRemoteDataSourceImpl({required this.client});

  @override
  Future<bool> add(Product product) async {
    try {
      /** 
       * Fetch from server 
       * */

      return true;
    } catch (e) {
      if (e is DioError) {
        // Status Code
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }

  @override
  Future<bool> remove(String id) async {
    try {
      /** 
       * Fetch from server 
       * */

      return true;
    } catch (e) {
      if (e is DioError) {
        // Status Code
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }

  @override
  Future<CartResponse> cart() async {
    try {
      String jsonString = await rootBundle.loadString('assets/json/cart.json');
      Map<String, dynamic> jsonData = jsonDecode(jsonString);
      var result = CartResponse.fromJson(jsonData);
      return result;
    }
    catch (e) {
      print(e);
      if (e is DioError) {
        // Status Code
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }

  @override
  Future<CalculateResponse> calculate(CalculateParams params) async {
    try {
      String jsonString = await rootBundle.loadString('assets/json/calculate.json');
      Map<String, dynamic> jsonData = jsonDecode(jsonString);
      var result = CalculateResponse.fromJson(jsonData);
      return result;
    }
    catch (e) {
      if (e is DioError) {
        // Status Code
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }
}
