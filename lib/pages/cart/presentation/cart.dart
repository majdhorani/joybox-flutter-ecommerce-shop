import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/widgets/button.dart';
import 'package:spirit/core/widgets/general_app_bar.dart';
import 'package:spirit/core/widgets/loading.dart';
import 'package:spirit/di.dart';
import 'package:spirit/pages/cart/domain/usecases/calculate_usecase.dart';
import 'package:spirit/pages/cart/presentation/cubits/calculate_cubit.dart';
import 'package:spirit/pages/cart/presentation/cubits/cart_cubit.dart';
import 'package:spirit/pages/cart/presentation/cubits/cart_state.dart';
import 'package:spirit/pages/cart/presentation/widgets/apply_coupon.dart';
import 'package:spirit/pages/cart/presentation/widgets/cart_empty.dart';
import 'package:spirit/pages/cart/presentation/widgets/cart_item.dart';
import 'package:spirit/pages/cart/presentation/widgets/cart_total.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';
import 'package:spirit/routes/routes.dart';

class CartPage extends StatefulWidget {
  const CartPage({super.key});

  @override
  State<CartPage> createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  TextEditingController couponController = TextEditingController();
  // Cubits
  CartCubit cartCubit = sl<CartCubit>();
  CalculateCubit calculateCubit = sl<CalculateCubit>();

  List<Product> products = [];
  String? coupon;

  void calculate() {
    calculateCubit
        .calculate(CalculateParams(products: products, coupon: coupon));
  }

  Widget CartList() {
    return ListView.separated(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return CartItem(
              product: products[index],
              onRemoved: () {
                cartCubit.cart();
              },
              onUpdated: () {
                print('updated product quantity: ${products[index].quantity}');
                cartCubit.refreshCartTotals();
                calculate();
                setState(() {});
              });
        },
        separatorBuilder: (context, state) {
          return Divider(
            height: 30,
            color: Colors.grey.withOpacity(0.1),
            thickness: 2,
          );
        },
        itemCount: products.length);
  }

  // @override
  // Widget build(BuildContext context) {
  //   return CupertinoPageScaffold(
  //     // appBar: SAppBar(),
  //     // bottomNavigationBar: SBottomNavigationBar(),
  //     child: SafeArea(
  //       child: Material(
  //         child: Column(
  //           children: [
  //             SGeneralAppBar(title: "Shopping cart",),
  //             Expanded(
  //               child: BlocConsumer<CartCubit, CartState>(
  //                 bloc: cartCubit,
  //                 listener: (BuildContext context, CartState state) {  },
  //                 builder: (context, state) {
  //                   return SingleChildScrollView(
  //                     child: Padding(
  //                       padding: const EdgeInsets.symmetric(horizontal: 16.0),
  //                       child: Column(
  //                         mainAxisAlignment: MainAxisAlignment.start,
  //                         crossAxisAlignment: CrossAxisAlignment.start,
  //                         children: [
  //                           SizedBox(height: 16),
  //                           CartList(),
  //                           Divider(height: 30,),
  //                           ApplyCoupon(),
  //                           const SizedBox(height: 16),
  //                           Container(
  //                             width: double.infinity,
  //                             decoration: BoxDecoration(
  //                                 color: Color.fromARGB(68, 241, 242, 242),
  //                                 border: Border.all(color: Color.fromARGB(51, 196, 196, 196), width: 2)),
  //                             child: Column(
  //                               mainAxisAlignment: MainAxisAlignment.start,
  //                                     crossAxisAlignment: CrossAxisAlignment.start,
  //                               children: [
  //                                 Padding(
  //                                   padding: const EdgeInsets.all(16.0),
  //                                   child: Column(
  //                                     mainAxisAlignment: MainAxisAlignment.start,
  //                                     crossAxisAlignment: CrossAxisAlignment.start,
  //                                     children: [
  //                                       Text("Subtotal"),
  //                                       SizedBox(
  //                                         height: 8,
  //                                       ),
  //                                       Text("Delivery Fees")
  //                                     ],
  //                                   ),
  //                                 ),
  //                                 Container(
  //                                     width: double.infinity,
  //                                     child: Material(
  //                                       color: Colors.black,
  //                                       child: InkWell(
  //                                         child: Container(
  //                                           padding: EdgeInsets.all(10),
  //                                           child: Text(
  //                                             "PROCESS TO CHECKOUT",
  //                                             textAlign: TextAlign.center,
  //                                             style: TextStyle(
  //                                                 fontWeight: FontWeight.bold,
  //                                                 color: Colors.white,
  //                                                 fontFamily: "Oswald",
  //                                                 fontSize: 18),
  //                                           ),
  //                                         ),
  //                                       ),
  //                                     ))
  //                               ],
  //                             ),
  //                           ),
  //                           const SizedBox(height: 10),
  //                         ],
  //                       ),
  //                     ),
  //                   );
  //                 }
  //               ),
  //             ),
  //           ],
  //         ),
  //       ),
  //     ),
  //   );
  // }

  // Widget _coupon() {
  //   return Container(
  //     padding: const EdgeInsets.symmetric(horizontal: 30),
  //     child: Column(
  //       children: [
  //         const Divider(height: 40, thickness: 2, color: Color(0xFFDADADA)),
  //         if (coupon == null)
  //           HTextField(
  //             hintText: 'coupon_code'.tr(),
  //             disabled: products.isEmpty,
  //             controller: couponController,
  //           ),
  //         if (coupon != null)
  //           Row(
  //             children: [
  //               Text(coupon!,
  //                   style: const TextStyle(
  //                       color: AppColors.black,
  //                       decoration: TextDecoration.underline)),
  //               const SizedBox(width: 2),
  //               IconButton(
  //                   onPressed: () {
  //                     setState(() {
  //                       coupon = null;
  //                     });
  //                     calculate();
  //                   },
  //                   icon: const Icon(Icons.close))
  //             ],
  //           ),
  //         const SizedBox(
  //           height: 10,
  //         ),
  //         HButton(
  //           title: 'apply_coupon'.tr(),
  //           disabled: coupon != null,
  //           onPressed: () {
  //             if (couponController.text.isEmpty) {
  //               setState(() {
  //                 coupon = null;
  //               });
  //             } else {
  //               setState(() {
  //                 coupon = couponController.text;
  //               });
  //               calculate();
  //             }
  //           },
  //           rounded: true,
  //         ),
  //       ],
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: SafeArea(
      child: Material(
        child: Column(
          children: [
            SGeneralAppBar(
              title: "Shopping cart",
            ),
            BlocConsumer<CartCubit, CartState>(
              bloc: cartCubit,
              listener: (context, state) {
                if (state is CartLoadedState) {
                  setState(() {
                    products = state.products ?? [];
                  });
                  if (state.products!.isNotEmpty) {
                    calculateCubit.calculate(CalculateParams(
                        products: state.products!, coupon: coupon));
                  }
                }
              },
              builder: (context, state) {
                if (state is CartLoadingState) {
                  return HLoading(isWhite: true);
                }
                if (state is CartFailedState) {
                  return Container(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    width: double.infinity,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'failed_to_load_wishlist'.tr(),
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(height: 10),
                        SizedBox(
                          width: 120,
                          child: HButton(
                              title: 'retry'.tr(),
                              rounded: true,
                              onPressed: () {
                                cartCubit.cart();
                              }),
                        )
                      ],
                    ),
                  );
                }
                if (state is CartLoadedState) {
                  if (state.products!.isEmpty) return const HCartEmpty();
                  return Expanded(
                    child: SingleChildScrollView(
                      child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Column(children: [
                          const SizedBox(height: 16),
                          CartList(),
                          Divider(
                            height: 30,
                            color: Colors.grey.withOpacity(0.4),
                          ),
                          const ApplyCoupon(),
                          const SizedBox(height: 16),
                          CartTotal(
                              calculateCubit: calculateCubit,
                              products: state.products ?? [],
                              onCheckout: () async {
                                await Navigator.of(context, rootNavigator: true)
                                    .pushNamed(Routes.routeCheckout,
                                        arguments: coupon);
                              })
                        ]),
                      ),
                    ),
                  );
                }
                return const SizedBox();
              },
            ),
          ],
        ),
      ),
    ));
  }
}
