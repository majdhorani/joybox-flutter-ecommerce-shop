import 'package:equatable/equatable.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';

abstract class CartItemState extends Equatable {
  const CartItemState();

  @override
  List<Object> get props => [];
}

class CartItemInitialState extends CartItemState {}

class CartItemLoadingState extends CartItemState {}

class CartItemLoadedState extends CartItemState {
  List<Product>? products;

  CartItemLoadedState({this.products});
}

class CartItemFailedState extends CartItemState {
  String message;
  CartItemFailedState({required this.message});
}
