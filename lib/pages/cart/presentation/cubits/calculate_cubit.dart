import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import '../../domain/usecases/calculate_usecase.dart';
import 'calculate_state.dart';

class CalculateCubit extends Cubit<CalculateState> {
  CalculateUseCase calculateUseCase;
  CalculateCubit({required this.calculateUseCase})
      : super(CalculateInitialState());

  void calculate(CalculateParams params) async {
    // Loading
    emit(CalculateLoadingState());

    // Fetch products
    var response = await calculateUseCase.call(params);
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(CalculateFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(CalculateFailedState(message: 'Something went wrong'));
    }, (response) {
      // Loaded
      print('received calc response data');
      emit(CalculateLoadedState(data: response));
    });
  }
}
