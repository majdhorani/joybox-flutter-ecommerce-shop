import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import '../../../product/domain/entities/product.dart';
import '../../domain/usecases/add_to_cart_usecase.dart';
import '../../domain/usecases/cached_in_cart_usecase.dart';
import '../../domain/usecases/remove_from_cart_usecase.dart';
import 'cart_item_state.dart';

class CartItemCubit extends Cubit<CartItemState> {
  AddToCartUseCase addToCartUseCase;
  RemoveFromCartUseCase removeFromCartUseCase;
  CachedInCartUseCase cachedInCartUseCase;
  CartItemCubit(
      {required this.addToCartUseCase,
      required this.removeFromCartUseCase,
      required this.cachedInCartUseCase})
      : super(CartItemInitialState());

  void add(Product product) async {
    // Loading
    emit(CartItemLoadingState());

    // Fetch products
    var response =
        await addToCartUseCase.call(AddToCartParams(product: product));
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(CartItemFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(CartItemFailedState(message: 'Something went wrong'));
    }, (response) {
      // Loaded
      emit(CartItemLoadedState());
    });
  }

  void remove(String id) async {
    // Loading
    emit(CartItemLoadingState());

    // Fetch products
    var response =
        await removeFromCartUseCase.call(RemoveFromCartParams(id: id));
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(CartItemFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(CartItemFailedState(message: 'Something went wrong'));
    }, (response) {
      emit(CartItemLoadedState());
    });
  }

  Future<bool> cached({required String id}) async {
    var cached = await cachedInCartUseCase.call(CachedInCartParams(id: id));
    return cached;
  }
}
