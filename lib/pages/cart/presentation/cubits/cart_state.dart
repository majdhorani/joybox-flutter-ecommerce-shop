import 'package:equatable/equatable.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';

abstract class CartState extends Equatable {
  const CartState();

  @override
  List<Object> get props => [];
}

class CartInitialState extends CartState {}

class CartLoadingState extends CartState {}

class CartLoadedState extends CartState {
  List<Product>? products;

  double getSubtotal() {
    double subtotal = 0;
    products?.forEach((product) {
      subtotal += product.total();
    });
    print('subtotal: $subtotal');
    return subtotal;
  }

  CartLoadedState({this.products});

  @override
  List<Object> get props => [products ?? [], getSubtotal()];
}

class CartFailedState extends CartState {
  String message;
  CartFailedState({required this.message});
}
