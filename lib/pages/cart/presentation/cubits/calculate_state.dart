import 'package:equatable/equatable.dart';
import '../../../../core/server/responses/calculate_response.dart';

abstract class CalculateState extends Equatable {
  const CalculateState();

  @override
  List<Object> get props => [];
}

class CalculateInitialState extends CalculateState {}

class CalculateLoadingState extends CalculateState {}

class CalculateLoadedState extends CalculateState {
  CalculateResponse data;

  CalculateLoadedState({required this.data});

  @override
  List<Object> get props => [];
}

class CalculateFailedState extends CalculateState {
  String message;
  CalculateFailedState({required this.message});
}
