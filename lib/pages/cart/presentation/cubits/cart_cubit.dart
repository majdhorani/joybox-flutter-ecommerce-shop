import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import '../../domain/usecases/cart_usecase.dart';
import 'cart_state.dart';

class CartCubit extends Cubit<CartState> {
  CartUseCase cartUseCase;
  CartCubit({required this.cartUseCase})
      : super(CartInitialState());

  void cart() async {
    // Loading
    emit(CartLoadingState());

    // Fetch products
    var response = await cartUseCase.call(const CartParams());
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(CartFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(CartFailedState(message: 'Something went wrong'));
    }, (response) {
      // Loaded
      print('products size ${response.products.length}');
      emit(CartLoadedState(products: response.products));
    });
  }

  void refreshCartTotals() {
    if(state is CartLoadedState) {
      emit(state);
    }
  }
}
