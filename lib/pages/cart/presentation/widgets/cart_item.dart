import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:iconsax/iconsax.dart';
import 'package:spirit/di.dart';
import 'package:spirit/pages/cart/presentation/cubits/cart_item_cubit.dart';
import 'package:spirit/pages/cart/presentation/cubits/cart_item_state.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';

class CartItem extends StatefulWidget {
  Product product;
  Function? onRemoved;
  Function? onUpdated;
  CartItem({Key? key, required this.product, this.onRemoved, this.onUpdated})
      : super(key: key);

  @override
  State<CartItem> createState() => _CartItemState();
}

class _CartItemState extends State<CartItem> {
  late int quantity;
  CartItemCubit updateCartItemCubit = sl<CartItemCubit>();
  CartItemCubit removeCartItemCubit = sl<CartItemCubit>();

  @override
  void initState() {
    super.initState();
    quantity = widget.product.quantity ?? 0;
  }

  Widget Actions() {
    return Column(
      children: [
        BlocConsumer<CartItemCubit, CartItemState>(
            bloc: removeCartItemCubit,
            listener: (context, state) {
              if (state is CartItemLoadedState) {
                if (widget.onRemoved != null) {
                  widget.onRemoved!();
                }
              }
            },
            builder: (context, state) {
              if(state is CartItemLoadingState) {
                return Container(
                  color: const Color(0xFFe6f1fa),
                  width: 20,
                  height: 20,
                  child: Center(child: SpinKitCircle(size: 10,)),
                );
              }
              return InkWell(
                onTap: () {
                  if(state is CartItemLoadingState) {
                    return;
                  }

                  // Remove from wishlist
                  removeCartItemCubit.remove(widget.product.id.toString());
                },
                child: Container(
                  color: Colors.white,
                  width: 20,
                  height: 20,
                  child: const Icon(Icons.close, size: 18),
                ),
              );
            }),
      ],
    );
  }

  Widget Footer() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Text(
        //   "QUANTITY",
        //   style: TextStyle(fontSize: 12, fontFamily: "Oswald", color: Colors.black.withOpacity(0.6)),
        // ),
        // const SizedBox(
        //   height: 2,
        // ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            BlocConsumer<CartItemCubit, CartItemState>(
                bloc: updateCartItemCubit,
                listener: (context, state) {
                  if (state is CartItemLoadedState) {
                    setState(() {
                      // quantity = int.parse(widget.product.quantity!);
                    });

                    if (widget.onUpdated != null) {
                      widget.onUpdated!();
                    }
                  }
                },
                builder: (context, state) {
                  return Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Colors.grey.withOpacity(0.6))),
                        child: InkWell(
                          onTap: () {
                            if (quantity <= 1) {
                              return;
                            }
                            quantity = quantity - 1;
                            widget.product.quantity = quantity;
                            updateCartItemCubit.add(widget.product);
                            setState(() {});
                          },
                          child: const Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 1, vertical: 1),
                            child: Icon(Iconsax.minus, color: Colors.grey, size: 24),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 12, vertical: 4),
                        child: Text(
                          quantity.toString(),
                          style:
                              const TextStyle(fontSize: 16, fontFamily: "Oswald"),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Colors.grey.withOpacity(0.6))),
                        child: InkWell(
                          onTap: () {
                            quantity = quantity + 1;
                            widget.product.quantity = quantity;
                            updateCartItemCubit.add(widget.product);
                            setState(() {});
                          },
                          child: const Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 1, vertical: 1),
                            child: Icon(Iconsax.add,
                                color: Color.fromRGBO(158, 158, 158, 1), size: 24),
                          ),
                        ),
                      ),
                    ],
                  );
                }),
            Container(
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      "${widget.product.getSellingPrice()} EUR",
                      style: const TextStyle(
                          fontSize: 18,
                          fontFamily: "Oswald",
                          fontWeight: FontWeight.w500),
                    ),
                  ]),
            ),
          ],
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
                width: 100,
                child: AspectRatio(
                  aspectRatio: 1,
                  child: Container(decoration: BoxDecoration(border: Border.all(color: Colors.grey.withOpacity(0.2), width: 1)), child: CachedNetworkImage(
                          imageUrl: widget.product.getImage(),
                        ),),
                )),
                const SizedBox(width: 3),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 44,
                        child: Text(
                          widget.product.name ?? "",
                          maxLines: 2,
                          style: const TextStyle(fontSize: 16, fontFamily: "Oswald"),
                        ),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      
                    ],
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Footer()
                ],
              ),
            )),
          ],
        ),
        // Actions
        Positioned(right: 0, child: Actions())
      ],
    );
  }
}
