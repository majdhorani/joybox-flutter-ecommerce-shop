import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../core/theme/app_colors.dart';
import '../../../../core/widgets/button.dart';

class HCartEmpty extends StatefulWidget {
  const HCartEmpty({Key? key}) : super(key: key);

  @override
  State<HCartEmpty> createState() => _HCartEmptyState();
}

class _HCartEmptyState extends State<HCartEmpty> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Stack(
        children: [
          SizedBox(
            width: double.infinity,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(height: 34),
                  Container(
                    width: 45,
                    height: 45,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: AppColors.black,
                    ),
                    child: Container(
                        padding: const EdgeInsets.all(10),
                        child: SvgPicture.asset('assets/svg/nav_cart.svg',
                            color: Colors.white)),
                  ),
                  const SizedBox(height: 34),
                  Text(
                    'your_cart_is_empty'.tr(),
                    style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                  ),
                  const SizedBox(height: 8),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 32),
                    child: Text(
                        'your_cart_is_empty_description'.tr(),
                        style:
                            const TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                        textAlign: TextAlign.center),
                  ),
                ]),
          ),
          Positioned(
              bottom: 68,
              left: 30,
              right: 30,
              child: HButton(
                  title: 'start_shopping'.tr().toUpperCase(),
                  backgroundColor: Colors.black,
                   rounded: false, onPressed: () {
                    Navigator.maybePop(context);
              }))
        ],
      ),
    );
  }
}
