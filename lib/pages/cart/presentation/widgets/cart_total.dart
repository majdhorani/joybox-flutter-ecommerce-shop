import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/widgets/button.dart';
import 'package:spirit/core/widgets/loading.dart';
import 'package:spirit/pages/cart/presentation/cubits/calculate_cubit.dart';
import 'package:spirit/pages/cart/presentation/cubits/calculate_state.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';

class CartTotal extends StatelessWidget {
  List<Product> products;
  CalculateCubit calculateCubit;
  Function onCheckout;
  CartTotal({super.key, required this.calculateCubit, required this.products, required this.onCheckout});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        BlocBuilder<CalculateCubit, CalculateState>(
            bloc: calculateCubit,
            builder: (context, state) {
              if (state is CalculateLoadingState) {
                return HLoading(isWhite: true);
              }

              if (state is CalculateLoadedState) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: const Color.fromARGB(68, 241, 242, 242),
                          borderRadius: BorderRadius.circular(8),
                          border: Border.all(
                              color: const Color.fromARGB(51, 196, 196, 196),
                              width: 2)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Text("Subtotal"),
                                    Text("${state.data.subtotal} AED", style: const TextStyle(fontWeight: FontWeight.bold),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Text("Delivery Fees"),
                                    Text(state.data.isFreeDelivery() ? "FREE" : "${state.data.deliveryFee} AED", style: const TextStyle(color: Colors.green, fontWeight: FontWeight.bold),),
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Text("Total Price"),
                                    Text("${state.data.total} AED", style: const TextStyle(fontWeight: FontWeight.bold),),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          HButton(
                                      textBold: true,
                                      title: 'proceed_to_checkout'.tr().toUpperCase(),
                                      backgroundColor: Colors.black,
                                      onPressed: () async {
                                        onCheckout();
                                      },
                                    ),
                        ],
                      ),
                    ),
                    if (state.data.hasMessage())
                      Text(state.data.message ?? ''),
                    // if (state is CalculateLoadedState &&
                    //     state.data.untilFreeDelivery() > 0)
                    //   Text(
                    //       'AED ${state.data.untilFreeDelivery().toStringAsFixed(2)} left for free shipping',
                    //       style: const TextStyle(fontSize: 12)),
                  ],
                );
              }
            
            return const SizedBox();
            }),
        const SizedBox(
          height: 16,
        ),
      ],
    );
  }
}
