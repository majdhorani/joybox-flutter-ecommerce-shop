import 'package:flutter/material.dart';

class ApplyCoupon extends StatefulWidget {
  const ApplyCoupon({super.key});

  @override
  State<ApplyCoupon> createState() => _ApplyCouponState();
}

class _ApplyCouponState extends State<ApplyCoupon> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text(
          "APPLY DISCOUNT CODE",
          style: TextStyle(
              color: Color(0xFF828282), fontSize: 14, fontFamily: "Oswald"),
        ),
        const SizedBox(height: 8),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 4),
          decoration: BoxDecoration(
                          color: const Color.fromARGB(68, 241, 242, 242),
                          borderRadius: BorderRadius.circular(8),
                          border: Border.all(
                              color: const Color.fromARGB(51, 196, 196, 196),
                              width: 2)),
          child: Row(
            children: [
              const Expanded(
                  child: Padding(
                padding: EdgeInsets.only(left: 12.0),
                child: Text(
                  "Enter discount code",
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
              )),
              Container(
                padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 26),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                    border: Border.all(
                      color: const Color.fromARGB(59, 155, 155, 155),
                      width: 1,
                    ),
                    color: const Color.fromARGB(255, 255, 186, 11)),
                child: const Text(
                  "APPLY",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontFamily: "Oswald"),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}