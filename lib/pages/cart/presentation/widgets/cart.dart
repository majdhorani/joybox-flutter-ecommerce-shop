import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import '../../../../core/theme/app_colors.dart';
import '../../../../di.dart';
import '../../../product/domain/entities/product.dart';
import '../../../product/presenter/widgets/product_quantity.dart';
import '../../../product/presenter/widgets/quantity_dialog.dart';
import '../cubits/cart_item_cubit.dart';
import '../cubits/cart_item_state.dart';

class HCart extends StatefulWidget {
  Product product;
  Function? onRemoved;
  Function? onUpdated;
  HCart({Key? key, required this.product, this.onRemoved, this.onUpdated}) : super(key: key);

  @override
  State<HCart> createState() => _HCartState();
}

class _HCartState extends State<HCart> {
  late int quantity;
  CartItemCubit updateCartItemCubit = sl<CartItemCubit>();
  CartItemCubit removeCartItemCubit = sl<CartItemCubit>();

  @override
  void initState() {
    super.initState();
    quantity = widget.product.quantity ?? 0;
  }

  @override
  Widget build(BuildContext context) {
    print(widget.product.toJson());
    return BlocConsumer(
      bloc: removeCartItemCubit,
      listener: (context, state) {
        if(state is CartItemLoadedState) {
          if(widget.onRemoved != null) {
            widget.onRemoved!();
          }
        }
      },
      builder: (context, state) {
        return Stack(
          children: [
            Column(
              children: [
                Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Text('${'product'.tr()} ${widget.product.name}',
                            style: const TextStyle(
                                fontSize: 12, fontWeight: FontWeight.bold)),
                      ),
                      const SizedBox(width: 10),
                      Container(
                        width: 50,
                        height: 50,
                        decoration:
                            BoxDecoration(border: Border.all(color: AppColors.grey)),
                        child: CachedNetworkImage(
                          imageUrl: widget.product.getImage(),
                          fit: BoxFit.cover,
                        ),
                      )
                    ]),
                const SizedBox(
                  height: 28,
                ),
                Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                  Text('price'.tr(),
                      style: const TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
                  Text('AED ${widget.product.salePrice}',
                      style: const TextStyle(fontSize: 12, fontWeight: FontWeight.bold))
                ]),
                const SizedBox(
                  height: 28,
                ),
                Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                  Text('quantity'.tr(),
                      style: const TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
                  BlocConsumer<CartItemCubit, CartItemState>(
                      bloc: updateCartItemCubit,
                      listener: (context, state) {
                        if (state is CartItemLoadedState) {
                          setState(() {
                            // quantity = int.parse(widget.product.quantity!);
                          });

                          if(widget.onUpdated != null) {
                            widget.onUpdated!();
                          }
                        }
                      },
                      builder: (context, state) {
                        if (state is CartItemLoadingState) {
                          return const SpinKitRipple(
                            color: AppColors.red,
                            size: 15,
                          );
                        }

                        if (state is! CartItemLoadingState) {
                          return ProductQuantity(
                            quantity: quantity,
                            onPressed: () {
                              showDialog(
                                context: context,
                                builder: (_) => QuantityDialog(
                                  onUpdate: (value) {
                                    widget.product.quantity = value;
                                    updateCartItemCubit.add(widget.product);
                                    setState(() {
                                      quantity = value;
                                    });
                                  },
                                  quantity: quantity,
                                ),
                              );
                            },
                          );
                        }
                        return const SizedBox();
                      })
                ]),
                const SizedBox(
                  height: 28,
                ),
                Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                  Text('subtotal'.tr(),
                      style: const TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
                  Text('AED ${widget.product.total()}',
                      style: const TextStyle(fontSize: 12, fontWeight: FontWeight.bold))
                ]),
              ],
            ),
            Positioned(
              right: 0,
              top: 0,
              child: InkWell(
                onTap: () {
                  if(state is CartItemLoadingState) {
                    return;
                  }

                  // Remove from wishlist
                  removeCartItemCubit.remove(widget.product.id.toString());
                },
                child: Container(
                    decoration: BoxDecoration(
                        color: AppColors.red.withOpacity(state is CartItemLoadingState ? 0.4 : 1),
                        borderRadius: BorderRadius.circular(100)
                    ),
                    child: const Icon(Icons.remove, color: Colors.white)
                ),
              ),
            )
          ],
        );
      }
    );
  }
}
