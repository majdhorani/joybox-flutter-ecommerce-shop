import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../../../../core/server/responses/calculate_response.dart';
import '../../../../core/server/responses/cart_response.dart';
import '../../../product/domain/entities/product.dart';
import '../usecases/calculate_usecase.dart';

abstract class CartRepository {
  Future<Either<Failure, CartResponse>> cart();
  Future<bool> cached({required String id});
  Future<Either<Failure, bool>> addToCart(
      {required Product product});
  Future<Either<Failure, bool>> removeFromCart({required String id});
  Future<Either<Failure, CalculateResponse>> calculate({required CalculateParams params});
}
