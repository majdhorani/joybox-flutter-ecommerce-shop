import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';
import '../repositories/cart_repository.dart';

class RemoveFromCartUseCase extends UseCase<bool, RemoveFromCartParams> {
  CartRepository repository;
  RemoveFromCartUseCase({required this.repository});

  @override
  Future<Either<Failure, bool>> call(RemoveFromCartParams params) {
    return repository.removeFromCart(
      id: params.id,
    );
  }
}

class RemoveFromCartParams extends Equatable {
  String id;
  RemoveFromCartParams({required this.id}) : super();

  @override
  List<Object?> get props => [id];
}
