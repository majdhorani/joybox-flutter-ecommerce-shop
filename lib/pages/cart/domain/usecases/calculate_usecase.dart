import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';
import '../../../../core/server/responses/calculate_response.dart';
import '../../../product/domain/entities/product.dart';
import '../repositories/cart_repository.dart';

class CalculateUseCase extends UseCase<CalculateResponse, CalculateParams> {
  CartRepository repository;
  CalculateUseCase({required this.repository});

  @override
  Future<Either<Failure, CalculateResponse>> call(CalculateParams params) {
    return repository.calculate(params: params);
  }
}

class CalculateParams extends Equatable {
  List<Product> products;
  String? coupon;
  CalculateParams({required this.products, required this.coupon}) : super();

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['products'] = products
        .map((product) => {
              'quantity': product.quantity,
              'id': product.id,
            })
        .toList();
    if (coupon != null) {
      data['coupon'] = coupon;
    }
    return data;
  }

  @override
  List<Object?> get props => [products, coupon];
}
