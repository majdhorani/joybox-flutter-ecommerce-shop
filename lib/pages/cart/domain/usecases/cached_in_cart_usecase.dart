import 'package:equatable/equatable.dart';
import '../repositories/cart_repository.dart';

class CachedInCartUseCase {
  CartRepository repository;
  CachedInCartUseCase({required this.repository});

  Future<bool> call(CachedInCartParams params) {
    return repository.cached(id: params.id);
  }
}

class CachedInCartParams extends Equatable {
  String id;
  CachedInCartParams({required this.id}) : super();

  @override
  List<Object?> get props => [];
}
