import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';
import '../../../product/domain/entities/product.dart';
import '../repositories/cart_repository.dart';

class AddToCartUseCase extends UseCase<bool, AddToCartParams> {
  CartRepository repository;
  AddToCartUseCase({required this.repository});

  @override
  Future<Either<Failure, bool>> call(AddToCartParams params) {
    return repository.addToCart(product: params.product);
  }
}

class AddToCartParams extends Equatable {
  Product product;
  AddToCartParams({required this.product}) : super();

  @override
  List<Object?> get props => [product];
}
