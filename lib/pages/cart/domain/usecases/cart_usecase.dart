import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';
import '../../../../core/server/responses/cart_response.dart';
import '../repositories/cart_repository.dart';

class CartUseCase extends UseCase<CartResponse, CartParams> {
  CartRepository repository;
  CartUseCase({required this.repository});

  @override
  Future<Either<Failure, CartResponse>> call(CartParams params) {
    return repository.cart();
  }
}

class CartParams extends Equatable {
  const CartParams() : super();

  @override
  List<Object?> get props => [];
}
