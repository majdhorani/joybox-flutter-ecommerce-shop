import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:iconsax/iconsax.dart';
import 'package:spirit/core/widgets/button.dart';
import 'package:spirit/core/widgets/images_switcher.dart';
import 'package:spirit/routes/routes.dart';

class StartPage extends StatefulWidget {
  const StartPage({super.key});

  @override
  State<StartPage> createState() => _StartPageState();
}

class _StartPageState extends State<StartPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(children: [
        Expanded(
            child: Stack(
          children: [
            SizedBox(
                height: double.infinity,
                child: ImagesSwitcher(images: const [
                  'assets/images/get_started_1.jpeg',
                  'assets/images/get_started_2.jpeg',
                ])),
            SizedBox(
              height: double.infinity,
              width: double.infinity,
              child: DecoratedBox(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: FractionalOffset.topCenter,
                    end: FractionalOffset.bottomCenter,
                    colors: [
                      Colors.black.withOpacity(0.8),
                      Colors.black.withOpacity(0.6),
                      Colors.black.withOpacity(0.8),
                      Colors.black,
                    ],
                  ),
                ),
              ),
            ),
            Container(
              width: double.infinity,
              padding:
                  const EdgeInsets.symmetric(vertical: 100, horizontal: 32),
              child: const Column(
                children: [
                  Text(
                    "JOYBOX",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.yellow,
                        fontSize: 34,
                        fontWeight: FontWeight.w800,
                        fontFamily: "Oswald"),
                  )
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  // const Divider(height: 1, color: Colors.grey),
                  Container(
                      // color: Colors.white,
                      padding: EdgeInsets.only(
                          left: 32,
                          right: 32,
                          top: 16,
                          bottom: MediaQuery.of(context).padding.bottom + 16),
                      width: double.infinity,
                      child: HButton(
                          title: 'get_started'.tr(),
                          textColor: Colors.black,
                          backgroundColor: Colors.yellow,
                          iconData: Iconsax.arrow_right_1,
                          iconAlignmentRight: true,
                          iconColor: Colors.black,
                          textBold: true,
                          onPressed: () {
                            Navigator.pushNamed(context, Routes.routeRegister);
                          }))
                ],
              ),
            )
          ],
        )),
      ]),
    );
  }
}
