import 'package:dio/dio.dart';
import '../../../../core/errors/exceptions.dart';
import '../../../../core/server/configurations.dart';
import '../../domain/entities/product.dart';

abstract class ProductRemoteDataSource {
  Future<Product?> product(String id);
}

class ProductRemoteDataSourceImpl implements ProductRemoteDataSource {
  Dio client;
  ProductRemoteDataSourceImpl({required this.client});

  @override
  Future<Product?> product(String id) async {
    try {
      var response = await client.get('${ServerConfigurations.product}?id=$id');
      print('Product Response');
      print(response.data["is_available"]);
      Product product = Product.fromJson(response.data);
      print("3 ${product.isAvailable}");
      return product;
    } catch (e) {
      if (e is DioError) {
        // Status Code
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }

}
