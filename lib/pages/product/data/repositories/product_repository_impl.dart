import 'package:dartz/dartz.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/network/network_info.dart';
import 'package:spirit/pages/product/data/datasources/product_remote_datasource.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';
import '../../../../core/errors/exceptions.dart';
import '../../domain/repositories/product_repository.dart';
import '../datasources/product_local_datasource.dart';

class ProductRepositoryImpl implements ProductRepository {
  ProductRemoteDataSource remoteDataSource;
  ProductLocalDataSource localDataSource;
  NetworkInfo networkInfo;
  ProductRepositoryImpl(
      {required this.remoteDataSource,
      required this.localDataSource,
      required this.networkInfo});

  @override
  Future<Either<Failure, Product>> product({required String id}) async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.product(id);
        print('right');
        return Right(response!);
      } catch (e) {
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }

}
