import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../core/widgets/rtl.dart';

class HProductAppBar extends StatefulWidget implements PreferredSizeWidget {
  Function onShare;
  HProductAppBar({Key? key, required this.onShare}) : super(key: key);

  @override
  State<HProductAppBar> createState() => _HProductAppBarState();

  @override
  Size get preferredSize => const Size.fromHeight(68);
}

class _HProductAppBarState extends State<HProductAppBar> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: MediaQuery.of(context).padding.top + 10),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32),
          child: Column(
            children: [
              SizedBox(
                height: 47,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      child: HRtl(
                        child: SvgPicture.asset(
                          'assets/svg/back.svg',
                          height: 23,
                        ),
                      ),
                      onTap: () {
                        Navigator.maybePop(context);
                      },
                    ),
                    // const SizedBox(
                    //   width: 0,
                    // ),
                    // InkWell(
                    //     child: const Icon(
                    //       Icons.ios_share_outlined,
                    //       size: 25,
                    //     ),
                    //     onTap: () {
                    //       widget.onShare();
                    //     }),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              const Divider(height: 0, thickness: 1, color: Color(0xFFDADADA))
            ],
          ),
        )
      ],
    );
  }
}
