import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../core/theme/app_colors.dart';
import '../../../../core/widgets/button.dart';

class QuantityDialog extends StatefulWidget {
  Function(int value) onUpdate;
  int quantity;
  QuantityDialog({Key? key, required this.quantity, required this.onUpdate})
      : super(key: key);

  @override
  State<QuantityDialog> createState() => _QuantityDialogState();
}

class _QuantityDialogState extends State<QuantityDialog> {
  late int quantity;

  @override
  void initState() {
    super.initState();
    quantity = widget.quantity;
  }

  void increase() {
    setState(() {
      quantity++;
    });
  }

  void decrease() {
    if (quantity > 1) {
      setState(() {
        quantity--;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(32.0))),
      contentPadding: const EdgeInsets.only(top: 5.0),
      content: Container(
        height: 240,
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              width: double.infinity,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text('quantity'.tr(),
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16),
                        textAlign: TextAlign.center),
                    Text('select_the_quantity_you_would_like_to_buy'.tr(),
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 12),
                        textAlign: TextAlign.center),
                    const SizedBox(height: 20),
                    Row(children: [
                      SizedBox(
                          width: 60,
                          child: HButton(
                              title: '-',
                              backgroundColor: AppColors.black,
                              rounded: true,
                              onPressed: decrease)),
                      Expanded(child: Center(child: Text(quantity.toString()))),
                      SizedBox(
                          width: 60,
                          child: HButton(
                              backgroundColor: AppColors.black,
                              title: '+',
                              rounded: true,
                              onPressed: increase)),
                    ]),
                  ]),
            ),
            const Divider(
              height: 30,
              color: AppColors.grey400,
            ),
            HButton(
                title: 'apply'.tr(),
                rounded: true,
                onPressed: () {
                  Navigator.pop(context);
                  widget.onUpdate(quantity);
                })
          ],
        ),
      ),
    );
  }
}
