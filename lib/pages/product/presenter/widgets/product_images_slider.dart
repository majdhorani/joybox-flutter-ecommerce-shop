import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:spirit/core/theme/app_colors.dart';

class ProductImagesSlider extends StatefulWidget {
  List<String> images;
  ProductImagesSlider({Key? key, required this.images}) : super(key: key);

  @override
  State<ProductImagesSlider> createState() => _ProductImagesSliderState();
}

class _ProductImagesSliderState extends State<ProductImagesSlider> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
            height: 200,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: AppColors.grey300, width: 0.3)
            ),
            margin: const EdgeInsets.symmetric(horizontal: 8),
            child: CarouselSlider(
                options: CarouselOptions(
                  enlargeCenterPage: false,
                  viewportFraction: 1,
                  aspectRatio: 1.0,
                  autoPlay: false,
                  autoPlayInterval: const Duration(seconds: 5),
                  autoPlayAnimationDuration: const Duration(milliseconds: 800)
                ),
                items: widget.images.map((image) => SizedBox(
                  width: double.infinity,
                  child: CachedNetworkImage(
                    imageUrl: image,
                    fit: BoxFit.cover,
                    progressIndicatorBuilder: (context, url, downloadProgress) =>
                    const Center(child: SpinKitCircle(size: 20, color: Colors.white)),
                    errorWidget: (context, url, error) => const Icon(Icons.error),
                  ),
                )).toList()));
  }
}
