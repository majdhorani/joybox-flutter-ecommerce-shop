import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';
import 'package:spirit/pages/wishlist/presentation/cubits/wishlist_item_cubit.dart';
import 'package:spirit/pages/wishlist/presentation/cubits/wishlist_item_state.dart';
import 'package:spirit/pages/wishlist/presentation/widgets/wishlist_not_authed_dialog.dart';
import '../../../../core/session/session.dart';
import '../../../../core/theme/app_colors.dart';
import '../../../../di.dart';
import '../../../../routes/routes.dart';

class HHorizontalProduct extends StatefulWidget {
  Product product;
  Function onPressed;
  HHorizontalProduct({Key? key, required this.onPressed, required this.product})
      : super(key: key);

  @override
  State<HHorizontalProduct> createState() => _HHorizontalProductState();
}

class _HHorizontalProductState extends State<HHorizontalProduct> {
  WishlistItemCubit wishlistItemCubit = sl<WishlistItemCubit>();
  bool inWishlist = false;

  @override
  void initState() {
    super.initState();
    checkIfProductInWishlist();
  }

  void checkIfProductInWishlist() async {
    bool result = await wishlistItemCubit.cached(id: widget.product.id.toString());
    setState(() {
      inWishlist = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        AspectRatio(
          aspectRatio: 1,
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                    border: Border.all(
                        width: 1, color: AppColors.grey400.withOpacity(0.7))),
                child: CachedNetworkImage(
                  imageUrl:
                      '${widget.product.image}',
                ),
              ),
              Material(
                color: Colors.transparent,
                child: InkWell(
                    splashColor: AppColors.red.withOpacity(0.2),
                    onTap: () {
                      widget.onPressed();
                    },
                    child: Container()),
              ),
            ],
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 8,
            ),
            Text(
              widget.product.name ?? '',
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(fontSize: 12),
              maxLines: 2,
            ),
            const SizedBox(height: 4),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        if (widget.product.discount() > 0)
                          const SizedBox(height: 5),
                        if (widget.product.discount() > 0)
                          Container(
                            height: 21,
                            padding: const EdgeInsets.symmetric(
                                horizontal: 8, vertical: 3),
                            decoration: const BoxDecoration(
                                color: AppColors.green,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(6))),
                            child: Text(
                              '-${widget.product.discount()}%',
                              style: const TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                          ),
                        const SizedBox(height: 8),
                        Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text('AED ${widget.product.salePrice}',
                                  style: const TextStyle(
                                      fontSize: 12, color: AppColors.red)),
                            ]),
                        const SizedBox(height: 5),
                        Text('  AED ${widget.product.regularPrice}',
                            style: const TextStyle(
                                fontSize: 10,
                                color: AppColors.black,
                                decoration: TextDecoration.lineThrough,
                                decorationColor: AppColors.red)),
                      ]),
                ),
                const SizedBox(width: 3),
                BlocConsumer<WishlistItemCubit, WishlistItemState>(
                    bloc: wishlistItemCubit,
                    listener: (context, state) {
                      if (state is WishlistItemLoadedState) {
                        checkIfProductInWishlist();
                      }
                    },
                    builder: (context, state) {
                      return Stack(
                        alignment: Alignment.center,
                        children: [
                          if (state is WishlistItemLoadedState ||
                              state is WishlistItemInitialState ||
                              state is WishlistItemFailedState)
                            InkWell(
                                onTap: () {
                                  if (!Session.isLoggedIn()) {
                                    showDialog(
                                      context: context,
                                      builder: (_) => WishlistNotAuthedDialog(
                                        onLogin: () {
                                          Navigator.pushNamed(
                                              context, Routes.routeLogin);
                                        },
                                      ),
                                    );
                                    return;
                                  }

                                  if (!inWishlist) {
                                    wishlistItemCubit.add(widget.product);
                                  } else {
                                    wishlistItemCubit.remove(widget.product.id.toString());
                                  }
                                },
                                child: SvgPicture.asset(
                                    'assets/svg/nav_wishlist.svg',
                                    width: 15,
                                    height: 15,
                                    color: inWishlist ? AppColors.red : AppColors.black
                                )),
                          if (state is WishlistItemLoadingState)
                            const SpinKitRipple(
                              color: AppColors.red,
                              size: 15,
                            )
                        ],
                      );
                    })
              ],
            ),
          ],
        )
      ],
    );
  }
}
