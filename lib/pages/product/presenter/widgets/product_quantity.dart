import 'package:flutter/material.dart';

import '../../../../core/theme/app_colors.dart';

class ProductQuantity extends StatefulWidget {
  int quantity;
  Function onPressed;

  ProductQuantity({super.key, required this.quantity, required this.onPressed});

  @override
  State<ProductQuantity> createState() => _ProductQuantityState();
}

class _ProductQuantityState extends State<ProductQuantity> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(50),
      onTap: () {
        widget.onPressed();
      },
      child: Container(
        width: 56,
        height: 56,
        decoration: BoxDecoration(
            border: Border.all(color: AppColors.buttonBorder, width: 2),
            borderRadius: BorderRadius.circular(8)),
        child: Center(child: Text(widget.quantity.toString())),
      ),
    );
  }
}
