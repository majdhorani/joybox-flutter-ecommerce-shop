import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:iconsax/iconsax.dart';
import 'package:spirit/core/widgets/general_app_bar.dart';
import 'package:spirit/core/widgets/header.dart';
import 'package:spirit/pages/cart/presentation/cubits/cart_item_cubit.dart';
import 'package:spirit/pages/cart/presentation/cubits/cart_item_state.dart';
import 'package:spirit/pages/home/presentation/widgets/product_vertical.dart';
import 'package:spirit/pages/product/domain/entities/attribute.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';
import 'package:spirit/pages/product/presenter/cubits/product_cubit.dart';
import 'package:spirit/pages/product/presenter/cubits/product_state.dart';
import 'package:spirit/pages/wishlist/presentation/cubits/wishlist_item_cubit.dart';
import 'package:spirit/pages/wishlist/presentation/cubits/wishlist_item_state.dart';
import 'package:spirit/pages/wishlist/presentation/widgets/wishlist_not_authed_dialog.dart';
import '../../../../core/session/session.dart';
import '../../../../core/theme/app_colors.dart';
import '../../../../core/widgets/button.dart';
import '../../../../di.dart';
import '../../../../routes/routes.dart';

class ProductPage extends StatefulWidget {
  String id;
  ProductPage({Key? key, required this.id}) : super(key: key);

  @override
  State<ProductPage> createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  // Cubits
  ProductCubit productCubit = sl<ProductCubit>();
  WishlistItemCubit wishlistItemCubit = sl<WishlistItemCubit>();
  CartItemCubit cartItemCubit = sl<CartItemCubit>();
  Product? product;

  // Indicate that the product exist in wishlist
  bool inWishlist = false;
  // Indicate that the product exist in cart
  bool inCart = false;
  // Quantity to be used in cart
  int quantity = 1;

  @override
  void initState() {
    super.initState();
    // Fetching product details
    productCubit.product(widget.id);
    checkIfProductInWishlist();
    checkIfProductInCart();
  }

  void checkIfProductInWishlist() async {
    bool result = await wishlistItemCubit.cached(id: product!.id.toString());
    setState(() {
      inWishlist = result;
    });
  }

  void checkIfProductInCart() async {
    bool result = await cartItemCubit.cached(id: product!.id.toString());
    print('in cart? $result');
    setState(() {
      inCart = result;
    });
  }

  Widget _buildProductImage(String image) {
    return Container(
        decoration: BoxDecoration(border: Border.all(color: AppColors.grey)),
        child: AspectRatio(
            aspectRatio: 1,
            child: CachedNetworkImage(
              imageUrl: image,
              fit: BoxFit.fitWidth,
            )));
  }

  Widget _buildAttributes(String name, List<Attribute> attributes) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '$name: ',
            style: const TextStyle(fontSize: 15),
          ),
          const SizedBox(
            height: 10,
          ),
          Wrap(
            children: [
              Text(
                attributes.map((attr) => attr.valueName).join(', '),
                style: const TextStyle(fontSize: 15),
              ),
            ],
          ),
          const Divider(
            height: 10,
          )
        ]);
  }

  Widget AddToCardBar() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (inCart)
            Container(
              color: AppColors.lightYellow,
              width: double.infinity,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Icon(
                    Icons.check,
                    size: 18,
                    color: Colors.white,
                  ),
                  const SizedBox(width: 6),
                  Padding(
                    padding: const EdgeInsets.all(2),
                    child: Text('product_added_to_cart'.tr().toUpperCase(),
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                            fontSize: 14, color: AppColors.white)),
                  ),
                ],
              ),
            ),
          if (product != null && !(product?.isAvailable ?? false))
            Container(
              color: AppColors.red,
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.all(2),
                child: Text('product_is_not_available'.tr().toUpperCase(),
                    textAlign: TextAlign.center,
                    style: const TextStyle(fontSize: 14, color: AppColors.white)),
              ),
            ),
          if ((product?.isAvailable ?? false))
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 8),
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border(
                  top: BorderSide(
                    color: Colors.grey.withOpacity(0.2),
                    width: 2.0,
                  ),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    height: 56,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('${product?.getSellingPrice()} EUR',
                            style: const TextStyle(
                                fontSize: 22,
                                fontFamily: "Oswald",
                                fontWeight: FontWeight.bold,
                                color: AppColors.black)),
                        Text('${product?.getRegularPrice()} EUR',
                            style: const TextStyle(
                                fontSize: 14,
                                fontFamily: "Oswald",
                                color: AppColors.red,
                                decorationColor: Colors.red,
                                decoration: TextDecoration.lineThrough)),
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      Row(children: [
                        SizedBox(
                            width: 160,
                            child: BlocConsumer<CartItemCubit, CartItemState>(
                                bloc: cartItemCubit,
                                listener: (context, state) {
                                  if (state is CartItemLoadedState) {
                                    checkIfProductInCart();
                                  }
                                },
                                builder: (context, state) {
                                  bool isLoading =
                                      state is CartItemLoadingState;
                                  return Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      if (!isLoading)
                                        HButton(
                                          textBold: true,
                                          title: (inCart
                                              ? 'view_cart'.tr()
                                              : 'add_to_cart'.tr()).toUpperCase(),
                                          iconData: inCart
                                              ? null
                                              : Iconsax.shopping_bag,
                                          iconColor: Colors.white,
                                          backgroundColor: Colors.black,
                                          onPressed: () {
                                            if (inCart) {
                                              // Navigate to cart
                                            } else {
                                              product?.quantity =
                                                  quantity;
                                              cartItemCubit.add(product!);
                                            }
                                          },
                                        ),
                                      if (isLoading)
                                        const SpinKitRipple(
                                          color: AppColors.red,
                                          size: 15,
                                        )
                                    ],
                                  );
                                })),
                      ]),
                    ],
                  ),
                ],
              ),
            ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: SafeArea(
        child: Material(
          child: Stack(
            children: [
              Column(
                children: [
                  SGeneralAppBar(
                    title: product?.name ?? "",
                    showBackButton: true,
                  ),
                  Expanded(
                    child: BlocConsumer(
                        bloc: productCubit,
                        listener: (context, state) {
                          if (state is ProductLoadedState) {
                            print('product ${state.product.toJson()}');
                            setState(() {
                              product = state.product;
                            });
                          }
                          if (state is ProductFailedState) {
                            setState(() {
                              product = null;
                            });
                          }
                        },
                        builder: (context, state) {
                          if (state is ProductLoadingState) {
                            return const Column(
                              children: [
                                Expanded(
                                    child:
                                        SpinKitCircle(color: AppColors.black))
                              ],
                            );
                          }
                          if (state is ProductFailedState) {
                            return Column(children: [
                              Expanded(
                                  child: Center(
                                      child: Text(state.message ??
                                          'unable_to_load_product'.tr())))
                            ]);
                          }
                          if (state is ProductLoadedState) {
                            // print("images");
                            // print(widget.product.images[0].toJson());
                            return SingleChildScrollView(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 30, right: 30, top: 20),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                        width: double.infinity,
                                        child: _buildProductImage(
                                            product!.getImage())),
                                    if (state.product.images.length > 1)
                                      const SizedBox(
                                        height: 10,
                                      ),
                                    if (state.product.images.length > 1)
                                      SizedBox(
                                        height: 120,
                                        child: ListView.separated(
                                            scrollDirection: Axis.horizontal,
                                            padding: EdgeInsets.zero,
                                            shrinkWrap: true,
                                            itemBuilder: (context, index) {
                                              String image = state.product
                                                      .images[index].image ??
                                                  '';
                                              return Stack(
                                                children: [
                                                  Container(
                                                      width: 120,
                                                      margin:
                                                          const EdgeInsets.only(
                                                              right: 10),
                                                      child: _buildProductImage(
                                                          image)),
                                                  Visibility(
                                                    visible:
                                                        state.product.image ==
                                                            state
                                                                .product
                                                                .images[index]
                                                                .image,
                                                    child: Positioned(
                                                      bottom: 0,
                                                      child: Container(
                                                          height: 3,
                                                          width: 120,
                                                          color: AppColors.red),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 120,
                                                    height: 120,
                                                    child: Material(
                                                      color: Colors.transparent,
                                                      child: InkWell(
                                                        onTap: () {
                                                          setState(() {
                                                            state.product
                                                                .image = image;
                                                          });
                                                        },
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              );
                                            },
                                            separatorBuilder: (context, index) {
                                              return const SizedBox(width: 0);
                                            },
                                            itemCount:
                                                state.product.images.length),
                                      ),
                                    const SizedBox(height: 10),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Text(
                                              state.product.name ?? '',
                                              style: const TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w500,
                                                  color: AppColors.black)),
                                        ),
                                        BlocConsumer<WishlistItemCubit,
                                                WishlistItemState>(
                                            bloc: wishlistItemCubit,
                                            listener: (context, state) {
                                              if (state
                                                  is WishlistItemLoadedState) {
                                                checkIfProductInWishlist();
                                              }
                                            },
                                            builder: (context, state) {
                                              return Stack(
                                                alignment: Alignment.center,
                                                children: [
                                                  if (state is WishlistItemLoadedState ||
                                                      state
                                                          is WishlistItemInitialState ||
                                                      state
                                                          is WishlistItemFailedState)
                                                    InkWell(
                                                        onTap: () {
                                                          if (!Session
                                                              .isLoggedIn()) {
                                                            showDialog(
                                                              context: context,
                                                              builder: (_) =>
                                                                  WishlistNotAuthedDialog(
                                                                onLogin: () {
                                                                  Navigator.pushNamed(
                                                                      context,
                                                                      Routes
                                                                          .routeLogin);
                                                                },
                                                              ),
                                                            );
                                                            return;
                                                          }
                                                          if (!inWishlist) {
                                                            wishlistItemCubit
                                                                .add(product!);
                                                          } else {
                                                            wishlistItemCubit
                                                                .remove(product!
                                                                    .id.toString());
                                                          }
                                                        },
                                                        child: SvgPicture.asset(
                                                            'assets/svg/nav_wishlist.svg',
                                                            width: 26,
                                                            height: 26,
                                                            color: inWishlist
                                                                ? AppColors.red
                                                                : AppColors
                                                                    .black)),
                                                  if (state
                                                      is WishlistItemLoadingState)
                                                    const SpinKitRipple(
                                                      color: AppColors.red,
                                                      size: 26,
                                                    )
                                                ],
                                              );
                                            })
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    // if (state.product.isAvailable)
                                    //   Row(children: [
                                    //     if (!inCart)
                                    //       ProductQuantity(
                                    //           quantity: quantity,
                                    //           onPressed: () {
                                    //             showDialog(
                                    //               context: context,
                                    //               builder: (_) =>
                                    //                   QuantityDialog(
                                    //                 onUpdate: (value) {
                                    //                   setState(() {
                                    //                     quantity = value;
                                    //                   });
                                    //                 },
                                    //                 quantity: quantity,
                                    //               ),
                                    //             );
                                    //           }),
                                    //     if (!inCart)
                                    //       const SizedBox(
                                    //         width: 10,
                                    //       ),
                                    //     SizedBox(
                                    //         width: 167,
                                    //         child: BlocConsumer<CartItemCubit,
                                    //                 CartItemState>(
                                    //             bloc: cartItemCubit,
                                    //             listener: (context, state) {
                                    //               if (state
                                    //                   is CartItemLoadedState) {
                                    //                 checkIfProductInCart();
                                    //               }
                                    //             },
                                    //             builder: (context, state) {
                                    //               bool isLoading = state
                                    //                   is CartItemLoadingState;
                                    //               return Stack(
                                    //                 alignment: Alignment.center,
                                    //                 children: [
                                    //                   if (!isLoading)
                                    //                     HButton(
                                    //                       textBold: true,
                                    //                       title: inCart
                                    //                           ? 'added_to_cart'
                                    //                               .tr()
                                    //                           : 'add_to_cart'
                                    //                               .tr(),
                                    //                       disabled: inCart,
                                    //                       iconData: Iconsax.add,
                                    //                       iconColor:
                                    //                           Colors.white,
                                    //                       backgroundColor:
                                    //                           Colors.black,
                                    //                       onPressed: () {
                                    //                         product?.quantity =
                                    //                             quantity
                                    //                                 .toString();
                                    //                         cartItemCubit
                                    //                             .add(product!);
                                    //                       },
                                    //                     ),
                                    //                   if (isLoading)
                                    //                     const SpinKitRipple(
                                    //                       color: AppColors.red,
                                    //                       size: 15,
                                    //                     )
                                    //                 ],
                                    //               );
                                    //             })),
                                    //   ]),

                                    // Container(
                                    //   padding: const EdgeInsets.all(13),
                                    //   decoration: BoxDecoration(
                                    //       border: Border.all(
                                    //           color: const Color(0xFFD9D9D9), width: 1),
                                    //       borderRadius: BorderRadius.circular(10)),
                                    //   child: Row(children: [
                                    //     const Expanded(
                                    //         child: Text(
                                    //       '4 interest - free credit card  payments of 23.25\nAED. No fees. Learn more',
                                    //       style: TextStyle(fontSize: 10),
                                    //     )),
                                    //     SizedBox(
                                    //       width: 86,
                                    //       child: Image.asset('assets/images/tabby.png'),
                                    //     )
                                    //   ]),
                                    // ),
                                    const SizedBox(height: 23),
                                    HHeader(title: 'description'.tr()),
                                    const SizedBox(height: 8),
                                    Text(product?.description?.trim() ?? '',
                                        style: const TextStyle(
                                            fontSize: 15,
                                            color: AppColors.textColor)),

                                    const SizedBox(height: 30),
                                    HHeader(title: 'specification'.tr()),
                                    const SizedBox(height: 10),
                                    Text(
                                        "${'sku'.tr()} ${product?.barcode?.toUpperCase()}",
                                        style: const TextStyle(
                                            fontSize: 15,
                                            color: AppColors.textColor)),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text('${'categories'.tr()}:  ',
                                            style: const TextStyle(
                                                fontSize: 15,
                                                color: AppColors.textColor)),
                                        Expanded(
                                          child: Wrap(
                                            children: [
                                              ...state.product.categories
                                                  .where((element) => (element
                                                          .name!.isNotEmpty &&
                                                      element.name!.isNotEmpty))
                                                  .map((category) => Text(
                                                      '${category.name!.isEmpty ? category.name : category.name}, ',
                                                      style: const TextStyle(
                                                          fontSize: 15,
                                                          color: AppColors
                                                              .textColor,
                                                          decoration:
                                                              TextDecoration
                                                                  .underline)))
                                                  .toList()
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                    ...product!
                                        .getNestedAttributes()
                                        .keys
                                        .map((key) => _buildAttributes(
                                            key,
                                            product?.getNestedAttributes()[
                                                    key] ??
                                                []))
                                        .toList(),
                                    if (product!
                                        .getNestedAttributes()
                                        .isNotEmpty)
                                      const SizedBox(height: 23),
                                    if (state.product.similar.isNotEmpty)
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          const SizedBox(height: 16),
                                          HHeader(
                                              title: 'you_may_also_like'.tr()),
                                          // Text('you_may_also_like'.tr(),
                                          //     style: const TextStyle(
                                          //         fontSize: 24,
                                          //         color: AppColors.textColor)),
                                          const SizedBox(height: 16),
                                          SizedBox(
                                            height: 280,
                                            child: ListView.builder(
                                                shrinkWrap: true,
                                                scrollDirection:
                                                    Axis.horizontal,
                                                itemCount: state
                                                    .product.similar.length,
                                                itemBuilder: (context, index) {
                                                  Product product = state
                                                      .product.similar[index];
                                                  return Container(
                                                      width: 120,
                                                      margin:
                                                          const EdgeInsetsDirectional
                                                              .only(end: 15),
                                                      child: SProductVertical(
                                                          product: product));
                                                }),
                                          ),
                                          const SizedBox(height: 80),
                                        ],
                                      ),
                                    const SizedBox(height: 10),
                                  ],
                                ),
                              ),
                            );
                          }
                          return const SizedBox();
                        }),
                  ),
                ],
              ),
              AddToCardBar()
            ],
          ),
        ),
      ),
    );
  }
}
