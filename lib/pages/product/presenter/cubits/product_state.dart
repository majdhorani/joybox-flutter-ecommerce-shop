import 'package:equatable/equatable.dart';
import '../../domain/entities/product.dart';

abstract class ProductState extends Equatable {
  const ProductState();

  @override
  List<Object> get props => [];
}

class ProductLoadingState extends ProductState {}

class ProductLoadedState extends ProductState {
  Product product;
  ProductLoadedState({required this.product});
}

class ProductFailedState extends ProductState {
  String? message;
  ProductFailedState({this.message});
}
