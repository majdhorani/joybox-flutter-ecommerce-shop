import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/pages/product/presenter/cubits/product_state.dart';
import '../../domain/usecases/product_usecase.dart';

class ProductCubit extends Cubit<ProductState> {
  ProductUseCase productUseCase;
  ProductCubit({required this.productUseCase}) : super(ProductLoadingState());

  Future product(String id) async {
    // Loading state
    emit(ProductLoadingState());

    // Fetch product
    var response = await productUseCase.call(ProductParams(id: id));
    response.fold((failure) {
      emit(ProductFailedState(message: 'Unable to load product'));
    }, (product) {
      // Loaded
      emit(ProductLoadedState(product: product));
    });
  }
}
