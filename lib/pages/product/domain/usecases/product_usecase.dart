import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';

import '../entities/product.dart';
import '../repositories/product_repository.dart';

class ProductUseCase extends UseCase<Product, ProductParams> {
  ProductRepository repository;
  ProductUseCase({required this.repository});

  @override
  Future<Either<Failure, Product>> call(ProductParams params) {
    return repository.product(id: params.id);
  }
}

class ProductParams extends Equatable {
  String id;
  ProductParams({required this.id}) : super();

  @override
  List<Object?> get props => [id];
}
