import 'package:spirit/pages/categories/domain/entities/category.dart';
import 'package:spirit/pages/home/domain/entities/image.dart';
import 'package:spirit/pages/product/domain/entities/attribute.dart';

class Product {
  int id;
  String? name;
  String? image;
  String? description;
  double? salePrice;
  double? regularPrice;
  String? barcode;
  List<Category> categories;
  List<Product> similar;
  List<Attribute> attributes;
  List<Image> images;
  bool isAvailable;

  // Used for cart
  int? quantity;

  Product(
      {required this.id,
      required this.name,
      required this.image,
      required this.description,
      required this.salePrice,
      required this.regularPrice,
      required this.categories,
      required this.similar,
      required this.attributes,
      required this.quantity,
      required this.images,
      required this.barcode,
      required this.isAvailable});

  factory Product.id(int id) {
    return Product(
        id: id,
        name: null,
        image: null,
        description: null,
        salePrice: null,
        regularPrice: null,
        categories: [],
        similar: [],
        attributes: [],
        images: [],
        isAvailable: true,
        quantity: 0,
        barcode: "");
  }

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
        id: json['id'],
        name: json['name'],
        image: json['image'],
        description: json['description'],
        salePrice: json['sale_price'],
        regularPrice: json['regular_price'],
        categories: json['categories'] == null
            ? []
            : (json['categories'] as List)
                .map((category) => Category.fromJson(category))
                .toList(),
        similar: json['similar'] == null
            ? []
            : (json['similar'] as List)
                .map((product) => Product.fromJson(product))
                .toList(),
        attributes: json['attributes'] == null
            ? []
            : (json['attributes'] as List)
                .map((attribute) => Attribute.fromJson(attribute))
                .toList(),
        images: json['images'] == null
            ? []
            : [
                Image(image: json['image'], selected: true),
                ...(json['images'] as List)
                    .map((image) => Image.fromJson(image))
                    .toList()
              ],
        quantity: json['quantity'],
        isAvailable: json['is_available'] == "1",
        barcode: json['barcode']);
  }
  
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'image': image,
      'description': description,
      'sale_price': salePrice,
      'regular_price': regularPrice,
      'categories': categories.map((e) => e.toJson()).toList(),
      'similar': similar.map((e) => e.toJson()).toList(),
      'attributes': attributes.map((e) => e.toJson()).toList(),
      'is_available': isAvailable,
      'images': [
        Image(image: image, selected: true),
        ...images.map((e) => e.toJson()).toList()
      ],
      'quantity': quantity
    };
  }

  String getImage() {
    return image ?? '';
  }

  num discount() {
    if (regularPrice == null || salePrice == null) {
      return 0;
    }

    var discount =
        100 - (salePrice! / regularPrice!) * 100;
    return discount.floor();
  }

  num total() {
    if (quantity == null) {
      return 0;
    }
    return salePrice! * quantity!;
  }

  String getRegularPrice() {
    return regularPrice.toString();
  }

  String getSellingPrice() {
    return salePrice!.toString();
  }

  Map<String, List<Attribute>> getNestedAttributes() {
    Map<String, List<Attribute>> result = {};

    for (var attribute in attributes) {
      if (attribute.attrName == null) {
        continue;
      }

      if (result.containsKey(attribute.attrName)) {
        result[attribute.attrName]?.add(attribute);
      } else {
        result[attribute.attrName!] = [attribute];
      }
    }
    return result;
  }
}