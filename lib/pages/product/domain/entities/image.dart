class Image {
  String? id;
  String? productId;
  String? image;
  bool selected = false;

  Image({
    this.id,
    this.productId,
    this.image,
    this.selected = false
  });

  Image.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['product_id'];
    image = json['image'];
    selected = false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['product_id'] = productId;
    data['image'] = image;
    return data;
  }
}
