class Attribute {
  int? id;
  String? attrName;
  String? valueName;
  String? productCount;
  List<Attribute>? attributes;
  bool isSelected = false;

  Attribute({
    this.id,
    this.attrName,
    this.valueName,
    this.productCount,
    this.attributes,
  });

  Attribute.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    attrName = json['attr_name'];
    valueName = json['value_name'];
    productCount = json['product_count'];
    attributes = [];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['attr_name'] = attrName;
    data['value_name'] = valueName;
    data['product_count'] = productCount;
    return data;
  }
}
