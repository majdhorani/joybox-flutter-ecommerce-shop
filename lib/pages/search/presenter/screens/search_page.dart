import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:spirit/core/theme/app_colors.dart';
import 'package:spirit/core/widgets/general_app_bar.dart';
import 'package:spirit/di.dart';
import 'package:spirit/pages/home/presentation/widgets/product_vertical.dart';
import 'package:spirit/pages/product/domain/entities/attribute.dart';
import 'package:spirit/pages/products/presentation/cubits/products_cubit.dart';
import 'package:spirit/pages/products/presentation/cubits/products_state.dart';
import 'package:spirit/pages/products/presentation/widget/pagination.dart';
import 'package:spirit/pages/search/presenter/widgets/search_app_bar.dart';
import '../../../products/domain/usecases/products_usecase.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  ProductsCubit productsCubit = sl<ProductsCubit>();
  String? searchKeyword;
  List<Attribute>? attributes;
  String? minPrice;
  String? maxPrice;
  int page = 0;

  @override
  void initState() {
    FlutterNativeSplash.remove();
    super.initState();
    productsCubit.products(ProductsParams(page: page));
  }

  void fetchProducts() {
    print('fetching');
    productsCubit.products(ProductsParams(
        name: searchKeyword,
        attrs: (attributes ?? []).map((attr) => attr.valueName ?? '').toList(),
        minPrice: minPrice,
        maxPrice: maxPrice,
        page: page));
  }

  String searchKeywords = "";

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      backgroundColor: Colors.white,
      child: Material(
        child: Column(
          children: [
            SGeneralAppBar(
              title: "Search",
              showBackButton: true,
            ),
            const SizedBox(height: 16),
            HSearchAppBar(onSearch: (search) {
              print("onSearch");
              setState(() {
                searchKeyword = search;
              });
              fetchProducts();
            }),
            const SizedBox(height: 16),
            // Products
            BlocConsumer(
                bloc: productsCubit,
                listener: (context, state) {},
                builder: (context, state) {
                  print('products state: $state');
                  if (state is ProductsLoadingState) {
                    return Expanded(
                        child: Center(
                      child: Container(
                          color: AppColors.white,
                          child: const SizedBox(
                              width: 30,
                              child: SpinKitCircle(
                                color: Colors.grey,
                              ))),
                    ));
                  }
      
                  if (state is ProductsLoadedState) {
                    return Expanded(
                        child: state.products.isEmpty
                            ? Center(child: Text('empty'.tr()))
                            : SingleChildScrollView(
                                child: Column(children: [
                                LayoutBuilder(builder: (context, constraints) {
                                  var columns = constraints.maxWidth ~/ 173;
                                  return GridView.builder(
                                      itemCount: state.products.length,
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      gridDelegate:
                                          SliverGridDelegateWithFixedCrossAxisCount(
                                              crossAxisCount: columns,
                                                crossAxisSpacing: 15,
                                                mainAxisSpacing: 0,
                                                childAspectRatio: (0.48)),
                                      padding: const EdgeInsets.symmetric(
                                            horizontal: 32),
                                      shrinkWrap: true,
                                      itemBuilder: (context, index) =>
                                          SProductVertical(
                                              product: state.products[index]));
                                }),
                                const SizedBox(
                                  height: 10,
                                ),
                                HPagination(
                                    pages: state.pages,
                                    page: page,
                                    totalCount: state.totalCount,
                                    onChange: (int page) {
                                      this.page = page;
                                      fetchProducts();
                                    }),
                                const SizedBox(
                                  height: 20,
                                ),
                              ])));
                  }
                  return const SizedBox();
                })
          ],
        ),
      ),
    );
  }
}
