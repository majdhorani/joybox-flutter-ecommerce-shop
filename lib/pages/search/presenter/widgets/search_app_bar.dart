import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:iconsax/iconsax.dart';
import 'package:spirit/core/utilities/debouncer.dart';
import 'package:spirit/core/widgets/textfield.dart';

class HSearchAppBar extends StatefulWidget implements PreferredSizeWidget {
  Function(String search) onSearch;
  HSearchAppBar({Key? key, required this.onSearch}) : super(key: key);

  @override
  State<HSearchAppBar> createState() => _HSearchAppBarState();
  
  @override
  Size get preferredSize => const Size.fromHeight(86);
}

class _HSearchAppBarState extends State<HSearchAppBar> {
  final debouncer = Debouncer(milliseconds: 1000);
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32),
          child: Column(
            children: [
              SizedBox(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                        child: HTextField(
                            controller: controller,
                            onSubmit: (value) {
                              widget.onSearch(value);
                            },
                            onChanged: (value) async {
                              print("onChanged");
                              debouncer.run(() {
                                widget.onSearch(value);
                              });
                            },
                            hintText: 'what_are_you_looking_for'.tr())),
                    const SizedBox(width: 16),
                    InkWell(
                        child: const Icon(Iconsax.search_normal_1),
                        onTap: () {
                          widget.onSearch(controller.text);
                        }),
                  ],
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
