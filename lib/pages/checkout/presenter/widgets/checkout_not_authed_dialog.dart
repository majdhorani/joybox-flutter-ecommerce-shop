import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../core/theme/app_colors.dart';
import '../../../../core/widgets/button.dart';

class CheckoutNotAuthedDialog extends StatefulWidget {
  Function onLogin;
  CheckoutNotAuthedDialog({Key? key, required this.onLogin}) : super(key: key);

  @override
  State<CheckoutNotAuthedDialog> createState() =>
      _CheckoutNotAuthedDialogState();
}

class _CheckoutNotAuthedDialogState extends State<CheckoutNotAuthedDialog> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(32.0))),
      contentPadding: const EdgeInsets.only(top: 10.0),
      content: Container(
        height: 300,
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              width: double.infinity,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const SizedBox(height: 20),
                    Container(
                      width: 45,
                      height: 45,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        color: AppColors.red,
                      ),
                      child: Container(
                          padding: const EdgeInsets.all(10),
                          child: SvgPicture.asset('assets/svg/nav_wishlist.svg',
                              color: Colors.white)),
                    ),
                    const SizedBox(height: 34),
                    Text(
                      'you_are_not_logged_in'.tr(),
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 12),
                    ),
                    const SizedBox(height: 34),
                    Text('wishlist_description'.tr(),
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 12),
                        textAlign: TextAlign.center),
                  ]),
            ),
            const SizedBox(height: 20),
            HButton(
                title: 'login'.tr(),
                rounded: true,
                onPressed: () {
                  Navigator.pop(context);
                  widget.onLogin();
                })
          ],
        ),
      ),
    );
  }
}
