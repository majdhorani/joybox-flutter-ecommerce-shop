import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../../routes/routes.dart';
import '../../domain/entities/tabby_currency.dart';
import '../../domain/entities/tabby_lang.dart';

class TabbyPresentationSnippet extends StatefulWidget {
  const TabbyPresentationSnippet({
    required this.price,
    required this.currency,
    required this.lang,
    this.borderColor = const Color(0xFFD6DED6),
    this.backgroundColor = const Color(0xFFFFFFFF),
    this.textColor = const Color(0xFF292929),
    Key? key,
  }) : super(key: key);
  final String price;
  final TabbyCurrency currency;
  final TabbyLang lang;
  final Color borderColor;
  final Color backgroundColor;
  final Color textColor;

  @override
  State<TabbyPresentationSnippet> createState() =>
      _TabbyPresentationSnippetState();
}

class _TabbyPresentationSnippetState extends State<TabbyPresentationSnippet> {
  late List<String> localStrings;

  final snippetWebUrls = <TabbyLang, String>{
    TabbyLang.en: 'https://checkout.tabby.ai/promos/product-page/installments/en/',
    TabbyLang.ar: 'https://checkout.tabby.ai/promos/product-page/installments/ar/',
  };

  List<String> getLocalStrings({
    required String price,
    required TabbyCurrency currency,
    required TabbyLang lang,
  }) {
    final fullPrice =
    (double.parse(price) / 4).toStringAsFixed(currency.decimals);
    if (lang == TabbyLang.ar) {
      return [
        'أو قسّمها على 4 دفعات شهرية بقيمة ',
        fullPrice,
        ' ${currency.name} ',
        'بدون رسوم أو فوائد. ',
        'لمعرفة المزيد'
      ];
    } else {
      return [
        'or 4 interest-free payments of ',
        fullPrice,
        ' ${currency.name}',
        '. ',
        'Learn more'
      ];
    }
  }

  @override
  void initState() {
    super.initState();
    localStrings = getLocalStrings(price: widget.price, currency: widget.currency, lang: widget.lang);
  }

  void openWebBrowser() {
    Navigator.pushNamed(context, Routes.routeBrowser, arguments: '${snippetWebUrls[widget.lang]}?price=${widget.price}&currency=${widget.currency.name}&source=sdk');
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: openWebBrowser,
      child: Container(
        constraints: const BoxConstraints(minWidth: 300, maxWidth: 720),
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          color: widget.backgroundColor,
          border: Border.all(
            color: widget.borderColor,
            width: 1,
          ),
          borderRadius: const BorderRadius.all(Radius.circular(4)),
        ),
        child: Row(
          children: [
            Expanded(
              child: RichText(
                text: TextSpan(
                  text: localStrings[0],
                  style: TextStyle(
                    color: widget.textColor,
                    fontFamily: widget.lang == TabbyLang.ar ? 'Arial' : 'Inter',
                    fontSize: 15,
                    height: 1.5,
                  ),
                  children: [
                    TextSpan(
                      text: localStrings[1],
                      style: const TextStyle(
                        fontFamily: 'Inter',
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    TextSpan(
                      text: localStrings[2],
                      style: const TextStyle(
                        fontFamily: 'Inter',
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    TextSpan(text: localStrings[3]),
                    TextSpan(
                      text: localStrings[4],
                      style: const TextStyle(
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsetsDirectional.only(start: 16),
              child: Image(
                image: AssetImage(
                  'assets/images/tabby-badge.png',
                ),
                width: 70,
                height: 28,
              ),
            ),
          ],
        ),
      ),
    );
  }


}