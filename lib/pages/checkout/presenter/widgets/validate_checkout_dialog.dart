import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../core/theme/app_colors.dart';
import '../../../../core/widgets/button.dart';

class ValidateCheckoutDialog extends StatefulWidget {
  String? message;
  ValidateCheckoutDialog({Key? key, required this.message}) : super(key: key);

  @override
  State<ValidateCheckoutDialog> createState() => _ValidateCheckoutDialogState();
}

class _ValidateCheckoutDialogState extends State<ValidateCheckoutDialog> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(32.0))),
      contentPadding: const EdgeInsets.only(top: 10.0),
      content: Container(
        height: 160,
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: double.infinity,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Validate'.tr(),
                            style: const TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18),
                          ),
                          const SizedBox(height: 6),
                          Text(
                            '${widget.message}'.tr(),
                            style: const TextStyle(
                                fontSize: 12),
                          ),
                          const SizedBox(height: 20),
                          Row(
                            children: [
                              Expanded(
                                  child: HButton(
                                      title: 'close'.tr(),
                                      backgroundColor: AppColors.black,
                                      rounded: true,
                                      onPressed: () {
                                        Navigator.maybePop(context);
                                      })),
                            ],
                          ),
                        ]),
                  ),
                ],
              ),
      ),
    );
  }
}
