import 'package:equatable/equatable.dart';

abstract class CheckoutState extends Equatable {
  const CheckoutState();

  @override
  List<Object> get props => [];
}

class CheckoutInitialState extends CheckoutState {}

class CheckoutLoadingState extends CheckoutState {}

class CheckoutLoadedState extends CheckoutState {
  int? orderId;
  String? paymentLink;
  CheckoutLoadedState({required this.orderId, required this.paymentLink});
}

class CheckoutFailedState extends CheckoutState {
  String message;
  CheckoutFailedState({required this.message});
}
