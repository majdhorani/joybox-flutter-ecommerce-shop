import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import '../../domain/usecases/checkout_usecase.dart';
import 'checkout_state.dart';

class CheckoutCubit extends Cubit<CheckoutState> {
  CheckoutUseCase checkoutUseCase;
  CheckoutCubit({required this.checkoutUseCase})
      : super(CheckoutInitialState());

  void checkout(CheckoutParams params) async {
    // Loading
    emit(CheckoutLoadingState());

    // Fetch products
    var response = await checkoutUseCase.call(params);
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(CheckoutFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(CheckoutFailedState(message: 'Something went wrong'));
    }, (response) {
      emit(CheckoutLoadedState(orderId: response.orderId, paymentLink: response.paymentLink));
    });
  }
}