import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import '../../../orders/domain/entities/order.dart';
import '../../domain/usecases/create_payment_link_usecase.dart';
import 'create_payment_link_state.dart';

class CreatePaymentLinkCubit extends Cubit<CreatePaymentLinkState> {
  CreatePaymentLinkUseCase createPaymentLinkUseCase;
  CreatePaymentLinkCubit({required this.createPaymentLinkUseCase})
      : super(CreatePaymentLinkInitialState());

  void createPaymentLink(MyOrder order) async {
    // Loading
    emit(CreatePaymentLinkLoadingState());

    // Fetch products
    var response = await createPaymentLinkUseCase
        .call(CreatePaymentLinkParams(order: order));
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(CreatePaymentLinkFailedState(
            message: failure.message ?? 'Something went wrong while trying to create payment link'));
      }
      // Error
      emit(CreatePaymentLinkFailedState(message: "Unable to create payment link"));
    }, (response) {
      print("payment success");
      emit(CreatePaymentLinkSuccessState(link: response.data));
    });
  }
}
