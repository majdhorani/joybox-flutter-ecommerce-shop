import 'package:equatable/equatable.dart';

abstract class CreatePaymentLinkState extends Equatable {
  const CreatePaymentLinkState();

  @override
  List<Object> get props => [];
}

class CreatePaymentLinkInitialState extends CreatePaymentLinkState {}

class CreatePaymentLinkLoadingState extends CreatePaymentLinkState {}

class CreatePaymentLinkSuccessState extends CreatePaymentLinkState {
  String? link;
  CreatePaymentLinkSuccessState({this.link});
}

class CreatePaymentLinkFailedState extends CreatePaymentLinkState {
  String message;
  CreatePaymentLinkFailedState({required this.message});
}
