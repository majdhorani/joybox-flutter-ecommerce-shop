import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/theme/app_colors.dart';
import 'package:spirit/core/widgets/button.dart';
import 'package:spirit/core/widgets/general_app_bar.dart';
import 'package:spirit/core/widgets/textfield.dart';
import 'package:spirit/pages/addresses/domain/entities/address.dart';
import 'package:spirit/pages/auth/presentation/widgets/register_dialog.dart';
import 'package:spirit/pages/cart/domain/usecases/calculate_usecase.dart';
import 'package:spirit/pages/cart/presentation/cubits/calculate_cubit.dart';
import 'package:spirit/pages/cart/presentation/cubits/calculate_state.dart';
import 'package:spirit/pages/cart/presentation/cubits/cart_cubit.dart';
import 'package:spirit/pages/cart/presentation/cubits/cart_state.dart';
import 'package:spirit/pages/checkout/domain/entities/device_type.dart';
import 'package:spirit/pages/checkout/domain/entities/payment_method.dart';
import 'package:spirit/pages/checkout/domain/usecases/checkout_usecase.dart';
import 'package:spirit/pages/checkout/presenter/cubits/checkout_cubit.dart';
import 'package:spirit/pages/checkout/presenter/cubits/checkout_state.dart';
import 'package:spirit/pages/checkout/presenter/widgets/validate_checkout_dialog.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';
import 'package:spirit/pages/recipients/domain/entities/recipient.dart';
import 'package:spirit/routes/routes.dart';
import '../../../../core/session/session.dart';
import '../../../../core/widgets/loading.dart';
import '../../../../di.dart';

class CheckoutPage extends StatefulWidget {
  String? coupon;
  CheckoutPage({Key? key, required this.coupon}) : super(key: key);

  @override
  State<CheckoutPage> createState() => _CheckoutPageState();
}

class _CheckoutPageState extends State<CheckoutPage> {
  // Cubits
  CheckoutCubit checkoutCubit = sl<CheckoutCubit>();
  CalculateCubit calculateCubit = sl<CalculateCubit>();
  CartCubit cartCubit = sl<CartCubit>();

  // Cart Products
  List<Product> products = [];

  // Controllers
  bool isEnabled = false;
  PaymentMethod paymentMethod = PaymentMethod.cod;
  bool isAgreed = false;

  // Reset indicate that user has registered from checkout screen,
  // So when reset is true, all navigation tabs will rebuild in order
  // to update those widgets which will be different when user is not logged in.
  bool reset = false;
  Address? address;
  Recipient? recipient;
  TextEditingController noteController = TextEditingController();
  String totalPrice = "";

  @override
  void initState() {
    super.initState();
    cartCubit.cart();
  }

  String? errors() {
    String errors = "";
    if (address == null) {
      errors += "Please select recipeint address.\n";
    }
    if (recipient == null) {
      errors += "Please select recipeint.\n";
    }
    return errors.trim().isEmpty ? null : errors;
  }

  void checkout() {
    String? errorMessages = errors();
    if (errorMessages != null) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (_) => ValidateCheckoutDialog(message: errorMessages));
      return;
    }
    if (!Session.isLoggedIn()) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => RegisterDialog(
          onRegistered: () {
            reset = true;
            checkoutCubit.checkout(CheckoutParams(
                products: products,
                address: address!,
                recipient: recipient!,
                paymentMethod: paymentMethod,
                deviceType:
                    Platform.isIOS ? DeviceType.ios : DeviceType.android,
                note: noteController.text,
                coupon: widget.coupon));
          },
        ),
      );
      return;
    }

    checkoutCubit.checkout(CheckoutParams(
        products: products,
        paymentMethod: paymentMethod,
        deviceType: Platform.isIOS ? DeviceType.ios : DeviceType.android,
        note: noteController.text,
        coupon: widget.coupon,
        address: address!,
        recipient: recipient!));
  }

  void validate() {
    setState(() {
      isEnabled = address != null && recipient != null && isAgreed;
    });
  }

  Widget HaveCoupon() {
    return Container(
      height: 42,
      width: double.infinity,
      color: AppColors.red,
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child: RichText(
          text: const TextSpan(children: [
            TextSpan(text: 'Have a coupon? ', style: TextStyle(fontSize: 13)),
            TextSpan(
                text: 'Click here to enter your code',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13)),
          ]),
        ),
      ),
    );
  }

  Widget SelectAddress() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("delivery_address".tr(),
                style: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Colors.black,
                    fontFamily: "Oswald"),
                textAlign: TextAlign.start),
            InkWell(
                onTap: () async {
                  var result = await Navigator.of(context, rootNavigator: true)
                      .pushNamed(Routes.routeAddresses, arguments: true);
                  if (result != null) {
                    if (result is Address) {
                      setState(() {
                        address = result;
                      });
                      validate();
                    }
                  }
                },
                child: Text(
                  address == null ? "select".tr() : "change".tr(),
                  style: const TextStyle(color: Colors.pink, fontSize: 14),
                ))
          ],
        ),
        if (address == null) const Text("no address selected "),
        if (address != null)
          Container(
            width: double.infinity,
            margin: const EdgeInsets.only(top: 8),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey.withOpacity(0.6)),
                borderRadius: BorderRadius.circular(8)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("City: ${address?.state?.trim()}",
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w400)),
                      Text("District: ${address?.district?.trim()}",
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w400)),
                      Text("Address: ${address?.address?.trim()}",
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w400)),
                      Text("Building: ${address?.building?.trim()}",
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w400)),
                      Text("Apartment: ${address?.apartment?.trim()}",
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w400)),
                      Text("Floor: ${address?.floor?.trim()}",
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w400))
                    ],
                  ),
                ),
              ],
            ),
          )
      ],
    );
  }

  Widget Note() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("note".tr(),
                style: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Colors.black,
                    fontFamily: "Oswald"),
                textAlign: TextAlign.start),
          ],
        ),
        const SizedBox(height: 4),
        HTextField(
            hintText: "Add your instructions (optional)",
            controller: noteController),
      ],
    );
  }

  Widget TermsAndConditions() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("terms_and_conditions".tr(),
                style: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Colors.black,
                    fontFamily: "Oswald"),
                textAlign: TextAlign.start),
            InkWell(
                onTap: () async {},
                child: Text(
                  "view".tr(),
                  style: const TextStyle(color: Colors.pink, fontSize: 14),
                ))
          ],
        ),
        const SizedBox(height: 4),
        InkWell(
          onTap: () {
            setState(() {
              isAgreed = !isAgreed;
            });
            validate();
          },
          child: Row(
            children: [
              SizedBox(
                width: 24,
                height: 24,
                child: Checkbox(
                    value: isAgreed,
                    onChanged: (value) {
                      setState(() {
                        isAgreed = value ?? false;
                      });
                      validate();
                    }),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: Text(
                  'i_have_read_and_agree_to the_website_t&c'.tr(),
                  style: const TextStyle(
                      fontSize: 12, fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget SelectRecipient() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("recipient_detials".tr(),
                style: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Colors.black,
                    fontFamily: "Oswald"),
                textAlign: TextAlign.start),
            InkWell(
                onTap: () async {
                  var result = await Navigator.of(context, rootNavigator: true)
                      .pushNamed(Routes.routeRecipients, arguments: true);
                  if (result != null) {
                    if (result is Recipient) {
                      setState(() {
                        recipient = result;
                      });
                      validate();
                    }
                  }
                },
                child: Text(
                  recipient == null ? "select".tr() : "change".tr(),
                  style: const TextStyle(color: Colors.pink, fontSize: 14),
                ))
          ],
        ),
        if (recipient == null) const Text("no recipient selected "),
        if (recipient != null)
          Container(
            width: double.infinity,
            margin: const EdgeInsets.only(top: 8),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey.withOpacity(0.6)),
                borderRadius: BorderRadius.circular(8)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                          "Name: ${recipient?.firstName?.trim()} ${recipient?.lastName?.trim()}",
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w400)),
                      Text("Phone : ${recipient?.phone?.trim()}",
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w400)),
                    ],
                  ),
                ),
              ],
            ),
          )
      ],
    );
  }

  Widget Payment() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("payment_method".tr(),
            style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400,
                color: Colors.black,
                fontFamily: "Oswald"),
            textAlign: TextAlign.start),
        const SizedBox(
          height: 8,
        ),
        InkWell(
          onTap: () {
            setState(() {
              paymentMethod = PaymentMethod.cod;
            });
          },
          child: Row(
            children: [
              SizedBox(
                width: 24,
                height: 24,
                child: Radio(
                    value: paymentMethod,
                    groupValue: PaymentMethod.cod,
                    onChanged: (value) {
                      if (value != null) {
                        setState(() {
                          paymentMethod = PaymentMethod.cod;
                        });
                      }
                    }),
              ),
              const SizedBox(width: 10),
              Text(
                'cash_on_delivery'.tr(),
                style:
                    const TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        const SizedBox(
          height: 2,
        ),
        InkWell(
          onTap: () {
            setState(() {
              paymentMethod = PaymentMethod.ziina;
            });
          },
          child: Row(
            children: [
              SizedBox(
                width: 24,
                height: 24,
                child: Radio(
                    value: paymentMethod,
                    groupValue: PaymentMethod.ziina,
                    onChanged: (value) {
                      if (value != null) {
                        setState(() {
                          paymentMethod = PaymentMethod.ziina;
                        });
                      }
                    }),
              ),
              const SizedBox(width: 10),
              Text(
                'debit_credit_card_payment'.tr(),
                style:
                    const TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        const SizedBox(
          height: 2,
        ),
        InkWell(
          onTap: () {
            setState(() {
              paymentMethod = PaymentMethod.tabby;
            });
          },
          child: Row(
            children: [
              SizedBox(
                width: 24,
                height: 24,
                child: Radio(
                    value: paymentMethod,
                    groupValue: PaymentMethod.tabby,
                    onChanged: (value) {
                      if (value != null) {
                        setState(() {
                          paymentMethod = PaymentMethod.tabby;
                        });
                      }
                    }),
              ),
              const SizedBox(width: 10),
              Text(
                'tabby_payments'.tr(),
                style:
                    const TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget Bill() {
    return BlocConsumer<CalculateCubit, CalculateState>(
        bloc: calculateCubit,
        listener: (context, state) {
          if (state is CalculateLoadedState) {
            setState(() {
              totalPrice = state.data.total.toString();
            });
          }
        },
        builder: (context, state) {
          if (state is CalculateLoadingState) {
            return HLoading(isWhite: true);
          }
          if (state is CalculateLoadedState) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("bill".tr(),
                        style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            color: Colors.black,
                            fontFamily: "Oswald"),
                        textAlign: TextAlign.start),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("subtotal".tr()),
                    Text("EUR ${(state.data.subtotal ?? "0")}",
                        style: const TextStyle(fontWeight: FontWeight.w500)),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("delivery".tr()),
                    Text("EUR ${(state.data.deliveryFee ?? "0")}",
                        style: const TextStyle(fontWeight: FontWeight.w500)),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("discount".tr()),
                    Text(
                        state.data.discount == null
                            ? "N/A"
                            : "EUR ${state.data.discount!}",
                        style: const TextStyle(fontWeight: FontWeight.w500)),
                    // Text(state.data.discount!),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("total".tr()),
                    Text(
                      "EUR ${state.data.total!}",
                      style: const TextStyle(fontWeight: FontWeight.w500),
                    ),
                  ],
                ),
              ],
            );
          }
          return const SizedBox();
        });
  }

  Widget _body() {
    return BlocConsumer<CartCubit, CartState>(
        bloc: cartCubit,
        listener: (context, state) {
          if (state is CartLoadedState) {
            setState(() {
              products = state.products ?? [];
            });
            if (state.products!.isNotEmpty) {
              calculateCubit.calculate(CalculateParams(
                  products: state.products!, coupon: widget.coupon));
            }
          }
        },
        builder: (context, state) {
          if (state is CartLoadingState) {
            return HLoading(
              isWhite: true,
            );
          }

          if (state is CartLoadedState) {
            return BlocConsumer<CheckoutCubit, CheckoutState>(
                bloc: checkoutCubit,
                listener: (context, state) async {
                  print("CheckoutState: $state");
                  sl<CartCubit>().cart();
                  if (state is CheckoutLoadedState) {
                    String? paymentLink = state.paymentLink;
                    if (paymentLink != null) {
                      // Redirect to NGenius payment link
                      await Navigator.of(context, rootNavigator: true).popAndPushNamed(Routes.routeSuccess);
                    }
                  }
                },
                builder: (context, state) {
                  if (state is CheckoutLoadingState) {
                    return HLoading();
                  }

                  return Expanded(
                    child: Column(
                      children: [
                        Expanded(
                          child: SingleChildScrollView(
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 30, vertical: 20),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  //HaveCoupon(),
                                  //const SizedBox(height: 33),
                                  SelectAddress(),
                                  Divider(
                                    height: 24,
                                    color: Colors.grey.withOpacity(0.4),
                                  ),
                                  SelectRecipient(),
                                  Divider(
                                    height: 24,
                                    color: Colors.grey.withOpacity(0.4),
                                  ),
                                  Note(),
                                  Divider(
                                    height: 24,
                                    color: Colors.grey.withOpacity(0.4),
                                  ),
                                  Bill(),
                                  Divider(
                                    height: 24,
                                    color: Colors.grey.withOpacity(0.4),
                                  ),
                                  Payment(),
                                  Divider(
                                    height: 24,
                                    color: Colors.grey.withOpacity(0.4),
                                  ),
                                  TermsAndConditions(),
                                  Divider(
                                    height: 24,
                                    color: Colors.grey.withOpacity(0.4),
                                  ),

                                  const SizedBox(height: 26),
                                ],
                              ),
                            ),
                          ),
                        ),
                        const Divider(height: 1),
                        Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 16, vertical: 16),
                          child: SizedBox(
                            child: HButton(
                                title:
                                    "${'pay'.tr().toUpperCase()} ($totalPrice EUR)",
                                textBold: isEnabled,
                                backgroundColor: Colors.black,
                                onPressed: () {
                                  checkout();
                                },
                                disabled: !isEnabled),
                          ),
                        ),
                      ],
                    ),
                  );
                });
          }

          return const SizedBox();
        });
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      backgroundColor: Colors.white,
      child: Material(
          child: SafeArea(
              child: Column(
        children: [
          SGeneralAppBar(
            title: "checkout".tr(),
            showBackButton: true,
          ),
          _body()
        ],
      ))),
    );
  }
}
