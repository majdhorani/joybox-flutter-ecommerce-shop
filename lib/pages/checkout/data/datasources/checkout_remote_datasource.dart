import 'dart:convert';
import 'dart:developer';
import 'package:dio/dio.dart';
import 'package:spirit/core/server/configurations.dart';
import 'package:spirit/core/server/responses/checkout_response.dart';
import '../../../../core/errors/exceptions.dart';
import '../../../../core/server/responses/payment_link_response.dart';
import '../../../../core/session/session.dart';
import '../../../orders/domain/entities/order.dart';
import '../../domain/usecases/checkout_usecase.dart';

abstract class CheckoutRemoteDataSource {
  Future<CheckoutResponse> checkout({required CheckoutParams params});
  Future<PaymentLinkResponse> createPaymentLink(MyOrder order);
}

class CheckoutRemoteDataSourceImpl implements CheckoutRemoteDataSource {
  Dio client;
  CheckoutRemoteDataSourceImpl({required this.client});

  @override
  Future<CheckoutResponse> checkout({required CheckoutParams params}) async {
    try {
      var url = ServerConfigurations.checkout;
      var token = Session.token();
      print('checkout body');
      print(params.toJson());
      print('token: $token');

      print('Checkout URL: $url');

      var response = await client.post(url,
          options: Options(headers: {"token": token}),
          data: jsonEncode(params.toJson()));
      print('Checkout Response');
      print(response.data);
      return CheckoutResponse.fromJson(response.data['data']);
    } catch (e) {
      log(e.toString());
      if (e is DioError) {
        // Status Code
        print('error in checkout');
        log(e.response.toString());
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode, message: json["message"]);
      } else {
        throw ServerException();
      }
    }
  }

  @override
  Future<PaymentLinkResponse> createPaymentLink(MyOrder order) async {
    try {
      var token = Session.token();
      var response = await client.post(ServerConfigurations.paymentLink,
          options: Options(
            headers: {"token": token},
          ),
          data: jsonEncode({'order_id': order.id}));
      print('order reference id Response');
      print(response.data);
      return PaymentLinkResponse.fromJson(response.data['data']);
    } catch (e) {
      if (e is DioError) {
        // Status Code
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode, message: json["message"]);
      } else {
        throw ServerException();
      }
    }
  }
}
