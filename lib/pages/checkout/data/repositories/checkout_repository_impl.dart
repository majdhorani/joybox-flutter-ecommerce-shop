import 'package:dartz/dartz.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/network/network_info.dart';
import 'package:spirit/core/server/responses/checkout_response.dart';
import 'package:spirit/pages/orders/domain/entities/order.dart';
import '../../../../core/errors/exceptions.dart';
import '../../../../core/server/responses/payment_link_response.dart';
import '../../../cart/data/datasources/cart_local_datasource.dart';
import '../../domain/repositories/checkout_repository.dart';
import '../../domain/usecases/checkout_usecase.dart';
import '../datasources/checkout_local_datasource.dart';
import '../datasources/checkout_remote_datasource.dart';

class CheckoutRepositoryImpl implements CheckoutRepository {
  CheckoutRemoteDataSource remoteDataSource;
  CheckoutLocalDataSource localDataSource;
  CartLocalDataSource cartLocalDataSource;
  NetworkInfo networkInfo;
  CheckoutRepositoryImpl(
      {required this.remoteDataSource,
      required this.localDataSource,
      required this.cartLocalDataSource,
      required this.networkInfo});

  @override
  Future<Either<Failure, CheckoutResponse>> checkout(
      {required CheckoutParams params}) async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.checkout(params: params);
        await cartLocalDataSource.clear();
        return Right(response);
      } catch (e) {
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }

  @override
  Future<Either<Failure, PaymentLinkResponse>> createPaymentLink(
      {required MyOrder order}) async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.createPaymentLink(order);
        return Right(response);
      } catch (e) {
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }
}
