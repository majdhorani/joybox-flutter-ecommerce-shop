import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/server/responses/checkout_response.dart';
import 'package:spirit/core/usecases/usecase.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';
import 'package:spirit/pages/addresses/domain/entities/address.dart';
import 'package:spirit/pages/recipients/domain/entities/recipient.dart';
import '../entities/device_type.dart';
import '../entities/payment_method.dart';
import '../repositories/checkout_repository.dart';

class CheckoutUseCase extends UseCase<CheckoutResponse, CheckoutParams> {
  CheckoutRepository repository;
  CheckoutUseCase({required this.repository});

  @override
  Future<Either<Failure, CheckoutResponse>> call(CheckoutParams params) {
    return repository.checkout(params: params);
  }
}

class CheckoutParams extends Equatable {
  List<Product> products;
  Address address;
  Recipient recipient;
  PaymentMethod paymentMethod;
  DeviceType deviceType;
  String note;
  String? coupon;

  CheckoutParams(
      {required this.products,
      required this.address,
      required this.recipient,
      required this.paymentMethod,
      required this.deviceType,
      required this.note,
      required this.coupon})
      : super();

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['products'] = products
        .map((product) => {
              'quantity': product.quantity,
              'id': product.id,
            })
        .toList();
    data['address'] = address.toJson();
    data['recipient'] = recipient.toJson();
    data['payment_method'] = paymentMethod.name.toLowerCase();
    data['order_from'] = deviceType.name;
    data['note'] = note;
    data['coupon'] = coupon;
    return data;
  }

  @override
  List<Object?> get props => [
        products,
        address,
        recipient,
        paymentMethod,
        deviceType,
        note,
        coupon
      ];
}
