import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';
import '../../../../core/server/responses/payment_link_response.dart';
import '../../../orders/domain/entities/order.dart';
import '../repositories/checkout_repository.dart';

class CreatePaymentLinkUseCase
    extends UseCase<PaymentLinkResponse, CreatePaymentLinkParams> {
  CheckoutRepository repository;
  CreatePaymentLinkUseCase({required this.repository});

  @override
  Future<Either<Failure, PaymentLinkResponse>> call(
      CreatePaymentLinkParams params) {
    return repository.createPaymentLink(order: params.order);
  }
}

class CreatePaymentLinkParams extends Equatable {
  MyOrder order;

  CreatePaymentLinkParams({
    required this.order,
  }) : super();

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};

    return data;
  }

  @override
  List<Object?> get props => [order];
}
