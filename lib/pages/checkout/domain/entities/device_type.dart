enum DeviceType {
  ios, android, web
}

extension DeviceTypeExtension on DeviceType {
  String get name {
    switch(this) {
      case DeviceType.ios:
        return 'ios';
      case DeviceType.android:
        return 'android';
      case DeviceType.web:
        return 'web';
    }
  }
}