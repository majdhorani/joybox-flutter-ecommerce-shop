enum TabbyCurrency { aed, sar, kwd, bdh, qar }
extension CurrencyExt on TabbyCurrency {
  String get name {
    switch (this) {
      case TabbyCurrency.aed:
        return 'AED';
      case TabbyCurrency.sar:
        return 'SAR';
      case TabbyCurrency.kwd:
        return 'KWD';
      case TabbyCurrency.bdh:
        return 'BDH';
      case TabbyCurrency.qar:
        return 'QAR';
    }
  }

  int get decimals {
    switch (this) {
      case TabbyCurrency.aed:
        return 2;
      case TabbyCurrency.sar:
        return 2;
      case TabbyCurrency.kwd:
        return 3;
      case TabbyCurrency.bdh:
        return 3;
      case TabbyCurrency.qar:
        return 2;
    }
  }
}