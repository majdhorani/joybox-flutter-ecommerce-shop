enum PaymentMethod {
  cod, bank, tabby, tamara, bankTransfer, paypal, ziina, stripe
}

extension PaymentMethodExtension on PaymentMethod {
  String get name {
    switch(this) {
      case PaymentMethod.cod:
        return 'cod';
      case PaymentMethod.bank:
        return 'bank';
      case PaymentMethod.tabby:
        return 'tabby';
      case PaymentMethod.tamara:
        return 'tamara';
      case PaymentMethod.bankTransfer:
        return 'bankTransfer';
      case PaymentMethod.paypal:
        return 'paypal';
      case PaymentMethod.ziina:
        return 'ziina';
        case PaymentMethod.stripe:
        return 'stripe';
    }
  }
}