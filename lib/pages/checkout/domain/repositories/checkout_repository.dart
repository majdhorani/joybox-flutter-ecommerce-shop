import 'package:dartz/dartz.dart';
import 'package:spirit/core/server/responses/checkout_response.dart';

import '../../../../core/errors/failures.dart';
import '../../../../core/server/responses/payment_link_response.dart';
import '../../../orders/domain/entities/order.dart';
import '../usecases/checkout_usecase.dart';

abstract class CheckoutRepository {
  Future<Either<Failure, CheckoutResponse>> checkout({required CheckoutParams params});
  Future<Either<Failure, PaymentLinkResponse>> createPaymentLink(
      {required MyOrder order});
}
