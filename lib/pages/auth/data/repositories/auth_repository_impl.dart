import 'package:dartz/dartz.dart';
import 'package:spirit/core/errors/exceptions.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/network/network_info.dart';
import '../../../../core/server/responses/login_response.dart';
import '../../../../core/server/responses/register_response.dart';
import '../../../../core/session/session.dart';
import '../../domain/repositories/auth_repository.dart';
import '../datasources/auth_local_datasource.dart';
import '../datasources/auth_remote_datasource.dart';

class AuthRepositoryImpl implements AuthRepository {
  AuthRemoteDataSource remoteDataSource;
  AuthLocalDataSource localDataSource;
  NetworkInfo networkInfo;
  AuthRepositoryImpl({required this.remoteDataSource, required this.localDataSource, required this.networkInfo});

  @override
  Future<Either<Failure, LoginResponse>> login({required String username, required String password}) async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.login(
            username: username, password: password);

        await Session.setUser(response.user!);
        await Session.setToken(response.user!.token!);
        await Session.setIsLoggedIn(true);
        return Right(response);
      } catch (e) {
        print(e);
        if (e is ServerException) {
          return Left(ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }

  @override
  Future<Either<Failure, RegisterResponse>> register({required String username, required String password}) async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.register(
            username: username, password: password);
        await Session.setUser(response.user!);
        await Session.setToken(response.user!.token!);
        await Session.setIsLoggedIn(true);
        return Right(response);
      } catch (e) {
        if (e is ServerException) {
          return Left(ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }
}