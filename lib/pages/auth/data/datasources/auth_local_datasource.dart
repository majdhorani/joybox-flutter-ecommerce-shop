
import '../../../../core/session/session.dart';

abstract class AuthLocalDataSource {
  bool isLoggedIn();
}

class AuthLocalDataSourceImpl implements AuthLocalDataSource {
  @override
  bool isLoggedIn() {
    return Session.isLoggedIn();
  }
}