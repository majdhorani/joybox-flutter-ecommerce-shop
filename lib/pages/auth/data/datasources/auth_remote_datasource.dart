

import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:spirit/core/errors/exceptions.dart';
import '../../../../core/server/responses/login_response.dart';
import '../../../../core/server/responses/register_response.dart';

abstract class AuthRemoteDataSource {
  Future<LoginResponse> login({required String username, required String password});
  Future<RegisterResponse> register({required String username, required String password});
}

class AuthRemoteDataSourceImpl implements AuthRemoteDataSource {
  Dio client;
  AuthRemoteDataSourceImpl({required this.client});

  @override
  Future<LoginResponse> login({required String username, required String password}) async {
    try {
      String jsonString = await rootBundle.loadString('assets/json/login.json');
      Map<String, dynamic> jsonData = jsonDecode(jsonString);
      var result = LoginResponse.fromJson(jsonData);
      return result;
    } catch (e) {
      if (e is DioError) {
        // Status Code
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode, message: json['message']);
      } else {
        throw ServerException();
      }
    }
  }

  @override
  Future<RegisterResponse> register({required String username, required String password}) async {
    try {
      String jsonString = await rootBundle.loadString('assets/json/register.json');
      Map<String, dynamic> jsonData = jsonDecode(jsonString);
      print(jsonData);
      var result = RegisterResponse.fromJson(jsonData);
      return result;
    } catch (e) {
      print('register error $e');
      if (e is DioError) {
        // Status Code
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode, message: json['message']);
      } else {
        throw ServerException();
      }
    }
  }
}