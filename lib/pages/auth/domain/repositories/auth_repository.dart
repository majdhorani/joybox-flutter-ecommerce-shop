import 'package:dartz/dartz.dart';
import 'package:spirit/core/errors/failures.dart';
import '../../../../core/server/responses/login_response.dart';
import '../../../../core/server/responses/register_response.dart';

abstract class AuthRepository {
  Future<Either<Failure,LoginResponse>> login({required String username, required String password});
  Future<Either<Failure,RegisterResponse>> register({required String username, required String password});
}