class User {
  int? id;
  String? name;
  String? username;
  String? email;
  String? token;

  User({
    this.id,
    this.name,
    this.username,
    this.email,
    this.token
  });

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    username = json['username'];
    email = json['email'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['username'] = username;
    data['email'] = email;
    data['token'] = token;
    return data;
  }
}
