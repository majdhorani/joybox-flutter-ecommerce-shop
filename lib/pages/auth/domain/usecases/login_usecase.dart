import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/server/responses/login_response.dart';
import 'package:spirit/core/usecases/usecase.dart';

import '../repositories/auth_repository.dart';

class LoginUseCase extends UseCase<LoginResponse, LoginParams> {
  AuthRepository repository;
  LoginUseCase({required this.repository});

  @override
  Future<Either<Failure, LoginResponse>> call(LoginParams params) {
    return repository.login(username: params.email, password: params.password);
  }
}

class LoginParams extends Equatable {
  String email;
  String password;
  LoginParams({required this.email, required this.password}) : super();

  @override
  List<Object?> get props => [email, password];
}
