import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';
import '../../../../core/server/responses/register_response.dart';
import '../repositories/auth_repository.dart';

class RegisterUseCase extends UseCase<RegisterResponse, RegisterParams> {
  AuthRepository repository;
  RegisterUseCase({required this.repository});

  @override
  Future<Either<Failure, RegisterResponse>> call(RegisterParams params) {
    return repository.register(username: params.email, password: params.password);
  }
}

class RegisterParams extends Equatable {
  String email;
  String password;
  RegisterParams({required this.email, required this.password}) : super();

  @override
  List<Object?> get props => [email, password];
}
