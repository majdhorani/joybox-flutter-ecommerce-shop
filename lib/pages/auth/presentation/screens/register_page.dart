import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/widgets/button.dart';
import 'package:spirit/core/widgets/error_message.dart';
import 'package:spirit/core/widgets/loading.dart';
import 'package:spirit/core/widgets/text_divider.dart';
import 'package:spirit/core/widgets/textfield.dart';
import 'package:spirit/pages/auth/presentation/cubits/register_cubit.dart';
import 'package:spirit/routes/routes.dart';
import '../../../../di.dart';
import '../cubits/register_state.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  // Controllers
  var emailController = TextEditingController();
  var passwordController = TextEditingController();
  var confirmPasswordController = TextEditingController();

  // Errors
  String? emailError;
  String? passwordError;

  // Cubits
  RegisterCubit cubit = sl<RegisterCubit>();

  void validate() {
    setState(() {
      emailError = null;
      passwordError = null;

      if (!passwordsMatches()) {
        passwordError = "please_confirm_your_password".tr();
      }

      if (!isEmailValid(emailController.text)) {
        emailError = "please_enter_valid_email".tr();
      }

      if (passwordController.text.length < 8) {
        passwordError = "minimum_password_length_is_8".tr();
      }
    });
  }

  bool passwordsMatches() {
    return passwordController.text.length ==
        confirmPasswordController.text.length;
  }

  bool isEmailValid(String email) {
    return RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email);
  }

  bool isPasswordCompliant(String password, [int minLength = 6]) {
    if (password.isEmpty) {
      return false;
    }

    bool hasUppercase = password.contains(RegExp(r'[A-Z]'));
    bool hasDigits = password.contains(RegExp(r'[0-9]'));
    bool hasLowercase = password.contains(RegExp(r'[a-z]'));
    bool hasSpecialCharacters =
        password.contains(RegExp(r'[!@#$%^&*(),.?":{}|<>]'));
    bool hasMinLength = password.length > minLength;

    return hasDigits &
        hasUppercase &
        hasLowercase &
        hasSpecialCharacters &
        hasMinLength;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: BlocConsumer<RegisterCubit, RegisterState>(
          bloc: cubit,
          listener: (context, state) async {
            if (state is RegisterSuccessState) {
              Navigator.pushNamedAndRemoveUntil(
                  context, Routes.routeDashboard, (route) => false);
            }

            if (state is RegisterFailedState) {
              // Fluttertoast.showToast(
              //     msg: state.message ?? 'could_not_register_please_try_again'.tr(),
              //     toastLength: Toast.LENGTH_LONG,
              //     gravity: ToastGravity.BOTTOM,
              //     timeInSecForIosWeb: 1,
              //     backgroundColor: AppColors.red,
              //     textColor: Colors.white,
              //     fontSize: 16.0);
            }
          },
          builder: (context, state) {
            return Stack(
              children: [
                SafeArea(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Expanded(
                        child: SingleChildScrollView(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 32, right: 32, top: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "Create an account",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 28,
                                      fontFamily: "Oswald"),
                                ),
                                Text(
                                  'register_description'.tr(),
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black.withOpacity(0.6),
                                      fontFamily: "Oswald"),
                                ),
                                const SizedBox(height: 35),
                                if (state is RegisterFailedState)
                                  ErrorMessage(
                                      message: state.message ??
                                          'could_not_register_please_try_again'
                                              .tr()),
                                HTextField(
                                  title: 'email'.tr(),
                                  controller: emailController,
                                  error: emailError,
                                ),
                                const SizedBox(height: 14),
                                HTextField(
                                  title: 'password'.tr(),
                                  controller: passwordController,
                                  error: passwordError,
                                ),
                                const SizedBox(height: 14),
                                HTextField(
                                  title: 'confirm_password'.tr(),
                                  controller: confirmPasswordController,
                                ),
                                const SizedBox(height: 41),
                                HButton(
                                    backgroundColor: Colors.black,
                                    textBold: true,
                                    title: "register".tr(),
                                    onPressed: () {
                                      validate();
                                      if (emailError == null &&
                                          passwordError == null) {
                                        cubit.register(emailController.text,
                                            passwordController.text);
                                      }
                                    }),
                                const SizedBox(height: 16),
                                HButton(
                                    backgroundColor: Colors.white,
                                    borderColor: Colors.black.withOpacity(0.3),
                                    textColor: Colors.black,
                                    textBold: true,
                                    title: "Continue As a Guest ".tr(),
                                    onPressed: () {
                                      validate();
                                      if (emailError == null &&
                                          passwordError == null) {
                                        cubit.register(emailController.text,
                                            passwordController.text);
                                      }
                                    }),
                                TextDivider(height: 64, text: "or".tr()),
                                HButton(
                                    backgroundColor: Colors.white,
                                    borderColor: Colors.black.withOpacity(0.3),
                                    textColor: Colors.black,
                                    textBold: true,
                                    assetSvg: "assets/icons/google.svg",
                                    title: "Sign Up with Google ".tr(),
                                    onPressed: () {
                                      validate();
                                      if (emailError == null &&
                                          passwordError == null) {
                                        cubit.register(emailController.text,
                                            passwordController.text);
                                      }
                                    }),
                                const SizedBox(height: 13),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: double.infinity,
                        child: Padding(
                          padding: const EdgeInsets.all(12),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              const Text(
                                "Already a member?",
                                style: TextStyle(color: Colors.grey),
                              ),
                              const SizedBox(
                                width: 8,
                              ),
                              InkWell(
                                  onTap: () {
                                    Navigator.pushNamed(
                                        context, Routes.routeLogin);
                                  },
                                  child: const Text(
                                    "Log in",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        decoration: TextDecoration.underline),
                                  ))
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                if (state is RegisterLoadingState) HLoading()
              ],
            );
          }),
    );
  }
}
