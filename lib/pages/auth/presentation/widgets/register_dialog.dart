import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../../../../core/theme/app_colors.dart';
import '../../../../core/widgets/button.dart';
import '../../../../core/widgets/loading.dart';
import '../../../../core/widgets/textfield.dart';
import '../../../../di.dart';
import '../cubits/register_cubit.dart';
import '../cubits/register_state.dart';

class RegisterDialog extends StatefulWidget {
  Function onRegistered;
  RegisterDialog({Key? key, required this.onRegistered}) : super(key: key);

  @override
  State<RegisterDialog> createState() => _RegisterDialogState();
}

class _RegisterDialogState extends State<RegisterDialog> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  // Errors
  String? emailError;
  String? passwordError;

  // Cubits
  RegisterCubit cubit = sl<RegisterCubit>();

  void validate() {
    setState(() {
      emailError = null;
      passwordError = null;

      if (!passwordsMatches()) {
        passwordError = "please_confirm_your_password".tr();
      }

      if (!isEmailValid(emailController.text)) {
        emailError = "please_enter_valid_email".tr();
      }

      if (passwordController.text.length < 8) {
        passwordError = "minimum_password_length_is_8".tr();
      }
    });
  }

  bool passwordsMatches() {
    return passwordController.text.length ==
        confirmPasswordController.text.length;
  }

  bool isEmailValid(String email) {
    return RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email);
  }

  bool isPasswordCompliant(String password, [int minLength = 6]) {
    if (password.isEmpty) {
      return false;
    }

    bool hasUppercase = password.contains(RegExp(r'[A-Z]'));
    bool hasDigits = password.contains(RegExp(r'[0-9]'));
    bool hasLowercase = password.contains(RegExp(r'[a-z]'));
    bool hasSpecialCharacters =
        password.contains(RegExp(r'[!@#$%^&*(),.?":{}|<>]'));
    bool hasMinLength = password.length > minLength;

    return hasDigits &
        hasUppercase &
        hasLowercase &
        hasSpecialCharacters &
        hasMinLength;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(32.0))),
      contentPadding: const EdgeInsets.only(top: 10.0),
      content: Container(
        height: 420,
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: BlocConsumer<RegisterCubit, RegisterState>(
            bloc: cubit,
            listener: (context, state) {
              if (state is RegisterSuccessState) {
                Navigator.maybePop(context);
                widget.onRegistered();
              }

              if (state is RegisterFailedState) {
                Fluttertoast.showToast(
                    msg: state.message ??
                        'could_not_register_please_try_again'.tr(),
                    toastLength: Toast.LENGTH_LONG,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIosWeb: 1,
                    backgroundColor: AppColors.red,
                    textColor: Colors.white,
                    fontSize: 16.0);
              }
            },
            builder: (context, state) {
              if (state is RegisterLoadingState) {
                return HLoading(isWhite: true);
              }
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: double.infinity,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'you_are_not_logged_in'.tr(),
                            style: const TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 12),
                          ),
                          Text(
                            'create_account'.tr(),
                            style: const TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 12),
                          ),
                          const SizedBox(height: 24),
                          Text(
                            'email'.tr(),
                            style: const TextStyle(
                                fontSize: 12, fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(height: 14),
                          HTextField(
                            controller: emailController,
                            error: emailError,
                          ),
                          const SizedBox(height: 14),
                          Text(
                            'password'.tr(),
                            style: const TextStyle(
                                fontSize: 12, fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(height: 14),
                          HTextField(
                            controller: passwordController,
                            error: passwordError,
                          ),
                          const SizedBox(height: 14),
                          Text(
                            'confirm_password'.tr(),
                            style: const TextStyle(
                                fontSize: 12, fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(height: 14),
                          HTextField(
                            controller: confirmPasswordController,
                          ),
                          const SizedBox(height: 20),
                          Row(
                            children: [
                              Expanded(
                                  child: HButton(
                                      title: 'register'.tr(),
                                      rounded: true,
                                      onPressed: () {
                                        validate();
                                        if (emailError == null &&
                                            passwordError == null) {
                                          cubit.register(emailController.text,
                                              passwordController.text);
                                        }
                                      })),
                              const SizedBox(width: 10),
                              Expanded(
                                  child: HButton(
                                      title: 'cancel'.tr(),
                                      backgroundColor: AppColors.black,
                                      rounded: true,
                                      onPressed: () {
                                        Navigator.maybePop(context);
                                      })),
                            ],
                          ),
                        ]),
                  ),
                ],
              );
            }),
      ),
    );
  }
}
