import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';

class HIndice extends StatelessWidget {
  String title;
  HIndice({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
            width: 24,
            height: 24,
            margin: const EdgeInsets.only(right: 10),
            child: SvgPicture.asset('assets/svg/check.svg')),
        Text(title, style: const TextStyle(fontSize: 12),)
      ],);
  }
}
