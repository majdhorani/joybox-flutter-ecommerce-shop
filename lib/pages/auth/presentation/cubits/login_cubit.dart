import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import '../../domain/usecases/login_usecase.dart';
import 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginUseCase loginUseCase;
  LoginCubit({required this.loginUseCase}) : super(LoginInitialState());

  void login(String email, String password) async {
    // Loading
    emit(LoginLoadingState());

    // Fetch branches
    var response =
        await loginUseCase.call(LoginParams(email: email, password: password));
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(LoginFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(LoginFailedState(message: 'Something went wrong'));
    }, (token) {
      // Loaded
      emit(LoginSuccessState());
    });
  }
}
