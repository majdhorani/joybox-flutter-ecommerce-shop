import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/pages/auth/presentation/cubits/register_state.dart';
import '../../domain/usecases/register_usecase.dart';


class RegisterCubit extends Cubit<RegisterState> {
  RegisterUseCase registerUseCase;
  RegisterCubit({required this.registerUseCase}) : super(RegisterInitialState());

  void register(String email, String password) async {
    // Loading
    emit(RegisterLoadingState());

    // Register user
    var response = await registerUseCase.call(RegisterParams(email: email, password: password));
    response.fold((failure) {
      // Error
      if(failure is ServerFailure) {
        emit(RegisterFailedState(message: failure.message));
      } else {
        emit(RegisterFailedState());
      }
    }, (token) {
      // Loaded
      emit(RegisterSuccessState());
    });
  }
}