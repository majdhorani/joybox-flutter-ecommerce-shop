import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WishlistPage extends StatefulWidget {
  const WishlistPage({super.key});

  @override
  State<WishlistPage> createState() => _WishlistPageState();
}

class _WishlistPageState extends State<WishlistPage> {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      // appBar: SAppBar(),
      // bottomNavigationBar: SBottomNavigationBar(),
      child: Material(
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(height: 16),
              ListView.separated(
                  itemBuilder: (context, state) {
                    return const Text('Test');
                  },
                  separatorBuilder: (context, state) {
                    return const Divider();
                  },
                  itemCount: 3),
              const SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: AspectRatio(
                  aspectRatio: 4,
                  child: Container(color: Colors.grey),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
