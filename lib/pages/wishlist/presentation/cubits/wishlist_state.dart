import 'package:equatable/equatable.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';

abstract class WishlistState extends Equatable {
  const WishlistState();

  @override
  List<Object> get props => [];
}

class WishlistInitialState extends WishlistState {}

class WishlistLoadingState extends WishlistState {}

class WishlistLoadedState extends WishlistState {
  List<Product>? products;

  WishlistLoadedState({this.products});
}

class WishlistFailedState extends WishlistState {
  String message;
  WishlistFailedState({required this.message});
}
