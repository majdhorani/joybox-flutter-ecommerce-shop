import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import '../../domain/usecases/wishlist_usecase.dart';
import 'wishlist_state.dart';

class WishlistCubit extends Cubit<WishlistState> {
  WishlistUseCase wishlistUseCase;
  WishlistCubit({
    required this.wishlistUseCase
  }) : super(WishlistInitialState());

  void wishlist() async {
    // Loading
    emit(WishlistLoadingState());

    // Fetch products
    var response = await wishlistUseCase.call(const WishlistParams());
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(WishlistFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(WishlistFailedState(message: 'Something went wrong'));
    }, (response) {
      // Loaded
      print('products size ${response.products.length}');
      emit(WishlistLoadedState(products: response.products));
    });
  }
}
