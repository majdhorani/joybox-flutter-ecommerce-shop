import 'package:equatable/equatable.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';

abstract class WishlistItemState extends Equatable {
  const WishlistItemState();

  @override
  List<Object> get props => [];
}

class WishlistItemInitialState extends WishlistItemState {}

class WishlistItemLoadingState extends WishlistItemState {}

class WishlistItemLoadedState extends WishlistItemState {
  List<Product>? products;

  WishlistItemLoadedState({this.products});
}

class WishlistItemFailedState extends WishlistItemState {
  String message;
  WishlistItemFailedState({required this.message});
}
