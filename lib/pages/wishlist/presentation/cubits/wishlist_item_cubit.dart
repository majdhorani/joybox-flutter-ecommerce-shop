import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/pages/wishlist/domain/usecases/cached_in_wishlist_usecase.dart';
import 'package:spirit/pages/wishlist/presentation/cubits/wishlist_item_state.dart';
import '../../../product/domain/entities/product.dart';
import '../../domain/usecases/add_to_wishlist_usecase.dart';
import '../../domain/usecases/remove_from_wishlist_usecase.dart';

class WishlistItemCubit extends Cubit<WishlistItemState> {
  AddToWishlistUseCase addToWishlistUseCase;
  RemoveFromWishlistUseCase removeFromWishlistUseCase;
  CachedInWishlistUseCase cachedInWishlistUseCase;
  WishlistItemCubit(
      {
        required this.addToWishlistUseCase,
        required this.removeFromWishlistUseCase,
        required this.cachedInWishlistUseCase
      })
      : super(WishlistItemInitialState());

  void add(Product product) async {
    // Loading
    emit(WishlistItemLoadingState());

    // Fetch products
    var response = await addToWishlistUseCase.call(AddToWishlistParams(product: product));
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(WishlistItemFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(WishlistItemFailedState(message: 'Something went wrong'));
    }, (response) {
      emit(WishlistItemLoadedState());
    });
  }

  void remove(String id) async {
    // Loading
    emit(WishlistItemLoadingState());

    // Fetch products
    var response =
        await removeFromWishlistUseCase.call(RemoveFromWishlistParams(id: id));
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(WishlistItemFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(WishlistItemFailedState(message: 'Something went wrong'));
    }, (response) {
      emit(WishlistItemLoadedState());
    });
  }

  Future<bool> cached({required String id}) async {
    var cached = await cachedInWishlistUseCase.call(CachedInWishlistParams(id: id));
    return cached;
  }
}
