import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../core/theme/app_colors.dart';
import '../../../../core/widgets/button.dart';
import '../../../../routes/routes.dart';

class HWishlistNotAuthed extends StatefulWidget {
  const HWishlistNotAuthed({Key? key}) : super(key: key);

  @override
  State<HWishlistNotAuthed> createState() => _HWishlistNotAuthedState();
}

class _HWishlistNotAuthedState extends State<HWishlistNotAuthed> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: double.infinity,
      child: Stack(
        children: [
          SizedBox(
            width: double.infinity,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(height: 34),
                  Container(
                    width: 45,
                    height: 45,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: AppColors.red,
                    ),
                    child: Container(
                        padding: const EdgeInsets.all(10),
                        child: SvgPicture.asset('assets/svg/nav_wishlist.svg',
                            color: Colors.white)),
                  ),
                  const SizedBox(height: 34),
                  Text(
                    'login_to_add_products_to_wishlist'.tr(),
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                  const SizedBox(height: 34),
                  Container(
                      padding: const EdgeInsets.symmetric(horizontal: 32),
                      child: Text('wishlist_description'.tr(),
                          style: const TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 12),
                          textAlign: TextAlign.center)),
                ]),
          ),
          Positioned(
              bottom: 68,
              left: 30,
              right: 30,
              child: HButton(
                  title: 'login'.tr(),
                  rounded: true,
                  onPressed: () {
                    Navigator.pushNamed(context, Routes.routeLogin);
                  }))
        ],
      ),
    );
  }
}
