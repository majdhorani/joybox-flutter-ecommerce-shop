import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../core/theme/app_colors.dart';
import '../../../../di.dart';
import '../../../../routes/routes.dart';
import '../../../product/domain/entities/product.dart';
import '../cubits/wishlist_item_cubit.dart';
import '../cubits/wishlist_item_state.dart';

class HWishlist extends StatefulWidget {
  Product product;
  Function onRemoved;
  HWishlist({Key? key, required this.product, required this.onRemoved}) : super(key: key);

  @override
  State<HWishlist> createState() => _HWishlistState();
}

class _HWishlistState extends State<HWishlist> {
  WishlistItemCubit wishlistItemCubit = sl<WishlistItemCubit>();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<WishlistItemCubit, WishlistItemState>(
      bloc: wishlistItemCubit,
      listener: (context, state) {
        if(state is WishlistItemLoadedState) {
          widget.onRemoved();
        }
      },
      builder: (context, state) {
        return Stack(
          children: [
            InkWell(
              onTap: () {
                Navigator.pushNamed(context, Routes.routeProduct,
                    arguments: widget.product);
              },
              child: Column(
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Text(widget.product.name ?? '',
                              style: const TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.bold)),
                        ),
                        const SizedBox(width: 10),
                        Container(
                          width: 50,
                          height: 50,
                          decoration:
                              BoxDecoration(border: Border.all(color: AppColors.grey)),
                          child: CachedNetworkImage(
                            imageUrl: widget.product.getImage(),
                            fit: BoxFit.cover,
                          ),
                        )
                      ]),
                  const SizedBox(
                    height: 28,
                  ),
                  Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                    Text('price'.tr(),
                        style: const TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
                    Text('AED ${widget.product.salePrice}',
                        style:
                            const TextStyle(fontSize: 12, fontWeight: FontWeight.bold))
                  ]),
                  const SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
            Positioned(
              right: 0,
              top: 0,
              child: InkWell(
                onTap: () {
                  if(state is WishlistItemLoadingState) {
                    return;
                  }

                  // Remove from wishlist
                  wishlistItemCubit.remove(widget.product.id.toString());
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: AppColors.red.withOpacity(state is WishlistItemLoadingState ? 0.4 : 1),
                    borderRadius: BorderRadius.circular(100)
                  ),
                    child: const Icon(Icons.remove, color: Colors.white)
                ),
              ),
            )
          ],
        );
      }
    );
  }
}
