import 'package:dartz/dartz.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/network/network_info.dart';
import 'package:spirit/core/server/responses/wishlist_response.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';
import '../../../../core/errors/exceptions.dart';
import '../../domain/repositories/wishlist_repository.dart';
import '../datasources/wishlist_local_datasource.dart';
import '../datasources/wishlist_remote_datasource.dart';

class WishlistRepositoryImpl implements WishlistRepository {
  WishlistRemoteDataSource remoteDataSource;
  WishlistLocalDataSource localDataSource;
  NetworkInfo networkInfo;
  WishlistRepositoryImpl(
      {required this.remoteDataSource,
      required this.localDataSource,
      required this.networkInfo});

  @override
  Future<Either<Failure, bool>> addToWishlist({required Product product}) async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.add(product);
        await localDataSource.add(product);
        return Right(response);
      } catch (e) {
        print(e);
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> removeFromWishlist({required String id}) async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.remove(id);
        await localDataSource.remove(id);
        return Right(response);
      } catch (e) {
        print(e);
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }

  @override
  Future<Either<Failure, WishlistResponse>> wishlist() async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.wishlist();
        await localDataSource.cache(response.products);
        return Right(response);
      } catch (e) {
        print(e);
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }

  @override
  Future<bool> cached({required String id}) {
    return localDataSource.cached(id);
  }
}
