import 'package:dio/dio.dart';
import 'package:spirit/core/server/configurations.dart';
import 'package:spirit/core/session/session.dart';
import '../../../../core/errors/exceptions.dart';
import '../../../../core/server/responses/wishlist_response.dart';
import '../../../product/domain/entities/product.dart';

abstract class WishlistRemoteDataSource {
  Future<bool> add(Product product);
  Future<bool> remove(String id);
  Future<WishlistResponse> wishlist();
}

class WishlistRemoteDataSourceImpl implements WishlistRemoteDataSource {
  Dio client;
  WishlistRemoteDataSourceImpl({required this.client});

  @override
  Future<bool> add(Product product) async {
    try {
      var url = ServerConfigurations.addToWishlist;
      var token = Session.token();
      print('token: $token');
      print('Add To Wishlist URL: $url');

      var response = await client.post(url,
          options: Options(
            headers: {"token": token},
          ),
          data: {'product_id': product.id});
      print('Add To Wishlist Response');
      print(response.data);
      return true;
    } catch (e) {
      if (e is DioError) {
        // Status Code
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }

  @override
  Future<bool> remove(String id) async {
    try {
      var url = ServerConfigurations.removeFromWishlist;
      var token = Session.token();
      print('token: $token');
      print('Add To Wishlist URL: $url');

      var response = await client.post(url,
          options: Options(headers: {"token": token}),
          data: {'product_id': id});
      print('Add To Wishlist Response');
      print(response.data);
      return true;
    } catch (e) {
      if (e is DioError) {
        // Status Code
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }

  @override
  Future<WishlistResponse> wishlist() async {
    try {
      var url = ServerConfigurations.wishlist;
      var token = Session.token();
      print('token: $token');
      print('Wishlist URL: $url');

      var response =
          await client.get(url, options: Options(headers: {"token": token}));
      print('Wishlist Response');
      print(response.data);
      var result = WishlistResponse.fromJson(response.data);
      return result;
    } catch (e) {
      if (e is DioError) {
        // Status Code
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }
}
