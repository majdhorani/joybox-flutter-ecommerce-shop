
import '../../../../core/session/session.dart';
import '../../../product/domain/entities/product.dart';

abstract class WishlistLocalDataSource {
  Future<bool> add(Product product);
  Future<bool> remove(String id);
  Future<bool> cached(String id);
  Future<List<Product>> wishlist();
  Future<bool> cache(List<Product> products);
}

class WishlistLocalDataSourceImpl implements WishlistLocalDataSource {
  @override
  Future<bool> add(Product product) async {
    return Session.addToeWishlist(product);
  }

  @override
  Future<bool> remove(String id) {
    return Session.removeFromWishlist(id);
  }

  @override
  Future<List<Product>> wishlist() async {
    return Session.wishlist();
  }

  @override
  Future<bool> cache(List<Product> products) {
    return Session.cacheWishlist(products);
  }

  @override
  Future<bool> cached(String id) async {
    var wishlist = Session.wishlist();
    for (var product in wishlist) {
      if(product.id == id) {
        return true;
      }
    }
    return false;
  }
}