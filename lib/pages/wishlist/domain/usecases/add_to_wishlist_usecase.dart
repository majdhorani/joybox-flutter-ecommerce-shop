import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';
import '../../../product/domain/entities/product.dart';
import '../repositories/wishlist_repository.dart';

class AddToWishlistUseCase extends UseCase<bool, AddToWishlistParams> {
  WishlistRepository repository;
  AddToWishlistUseCase({required this.repository});

  @override
  Future<Either<Failure, bool>> call(AddToWishlistParams params) {
    return repository.addToWishlist(product: params.product);
  }
}

class AddToWishlistParams extends Equatable {
  Product product;
  AddToWishlistParams({required this.product}) : super();

  @override
  List<Object?> get props => [product];
}
