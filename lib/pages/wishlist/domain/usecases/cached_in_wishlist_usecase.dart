import 'package:equatable/equatable.dart';
import '../repositories/wishlist_repository.dart';

class CachedInWishlistUseCase {
  WishlistRepository repository;
  CachedInWishlistUseCase({required this.repository});

  Future<bool> call(CachedInWishlistParams params) {
    return repository.cached(id: params.id);
  }
}

class CachedInWishlistParams extends Equatable {
  String id;
  CachedInWishlistParams({required this.id}) : super();

  @override
  List<Object?> get props => [];
}
