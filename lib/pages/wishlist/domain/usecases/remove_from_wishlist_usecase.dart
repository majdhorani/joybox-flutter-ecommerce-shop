import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';
import '../repositories/wishlist_repository.dart';

class RemoveFromWishlistUseCase
    extends UseCase<bool, RemoveFromWishlistParams> {
  WishlistRepository repository;
  RemoveFromWishlistUseCase({required this.repository});

  @override
  Future<Either<Failure, bool>> call(RemoveFromWishlistParams params) {
    return repository.removeFromWishlist(id: params.id);
  }
}

class RemoveFromWishlistParams extends Equatable {
  String id;
  RemoveFromWishlistParams({required this.id}) : super();

  @override
  List<Object?> get props => [];
}
