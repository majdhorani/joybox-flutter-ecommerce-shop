import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';
import '../../../../core/server/responses/wishlist_response.dart';
import '../repositories/wishlist_repository.dart';

class WishlistUseCase extends UseCase<WishlistResponse, WishlistParams> {
  WishlistRepository repository;
  WishlistUseCase({required this.repository});

  @override
  Future<Either<Failure, WishlistResponse>> call(WishlistParams params) {
    return repository.wishlist();
  }
}

class WishlistParams extends Equatable {
  const WishlistParams() : super();

  @override
  List<Object?> get props => [];
}
