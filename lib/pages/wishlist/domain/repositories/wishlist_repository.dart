import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../../../../core/server/responses/wishlist_response.dart';
import '../../../product/domain/entities/product.dart';

abstract class WishlistRepository {
  Future<Either<Failure,WishlistResponse>> wishlist();
  Future<bool> cached({required String id});
  Future<Either<Failure,bool>> addToWishlist({required Product product});
  Future<Either<Failure,bool>> removeFromWishlist({required String id});
}