import 'package:flutter/material.dart';
import 'package:spirit/pages/categories/domain/entities/category.dart';
import 'package:spirit/pages/categories/presentation/widgets/subcaategories_list.dart';
import 'package:spirit/pages/products/domain/usecases/products_usecase.dart';
import 'package:spirit/routes/routes.dart';

class CategoriesList extends StatelessWidget {
  List<Category> categories;
  CategoriesList({super.key, required this.categories});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: ListView.separated(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        separatorBuilder: (context, index) {
          return Divider(
            color: Colors.grey.withOpacity(0.4),
          );
        },
        itemBuilder: (context, index) {
          Category category = categories[index];
          return Column(
            children: [
              InkWell(
                onTap: () {
                  Navigator.pushNamed(context, Routes.routeProducts,
                arguments: ProductsParams(
                    categoryId: category.id.toString(), title: category.name));
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 2),
                      child: Text(category.name?.trim() ?? ""),
                    ),
                    Icon(Icons.keyboard_arrow_right,
                        size: 20, color: Colors.black.withOpacity(0.3)),
                  ],
                ),
              ),
              SubCategoriesList(category: category,)
            ],
          );
        },
        itemCount: categories.length,
      ),
    );
  }
}
