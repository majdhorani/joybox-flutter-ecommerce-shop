import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:spirit/pages/categories/domain/entities/category.dart';
import 'package:spirit/pages/products/domain/usecases/products_usecase.dart';
import 'package:spirit/routes/routes.dart';

class SubCategory extends StatefulWidget {
  Category category;
  SubCategory({super.key, required this.category});

  @override
  State<SubCategory> createState() => _SubCategoryState();
}

class _SubCategoryState extends State<SubCategory> {
  @override
  Widget build(BuildContext context) {
    print("image: ${widget.category.image}");
    return Container(
      margin: const EdgeInsets.only(right: 8, bottom: 8),
      child: InkWell(
        onTap: () {
          Navigator.pushNamed(context, Routes.routeProducts,
              arguments: ProductsParams(
                  categoryId: widget.category.id.toString(),
                  title: widget.category.name));
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.grey.withOpacity(0.8), width: 0.5),
                  borderRadius: BorderRadius.circular(4)),
              width: 50,
              height: 50,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(4),
                child: CachedNetworkImage(
                  width: 50,
                  height: 50,
                  imageUrl: widget.category.image ?? '',
                  fit: BoxFit.contain,
                  errorWidget: (context, url, error) {
                    return const SizedBox();
                  },
                ),
              ),
            ),
            const SizedBox(
              height: 4,
            ),
            SizedBox(
              width: 60,
              child: Text(
                widget.category.name?.trim() ?? "",
                textAlign: TextAlign.center,
                style: const TextStyle(fontSize: 12),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
