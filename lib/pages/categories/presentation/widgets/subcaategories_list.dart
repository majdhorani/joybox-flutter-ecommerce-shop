import 'package:flutter/material.dart';
import 'package:spirit/pages/categories/domain/entities/category.dart';
import 'package:spirit/pages/categories/presentation/widgets/subcategory.dart';

class SubCategoriesList extends StatelessWidget {
  Category category;
  SubCategoriesList({super.key, required this.category});
  
  @override
  Widget build(BuildContext context) {
    List<Category> subcategories = category.subCategories ?? [];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // if (category.subCategories!.isNotEmpty)
        //   Divider(
        //     color: Colors.grey.withOpacity(0.3),
        //   ),
        const SizedBox(height: 8),
        SizedBox(
          width: double.infinity,
          child: Wrap(
            alignment: WrapAlignment.start,
            children: [
              ...subcategories.map((Category subcategory) {
                return SubCategory(category: subcategory);
              }).toList(),
            ],
          ),
        )
      ],
    );
  }
}