import 'package:equatable/equatable.dart';

import '../../domain/entities/category.dart';

abstract class CategoriesState extends Equatable {
  const CategoriesState();

  @override
  List<Object> get props => [];
}

class CategoriesInitialState extends CategoriesState {}

class CategoriesLoadingState extends CategoriesState {}

class CategoriesLoadedState extends CategoriesState {
  List<Category> categories;
  CategoriesLoadedState({required this.categories});
}

class CategoriesFailedState extends CategoriesState {
  String message;
  CategoriesFailedState({required this.message});
}
