import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import '../../domain/usecases/categories_usecase.dart';
import 'categories_state.dart';

class CategoriesCubit extends Cubit<CategoriesState> {
  CategoriesUseCase categoriesUseCase;
  CategoriesCubit({required this.categoriesUseCase})
      : super(CategoriesInitialState());

  void categories() async {
    // Loading
    emit(CategoriesLoadingState());

    // Fetch branches
    var response = await categoriesUseCase.call(const CategoriesParams());
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(CategoriesFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(CategoriesFailedState(message: 'Something went wrong'));
    }, (categories) {
      // Loaded
      emit(CategoriesLoadedState(categories: categories));
    });
  }
}