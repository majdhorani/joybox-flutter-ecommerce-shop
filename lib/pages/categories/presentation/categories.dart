import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/widgets/general_app_bar.dart';
import 'package:spirit/core/widgets/loading.dart';
import 'package:spirit/di.dart';
import 'package:spirit/pages/categories/presentation/cubits/categories_cubit.dart';
import 'package:spirit/pages/categories/presentation/cubits/categories_state.dart';
import 'package:spirit/pages/categories/presentation/widgets/categories_list.dart';

class CategoriesPage extends StatefulWidget {
  const CategoriesPage({super.key});

  @override
  State<CategoriesPage> createState() => _CategoriesPageState();
}

class _CategoriesPageState extends State<CategoriesPage> {
  CategoriesCubit categoriesCubit = sl<CategoriesCubit>();

  @override
  void initState() {
    categoriesCubit.categories();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: Material(
        child: SafeArea(
          child: Column(
            children: [
              SGeneralAppBar(
                title: "categories".tr(),
              ),
              BlocBuilder<CategoriesCubit, CategoriesState>(
                  bloc: categoriesCubit,
                  builder: (context, state) {
                    if (state is CategoriesLoadingState) {
                      return HLoading(isWhite: true);
                    }

                    if (state is CategoriesLoadedState) {
                      return Expanded(
                        child: SingleChildScrollView(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const SizedBox(
                                  height: 8,
                                ),
                                CategoriesList(categories: state.categories),
                                const SizedBox(height: 10),
                                AspectRatio(
                                  aspectRatio: 4,
                                  child: Container(color: Colors.grey.shade200),
                                ),
                                const SizedBox(height: 16),
                              ],
                            ),
                          ),
                        ),
                      );
                    }

                    return const SizedBox();
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
