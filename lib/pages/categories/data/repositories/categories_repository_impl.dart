import 'package:dartz/dartz.dart';
import 'package:spirit/core/errors/exceptions.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/network/network_info.dart';
import '../../../../core/session/session.dart';
import '../../domain/entities/category.dart';
import '../../domain/repositories/categories_repository.dart';
import '../datasources/categories_local_datasource.dart';
import '../datasources/categories_remote_datasource.dart';

class CategoriesRepositoryImpl implements CategoriesRepository {
  CategoriesRemoteDataSource remoteDataSource;
  CategoriesLocalDataSource localDataSource;
  NetworkInfo networkInfo;
  CategoriesRepositoryImpl(
      {required this.remoteDataSource,
      required this.localDataSource,
      required this.networkInfo});

  @override
  Future<Either<Failure, List<Category>>> categories() async {
    // If cached get data from cache
    // var cachedCategories = Session.categories();
    // if (cachedCategories.isNotEmpty) {
    //   return Right(cachedCategories);
    // }

    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.categories();
        await Session.cacheCategories(response.data!);
        print('right');
        return Right(response.data!);
      } catch (e) {
        print(e);
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }

  @override
  Future<List<Category>> cachedCategories() {
    return localDataSource.categories();
  }
}
