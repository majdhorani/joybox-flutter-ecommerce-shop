import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import '../../../../core/errors/exceptions.dart';
import '../../../../core/server/responses/categories_response.dart';

abstract class CategoriesRemoteDataSource {
  Future<CategoriesResponse> categories();
}

class CategoriesRemoteDataSourceImpl implements CategoriesRemoteDataSource {
  Dio client;
  CategoriesRemoteDataSourceImpl({required this.client});

  @override
  Future<CategoriesResponse> categories() async {
    try {
      String jsonString = await rootBundle.loadString('assets/json/categories.json');
      Map<String, dynamic> jsonData = jsonDecode(jsonString);
      var result = CategoriesResponse.fromJson(jsonData);
      return result;
    } catch (e) {
      if (e is DioError) {
        // Status Code
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }
}
