import 'package:spirit/core/session/session.dart';
import '../../domain/entities/category.dart';

abstract class CategoriesLocalDataSource {
  Future<List<Category>> categories();
  Future<bool> cacheCategories(List<Category> categories);
}

class CategoriesLocalDataSourceImpl implements CategoriesLocalDataSource {
  @override
  Future<List<Category>> categories() async {
    return Session.categories();
  }

  @override
  Future<bool> cacheCategories(List<Category> categories) async {
    return await Session.cacheCategories(categories);
  }
}