import 'package:dartz/dartz.dart';
import 'package:spirit/core/errors/failures.dart';
import '../entities/category.dart';

abstract class CategoriesRepository {
  Future<Either<Failure,List<Category>>> categories();
  Future<List<Category>> cachedCategories();
}