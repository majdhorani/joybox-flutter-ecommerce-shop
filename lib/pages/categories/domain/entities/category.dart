class Category {
  int? id;
  String? parentId;
  String? name;
  String? image;
  String? order;
  List<Category>? subCategories;

  Category(
      {required this.id,
      required this.name,
      this.image,
      this.order,
      this.subCategories});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    parentId = json['parent_id'];
    name = json['name'];
    image = json['image'];
    order = json['order'];
    subCategories = json['sub_categories'] == null
        ? []
        : (json['sub_categories'] as List)
            .map((category) => Category.fromJson(category))
            .toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['parent_id'] = parentId;
    data['name'] = name;
    data['image'] = image;
    data['order'] = order;
    data['sub_categories'] = subCategories == null ? [] : subCategories?.map((e) => e.toJson()).toList();
    return data;
  }
}
