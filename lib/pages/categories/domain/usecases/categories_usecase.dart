import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';
import '../entities/category.dart';
import '../repositories/categories_repository.dart';

class CategoriesUseCase extends UseCase<List<Category>, CategoriesParams> {
  CategoriesRepository repository;
  CategoriesUseCase({required this.repository});

  @override
  Future<Either<Failure, List<Category>>> call(CategoriesParams params) {
    return repository.categories();
  }
}

class CategoriesParams extends Equatable {
  const CategoriesParams() : super();

  @override
  List<Object?> get props => [];
}
