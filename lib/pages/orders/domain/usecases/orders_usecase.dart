import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';
import '../../../../core/server/responses/orders_response.dart';
import '../repositories/orders_repository.dart';

class OrdersUseCase extends UseCase<OrdersResponse, OrdersParams> {
  OrdersRepository repository;
  OrdersUseCase({required this.repository});

  @override
  Future<Either<Failure, OrdersResponse>> call(OrdersParams params) {
    return repository.orders();
  }
}

class OrdersParams extends Equatable {
  const OrdersParams() : super();

  @override
  List<Object?> get props => [];
}
