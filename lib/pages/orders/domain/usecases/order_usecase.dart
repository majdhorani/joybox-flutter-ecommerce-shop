import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';
import '../../../../core/server/responses/order_response.dart';
import '../repositories/orders_repository.dart';

class OrderUseCase extends UseCase<OrderResponse, OrderParams> {
  OrdersRepository repository;
  OrderUseCase({required this.repository});

  @override
  Future<Either<Failure, OrderResponse>> call(OrderParams params) {
    return repository.order(id: params.id);
  }
}

class OrderParams extends Equatable {
  String id;
  OrderParams({required this.id}) : super();

  @override
  List<Object?> get props => [id];
}
