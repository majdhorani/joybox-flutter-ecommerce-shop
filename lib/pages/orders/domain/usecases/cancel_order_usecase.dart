import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/usecases/usecase.dart';
import '../repositories/orders_repository.dart';

class CancelOrderUseCase extends UseCase<bool, CancelOrderParams> {
  OrdersRepository repository;
  CancelOrderUseCase({required this.repository});

  @override
  Future<Either<Failure, bool>> call(CancelOrderParams params) {
    return repository.cancel(id: params.id);
  }
}

class CancelOrderParams extends Equatable {
  String id;
  CancelOrderParams({required this.id}) : super();

  @override
  List<Object?> get props => [id];
}
