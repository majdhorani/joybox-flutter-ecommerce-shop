import 'package:dartz/dartz.dart';
import '../../../../core/errors/failures.dart';
import '../../../../core/server/responses/order_response.dart';
import '../../../../core/server/responses/orders_response.dart';

abstract class OrdersRepository {
  Future<Either<Failure, OrdersResponse>> orders();
  Future<Either<Failure, OrderResponse>> order({required String id});
  Future<Either<Failure, bool>> cancel({required String id});
}
