import 'package:flutter/material.dart';
import 'package:spirit/core/theme/app_colors.dart';
import 'package:spirit/pages/addresses/domain/entities/address.dart';
import 'package:spirit/pages/recipients/domain/entities/recipient.dart';

class MyOrder {
  String id;
  String? date;
  int? quantity;
  double? total;
  double? subtotal;
  double? discount;
  double? deliveryFee;
  String? paymentMethod;
  String? status;
  Address? address;
  Recipient? recipient;

  MyOrder(
      {required this.id,
      required this.date,
      required this.quantity,
      required this.subtotal,
      required this.total,
      required this.discount,
      required this.deliveryFee,
      required this.paymentMethod,
      required this.status,
      required this.address,
      required this.recipient});

  factory MyOrder.fromJson(Map<String, dynamic> json) {
    return MyOrder(
        id: json['id'],
        date: json['date'],
        quantity: json['total_quantity'],
        subtotal: json['subtotal'],
        total: json['total'],
        paymentMethod: json['payment_method'],
        discount: json['discount'],
        deliveryFee: json['delivery_fee'],
        status: json['status'],
        address: json['address'] == null
            ? null
            : Address(
                state: json['address']['state'],
                district: json['address']['district'],
                address: json['address']['address'],
                building: json['address']['building'],
                floor: json['address']['floor'],
                apartment: json['address']['apartment'],
                latitude: json['address']['latitude'],
                longitude: json['address']['longitude']),
        recipient: Recipient(
            firstName: json['recipient']['first_name'],
            lastName: json['recipient']['last_name'],
            phone: json['recipient']['phone']));
  }

  bool isFreeDelivery() {
    return  deliveryFee == 0;
  }

  bool hasDiscount() {
    return discount != 0;
  }

  bool isPaymentMethodCod() {
    return paymentMethod?.toLowerCase() == 'cod';
  }

  bool isPending() {
    return status?.toLowerCase() == 'pending';
  }

  bool isWaitingForPayment() {
    return (status?.toLowerCase() == 'waiting for payment' ||
            status?.toLowerCase() == 'new') &&
        (paymentMethod == 'bank' || paymentMethod == 'tabby' || paymentMethod == 'ziina');
  }

  bool isCancelled() {
    return status?.toLowerCase() == 'cancelled';
  }

  bool isProcessing() {
    return status?.toLowerCase() == 'processing';
  }

  bool isFailed() {
    return status?.toLowerCase() == 'failed';
  }

  bool isCompleted() {
    return status?.toLowerCase() == 'completed';
  }
  
  bool isRefunded() {
    return status?.toLowerCase() == 'refunded';
  }

  bool canCancel() {
    return isWaitingForPayment();
  }

  Color getStatusBackgroundColor() {
    if(isWaitingForPayment()) {
      return const Color.fromARGB(255, 255, 184, 77).withOpacity(0.4);
    }
    if(isProcessing()) {
      return AppColors.grey;
    }
    if(isCompleted()) {
      return AppColors.green.withOpacity(0.2);
    }
    if(isCancelled() || isRefunded()) {
      return AppColors.red.withOpacity(0.2);
    }
    return AppColors.grey400;
  }

  Color getStatusTextColor() {
    if(isWaitingForPayment()) {
      return const Color.fromARGB(255, 149, 89, 0);
    }
    if(isCompleted()) {
      return const Color.fromARGB(255, 0, 93, 3);
    }
    if(isCancelled() || isRefunded()) {
      return AppColors.red;
    }
    if(isProcessing()) {
      return AppColors.black;
    }
    return Colors.black;
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
    };
  }
}
