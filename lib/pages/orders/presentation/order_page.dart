import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/utilities/utilities.dart';
import 'package:spirit/core/widgets/button.dart';
import 'package:spirit/core/widgets/general_app_bar.dart';
import 'package:spirit/pages/checkout/presenter/cubits/create_payment_link_cubit.dart';
import 'package:spirit/pages/checkout/presenter/cubits/create_payment_link_state.dart';
import 'package:spirit/pages/orders/domain/entities/order.dart';
import 'package:spirit/pages/orders/presentation/cubit/order_cubit.dart';
import 'package:spirit/pages/orders/presentation/cubit/order_state.dart';
import 'package:spirit/pages/orders/presentation/widgets/cancel_order_dialog.dart';
import 'package:spirit/pages/orders/presentation/widgets/order_item.dart';
import 'package:spirit/pages/orders/presentation/widgets/order_product.dart';
import 'package:spirit/routes/routes.dart';
import '../../../../core/theme/app_colors.dart';
import '../../../../core/widgets/loading.dart';
import '../../../../di.dart';

class OrderPage extends StatefulWidget {
  MyOrder order;
  OrderPage({Key? key, required this.order}) : super(key: key);

  @override
  State<OrderPage> createState() => _OrderPageState();
}

class _OrderPageState extends State<OrderPage> {
  OrderCubit orderCubit = sl<OrderCubit>();
  CreatePaymentLinkCubit createPaymentLinkCubit = sl<CreatePaymentLinkCubit>();
  MyOrder? order;

  @override
  void initState() {
    order = widget.order;
    super.initState();
    orderCubit.order(widget.order.id);
  }

  Widget Actions() {
    return BlocConsumer(
        bloc: createPaymentLinkCubit,
        listener: (context, state) async {
          print("Payment link status  $state");
          if (state is CreatePaymentLinkSuccessState) {
            if (state.link != null) {
              if (state.link!.isNotEmpty) {
                // Redirect to NGenius payment link
                await Navigator.of(context, rootNavigator: true)
                    .pushNamed(Routes.routeBrowser, arguments: state.link);
                orderCubit.order(widget.order.id);
              }
            }
          }
        },
        builder: (context, state) {
          return Column(
            children: [
              if (order!.canCancel())
                const Divider(height: 38, thickness: 2, color: AppColors.grey),
              Container(
                margin: const EdgeInsets.only(bottom: 8),
                child: HButton(
                    backgroundColor: Colors.green,
                    textColor: Colors.white,
                    iconColor: Colors.white,
                    textBold: true,
                    title: "contact_us".tr().toUpperCase(),
                    assetSvg: "assets/svg/whatsapp.svg",
                    svg: true,
                    disabled: state is CreatePaymentLinkLoadingState,
                    onPressed: () {
                      launchWhatsapp();
                    }),
              ),
              if (order!.canCancel())
                Row(
                  children: [
                    Expanded(
                      child: HButton(
                          backgroundColor: Colors.black,
                          textColor: Colors.white,
                          textBold: true,
                          title: "pay".tr().toUpperCase(),
                          disabled: state is CreatePaymentLinkLoadingState,
                          onPressed: () {
                            if (order != null) {
                              createPaymentLinkCubit.createPaymentLink(order!);
                            }
                          }),
                    ),
                    const SizedBox(width: 12),
                    Expanded(
                      child: HButton(
                          backgroundColor: Colors.white,
                          borderColor: Colors.grey,
                          textColor: Colors.black,
                          title: "cancel".tr().toUpperCase(),
                          disabled: state is CreatePaymentLinkLoadingState,
                          onPressed: () async {
                            var result = await showDialog(
                              context: context,
                              builder: (_) =>
                                  CancelOrderDialog(myOrder: widget.order),
                            );
                            if (result != null) {
                              if (result == true) {
                                orderCubit.order(widget.order.id);
                              }
                            }
                          }),
                    ),
                  ],
                ),
              const SizedBox(height: 16),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      backgroundColor: Colors.white,
      child: SafeArea(
        child: Material(
          child: Column(
            children: [
              SGeneralAppBar(
                title: "${"order".tr()} #${order?.id}",
                showBackButton: true,
              ),
              BlocConsumer<OrderCubit, OrderState>(
                bloc: orderCubit,
                listener: (context, state) {
                  if (state is OrderLoadedState) {
                    if (state.order != null) {
                      setState(() {
                        print('received order: ${state.order}');
                        order = state.order!;
                      });
                    }
                  }
                },
                builder: (context, state) {
                  if (state is OrderLoadingState) {
                    return HLoading(isWhite: true);
                  }
                  if (state is OrderLoadedState) {
                    return Expanded(
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: Column(
                            children: [
                              const SizedBox(height: 16),
                              OrderItem(order: widget.order, fullDetails: true),
                              // const Divider(
                              //           height: 32,
                              //           thickness: 2,
                              //           color: AppColors.grey),
                              const SizedBox(height: 24),
                              ListView.separated(
                                  itemCount: state.products!.length,
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 2),
                                  itemBuilder: (context, index) {
                                    return HOrderProduct(
                                        product: state.products![index]);
                                  },
                                  separatorBuilder: (context, index) {
                                    return const Divider(
                                        height: 32,
                                        thickness: 2,
                                        color: AppColors.grey);
                                  }),

                              if (!order!.canCancel())
                                const SizedBox(
                                  height: 16,
                                ),
                              Actions()
                              // HHeader(title: 'details'.tr()),
                              // const SizedBox(
                              //   height: 16,
                              // ),
                              // OrderDetails(
                              //     order: state.order!,
                              //     onCancel: (MyOrder order) async {
                              //       var result = await showDialog(
                              //         context: context,
                              //         builder: (_) =>
                              //             CancelOrderDialog(myOrder: order),
                              //       );
                              //       print('order cancelled result: $result');
                              //       if (result != null) {
                              //         if (result == true) {
                              //           orderCubit.order(order.id);
                              //         }
                              //       }
                              //     },
                              //     onPay: (MyOrder order) {}),
                              // const SizedBox(height: 20),
                            ],
                          ),
                        ),
                      ),
                    );
                  }
                  return const SizedBox();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
