import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import '../../domain/usecases/cancel_order_usecase.dart';
import '../../domain/usecases/order_usecase.dart';
import 'order_state.dart';

class OrderCubit extends Cubit<OrderState> {
  OrderUseCase orderUseCase;
  CancelOrderUseCase cancelOrderUseCase;

  OrderCubit(
      { required this.orderUseCase, required this.cancelOrderUseCase })
      : super(OrderInitialState());

  void order(String id) async {
    // Loading
    emit(OrderLoadingState());

    // Fetch products
    var response = await orderUseCase.call(OrderParams(id: id));
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(OrderFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(OrderFailedState(message: 'Something went wrong'));
    }, (response) {
      // Loaded
      print('order response ${response.products.length}');
      emit(OrderLoadedState(products: response.products, order: response.order));
    });
  }

  void cancel(String id) async {
    // Loading
    emit(OrderLoadingState());

    // Fetch products
    var response = await cancelOrderUseCase.call(CancelOrderParams(id: id));
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(OrderFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(OrderFailedState(message: 'Something went wrong'));
    }, (response) {
      // Loaded
      emit(OrderLoadedState());
    });
  }

}
