import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/errors/failures.dart';
import '../../domain/usecases/orders_usecase.dart';
import 'orders_state.dart';

class OrdersCubit extends Cubit<OrdersState> {
  OrdersUseCase ordersUseCase;

  OrdersCubit(
      { required this.ordersUseCase })
      : super(OrdersLoadingState());

  void orders() async {
    // Loading
    emit(OrdersLoadingState());

    // Fetch products
    var response = await ordersUseCase.call(const OrdersParams());
    response.fold((failure) {
      if (failure is ServerFailure) {
        emit(OrdersFailedState(
            message: failure.message ?? 'Something went wrong'));
      }
      // Error
      emit(OrdersFailedState(message: 'Something went wrong'));
    }, (response) {
      // Loaded
      emit(OrdersLoadedState(orders: response.orders));
    });
  }

}
