import 'package:equatable/equatable.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';
import '../../domain/entities/order.dart';

abstract class OrderState extends Equatable {
  const OrderState();

  @override
  List<Object> get props => [];
}

class OrderInitialState extends OrderState {}
class OrderLoadingState extends OrderState {}

class OrderLoadedState extends OrderState {
  List<Product>? products;
  MyOrder? order;

  double getSubtotal() {
    double subtotal = 0;
    products?.forEach((product) {
      subtotal += product.total();
    });
    print('subtotal: $subtotal');
    return subtotal;
  }

  OrderLoadedState({this.products, this.order});
}

class OrderFailedState extends OrderState {
  String message;
  OrderFailedState({required this.message});
}
