import 'package:equatable/equatable.dart';
import '../../domain/entities/order.dart';

abstract class OrdersState extends Equatable {
  const OrdersState();

  @override
  List<Object> get props => [];
}

class OrdersLoadingState extends OrdersState {}

class OrdersLoadedState extends OrdersState {
  List<MyOrder>? orders;

  OrdersLoadedState({this.orders});
}

class OrdersFailedState extends OrdersState {
  String message;
  OrdersFailedState({required this.message});
}
