import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spirit/core/widgets/general_app_bar.dart';
import 'package:spirit/core/widgets/loading.dart';
import 'package:spirit/di.dart';
import 'package:spirit/pages/orders/domain/entities/order.dart';
import 'package:spirit/pages/orders/presentation/cubit/orders_cubit.dart';
import 'package:spirit/pages/orders/presentation/cubit/orders_state.dart';
import 'package:spirit/pages/orders/presentation/widgets/order_item.dart';
import 'package:spirit/pages/orders/presentation/widgets/orders_empty.dart';
import 'package:spirit/routes/routes.dart';

class OrdersPage extends StatefulWidget {
  const OrdersPage({super.key});

  @override
  State<OrdersPage> createState() => _OrdersPageState();
}

class _OrdersPageState extends State<OrdersPage> {
  OrdersCubit ordersCubit = sl<OrdersCubit>();

  Widget OrdersList(List<MyOrder> orders) {
    return ListView.separated(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (context, index) {
          MyOrder order = orders[index];
          return OrderItem(
            order: order, 
            onTap: (order) {
              Navigator.pushNamed(context, Routes.routeOrder, arguments: order);
            });
        },
        separatorBuilder: (context, state) {
          return Divider(
            height: 16,
            color: Colors.grey.withOpacity(0.0),
            thickness: 2,
          );
        },
        itemCount: orders.length);
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: SafeArea(
        child: Material(
          child: Column(
            children: [
              SGeneralAppBar(
                title: "orders".tr(),
              ),
              BlocConsumer<OrdersCubit, OrdersState>(
                  bloc: ordersCubit,
                  listener: (context, state) {},
                  builder: (context, state) {
                    if (state is OrdersLoadingState) {
                      return HLoading(isWhite: true);
                    }

                    if (state is OrdersLoadedState) {
                      if(state.orders!.isEmpty) {
                        return const OrdersEmpty();
                      }
                      return Expanded(
                        child: SingleChildScrollView(
                          child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                OrdersList(state.orders ?? []),
                                Divider(
                                  height: 30,
                                  color: Colors.grey.withOpacity(0.1),
                                  thickness: 2,
                                ),
                                const SizedBox(height: 10),
                              ],
                            ),
                          ),
                        ),
                      );
                    }
                    return const SizedBox();
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
