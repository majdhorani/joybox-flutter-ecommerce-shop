import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../../core/theme/app_colors.dart';
import '../../../product/domain/entities/product.dart';
import '../../../../core/extensions/string_extensions.dart';

class HOrderProduct extends StatefulWidget {
  Product product;
  Function? onRemoved;
  Function? onUpdated;
  HOrderProduct(
      {Key? key, required this.product, this.onRemoved, this.onUpdated})
      : super(key: key);

  @override
  State<HOrderProduct> createState() => _HOrderProductState();
}

class _HOrderProductState extends State<HOrderProduct> {
  late int quantity;

  @override
  void initState() {
    super.initState();
    quantity = widget.product.quantity!;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          children: [
            Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                        border: Border.all(color: AppColors.black.withOpacity(0.2))),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: CachedNetworkImage(
                        imageUrl: widget.product.getImage(),
                        fit: BoxFit.fitHeight,
                      ),
                    ),
                  ),
                  const SizedBox(width: 16),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(widget.product.name ?? '',
                            style: const TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold)),
                        const SizedBox(height: 4),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('subtotal'.tr(),
                                  style: const TextStyle(fontSize: 12)),
                              Text('EUR ${widget.product.total().toString().formatPrice()}',
                                  style: const TextStyle(fontSize: 12))
                            ]),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('price'.tr(),
                                  style: const TextStyle(fontSize: 12)),
                              Text('EUR ${widget.product.salePrice}',
                                  style: const TextStyle(fontSize: 12))
                            ]),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('quantity'.tr(),
                                  style: const TextStyle(fontSize: 12)),
                              Text(widget.product.quantity.toString(),
                                  style: const TextStyle(fontSize: 12))
                            ]),
                      ],
                    ),
                  ),
                ]),
          ],
        )
      ],
    );
  }
}
