import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:spirit/pages/orders/domain/entities/order.dart';

class OrderItem extends StatefulWidget {
  MyOrder order;
  bool fullDetails;
  Function(MyOrder order)? onTap;
  OrderItem(
      {super.key,
      required this.order,
      this.onTap,
      this.fullDetails = false});

  @override
  State<OrderItem> createState() => _OrderItemState();
}

class _OrderItemState extends State<OrderItem> {
  Widget Status() {
    return Row(
      children: [
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
          decoration: BoxDecoration(
              color: widget.order.getStatusBackgroundColor(),
              borderRadius: BorderRadius.circular(6)),
          child: Text(
            widget.order.status ?? '',
            style: TextStyle(fontSize: 13, fontFamily: "Oswald", color: widget.order.getStatusTextColor()),
          ),
        ),
      ],
    );
  }

  Widget Product() {
    return Container(
      color: Colors.grey.withOpacity(0.1),
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.only(bottom: 6),
      child: const Padding(
        padding: EdgeInsets.symmetric(horizontal: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                child: Text(
              "Apple IPhone 15 Pro Max, 256GB Blue Titanium",
              style: TextStyle(
                fontFamily: "Oswald",
              ),
            )),
            Text(
              "x1",
              style: TextStyle(
                color: Colors.grey,
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onTap == null ? null : () {
        widget.onTap!(widget.order);
      },
      child: Stack(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                  child: Container(
                decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.05),
                  border: Border.all(color: Colors.black.withOpacity(0.2)),
                  borderRadius: BorderRadius.circular(6),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.grey.withOpacity(0.2),
                              borderRadius: BorderRadius.circular(6)),
                          padding: const EdgeInsets.symmetric(
                              vertical: 6, horizontal: 8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Order No #${widget.order.id}",
                                style: const TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 18,
                                    fontFamily: "Oswald"),
                              ),
                              Row(
                                children: [
                                  Status(),
                                  // const SizedBox(width: 2),
                                  // const Icon(Icons.keyboard_arrow_right)
                                ],
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10, vertical: 10),
                      child: Column(
                        children: [
                          if (widget.fullDetails)
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  "subtotal".tr(),
                                  style: const TextStyle(
                                      fontSize: 14, fontFamily: "Oswald"),
                                ),
                                Text(
                                  widget.order.subtotal.toString(),
                                  style: const TextStyle(
                                      fontSize: 14, fontFamily: "Oswald"),
                                ),
                              ],
                            ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                "total".tr(),
                                style: const TextStyle(
                                    fontSize: 14, fontFamily: "Oswald"),
                              ),
                              Text(
                                "EUR ${widget.order.total}",
                                style: const TextStyle(
                                    fontSize: 14, fontFamily: "Oswald"),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                "recipient".tr(),
                                style: const TextStyle(
                                    fontSize: 14, fontFamily: "Oswald"),
                              ),
                              Text(
                                "${widget.order.recipient?.firstName ?? '-'} ${widget.order.recipient?.lastName ?? ''}",
                                style: const TextStyle(
                                    fontSize: 14, fontFamily: "Oswald"),
                              ),
                            ],
                          ),
                          if (widget.fullDetails)
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  "recipient_phone".tr(),
                                  style: const TextStyle(
                                      fontSize: 14, fontFamily: "Oswald"),
                                ),
                                Text(
                                  "${widget.order.recipient?.phone}",
                                  style: const TextStyle(
                                      fontSize: 14, fontFamily: "Oswald"),
                                ),
                              ],
                            ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                "payment".tr(),
                                style: const TextStyle(
                                    fontSize: 14, fontFamily: "Oswald"),
                              ),
                              Text(
                                "${widget.order.paymentMethod}",
                                style: const TextStyle(
                                    fontSize: 14, fontFamily: "Oswald"),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                "date".tr(),
                                style: const TextStyle(
                                    fontSize: 14, fontFamily: "Oswald"),
                              ),
                              Text(
                                widget.order.date ?? '-',
                                style: const TextStyle(
                                    fontSize: 14, fontFamily: "Oswald"),
                              ),
                            ],
                          ),
                          if (widget.fullDetails)
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  "city".tr(),
                                  style: const TextStyle(
                                      fontSize: 14, fontFamily: "Oswald"),
                                ),
                                Text(
                                  "${widget.order.address?.state}",
                                  style: const TextStyle(
                                      fontSize: 14, fontFamily: "Oswald"),
                                ),
                              ],
                            ),
                          if (widget.fullDetails)
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  "district".tr(),
                                  style: const TextStyle(
                                      fontSize: 14, fontFamily: "Oswald"),
                                ),
                                Text(
                                  "${widget.order.address?.district}",
                                  style: const TextStyle(
                                      fontSize: 14, fontFamily: "Oswald"),
                                ),
                              ],
                            ),
                          if (widget.fullDetails)
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  "address".tr(),
                                  style: const TextStyle(
                                      fontSize: 14, fontFamily: "Oswald"),
                                ),
                                Text(
                                  "${widget.order.address?.address}",
                                  style: const TextStyle(
                                      fontSize: 14, fontFamily: "Oswald"),
                                ),
                              ],
                            ),
                          if (widget.fullDetails)
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  "building".tr(),
                                  style: const TextStyle(
                                      fontSize: 14, fontFamily: "Oswald"),
                                ),
                                Text(
                                  "${widget.order.address?.building}",
                                  style: const TextStyle(
                                      fontSize: 14, fontFamily: "Oswald"),
                                ),
                              ],
                            ),
                          if (widget.fullDetails)
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  "floor".tr(),
                                  style: const TextStyle(
                                      fontSize: 14, fontFamily: "Oswald"),
                                ),
                                Text(
                                  "${widget.order.address?.floor}",
                                  style: const TextStyle(
                                      fontSize: 14, fontFamily: "Oswald"),
                                ),
                              ],
                            ),
                          if (widget.fullDetails)
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  "apartment".tr(),
                                  style: const TextStyle(
                                      fontSize: 14, fontFamily: "Oswald"),
                                ),
                                Text(
                                  "${widget.order.address?.apartment}",
                                  style: const TextStyle(
                                      fontSize: 14, fontFamily: "Oswald"),
                                ),
                              ],
                            ),
                        ],
                      ),
                    ),
                    // Divider(height: 1, color: Colors.grey.withOpacity(0.2),),
                    // Container(
                    //       decoration: BoxDecoration(
                    //           color: Colors.grey.withOpacity(0),
                    //           borderRadius: BorderRadius.circular(6)),
                    //       padding: const EdgeInsets.symmetric(
                    //           vertical: 6, horizontal: 8),
                    //       child: Row(
                    //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //         children: [
                    //           Text(
                    //             "${widget.order.date}",
                    //             style: const TextStyle(
                    //               fontWeight: FontWeight.w300,
                    //                 fontSize: 12, fontFamily: "Oswald"),
                    //           ),
                    //           Row(
                    //             children: [

                    //               // Status(),
                    //               // const SizedBox(width: 2),
                    //               // const Icon(Icons.keyboard_arrow_right)
                    //             ],
                    //           )
                    //         ],
                    //       ),
                    //     ),
                  ],
                ),
              )),
            ],
          ),
        ],
      ),
    );
  }
}
