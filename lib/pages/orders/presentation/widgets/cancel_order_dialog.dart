import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../core/theme/app_colors.dart';
import '../../../../core/widgets/button.dart';
import '../../../../core/widgets/loading.dart';
import '../../../../di.dart';
import '../../domain/entities/order.dart';
import '../cubit/order_cubit.dart';
import '../cubit/order_state.dart';

class CancelOrderDialog extends StatefulWidget {
  MyOrder myOrder;
  CancelOrderDialog({Key? key, required this.myOrder})
      : super(key: key);

  @override
  State<CancelOrderDialog> createState() => _CancelOrderDialogState();
}

class _CancelOrderDialogState extends State<CancelOrderDialog> {
  OrderCubit orderCubit = sl<OrderCubit>();

  @override
  void initState() {
    super.initState();
  }

  Widget _buildOrderCanceled() {
    return Container(
      height: 175,
      padding: const EdgeInsets.symmetric(horizontal: 16),
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: double.infinity,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text('order_cancelled'.tr().toUpperCase(),
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20),
                      textAlign: TextAlign.center),
                  Text('your_order_has_been_cancelled_successfully'.tr(args: [widget.myOrder.id]),
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 14),
                      textAlign: TextAlign.center),
                ]),
          ),
          const Divider(
            height: 20,
            color: AppColors.grey400,
          ),
          HButton(
              title: 'close'.tr().toUpperCase(),
              backgroundColor: AppColors.black,
              onPressed: () {
                Navigator.maybePop(context, true);
              })
        ],
      ),
    );
  }

  Widget _buildOrderCancellingFailed() {
    return Container(
      height: 190,
      padding: const EdgeInsets.symmetric(horizontal: 16),
      decoration: const BoxDecoration(color: Colors.white),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: double.infinity,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text('cancellation_failed'.tr(),
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20),
                      textAlign: TextAlign.center),
                  Text('cancellation_failed_description'.tr(),
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 14),
                      textAlign: TextAlign.center),
                ]),
          ),
          const Divider(
            height: 20,
            color: AppColors.grey400,
          ),
          HButton(
              title: 'close'.tr(),
              backgroundColor: AppColors.black,
              onPressed: () {
                Navigator.maybePop(context, false);
              })
        ],
      ),
    );
  }

  Widget _buildCancelOrder() {
    return Container(
      height: 175,
      padding: const EdgeInsets.symmetric(horizontal: 16),
      decoration: const BoxDecoration(color: Colors.white),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: double.infinity,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text("${'cancel_order'.tr().toUpperCase()} #${widget.myOrder.id}",
                      style: const TextStyle(
                        fontFamily: "Oswald",
                          fontWeight: FontWeight.bold, fontSize: 20),
                      textAlign: TextAlign.center),
                  Text('are_you_sure_you_want_to_cancel_your_order'.tr(),
                      style: const TextStyle(
                          fontSize: 16),
                      textAlign: TextAlign.center),
                ]),
          ),
          const Divider(
            height: 20,
            color: AppColors.grey400,
          ),
          Row(children: [
            Expanded(
              child: HButton(
                  title: 'no'.tr().toUpperCase(),
                  backgroundColor: AppColors.black,
                  onPressed: () {
                    Navigator.pop(context);
                  }),
            ),
            const SizedBox(width: 10),
            Expanded(
              child: HButton(
                  title: 'yes'.tr().toUpperCase(),
                  onPressed: () {
                    orderCubit.cancel(widget.myOrder.id);
                  }),
            ),
          ],)
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0))),
          backgroundColor: Colors.transparent,
      contentPadding: const EdgeInsets.only(),
      content: BlocConsumer<OrderCubit, OrderState>(
        bloc: orderCubit,
        listener: (context, state) {},
        builder: (context, state) {
          if(state is OrderLoadingState) {
            return Container(
                height: 175,
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: HLoading(isWhite:true)
            );
          }

          if(state is OrderLoadedState) {
            return _buildOrderCanceled();
          }

          if(state is OrderFailedState) {
            return _buildOrderCancellingFailed();
          }

          if(state is OrderInitialState) {
            return _buildCancelOrder();
          }
          return const SizedBox();
        }
      ),
    );
  }
}
