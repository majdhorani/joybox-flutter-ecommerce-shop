import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:spirit/core/widgets/button.dart';
import '../../../../core/theme/app_colors.dart';
import '../../domain/entities/order.dart';

class OrderDetails extends StatefulWidget {
  MyOrder order;
  Function(MyOrder order)? onPressed;
  Function(MyOrder order) onCancel;
  Function(MyOrder order) onPay;
  OrderDetails(
      {Key? key,
      this.onPressed,
      required this.order,
      required this.onCancel,
      required this.onPay})
      : super(key: key);

  @override
  State<OrderDetails> createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (widget.onPressed != null) {
          widget.onPressed!(widget.order);
        }
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text('order'.tr(),
                style:
                    const TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
            Text('#${widget.order.id}',
                style:
                    const TextStyle(fontSize: 12, fontWeight: FontWeight.bold))
          ]),
          
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text('date'.tr(),
                style: const TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
            Text(widget.order.date ?? '',
                style:
                    const TextStyle(fontSize: 12, fontWeight: FontWeight.bold))
          ]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text('status'.tr(),
                style: const TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
            Text(widget.order.status ?? '',
                style:
                    const TextStyle(fontSize: 12, fontWeight: FontWeight.bold))
          ]),
          
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text('subtotal'.tr(),
                style: const TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
            Text('AED ${widget.order.subtotal}',
                style:
                    const TextStyle(fontSize: 12, fontWeight: FontWeight.bold))
          ]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text('delivery'.tr(),
                style: const TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
            if (!widget.order.isFreeDelivery())
              Text('AED ${widget.order.deliveryFee}',
                  style: const TextStyle(
                      fontSize: 12, fontWeight: FontWeight.bold)),
            if (widget.order.isFreeDelivery())
              Text('free'.tr(),
                  style: const TextStyle(
                      color: AppColors.green,
                      fontSize: 12,
                      fontWeight: FontWeight.bold))
          ]),
          if (widget.order.hasDiscount())
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Text('discount'.tr(),
                  style: const TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
              Text('AED ${widget.order.discount}',
                  style: const TextStyle(
                      fontSize: 12, fontWeight: FontWeight.bold))
            ]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text('total'.tr(),
                style: const TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
            Text('AED ${widget.order.total}',
                style:
                    const TextStyle(fontSize: 12, fontWeight: FontWeight.bold))
          ]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text('quantity'.tr(),
                style:
                    const TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
            Text(
                '${widget.order.quantity} ${widget.order.quantity == "1" ? 'product'.tr() : 'products'.tr()}',
                style:
                    const TextStyle(fontSize: 12, fontWeight: FontWeight.bold))
          ]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text('payment_method'.tr(),
                style:
                    const TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
            Text(widget.order.paymentMethod?.toUpperCase() ?? '',
                style:
                    const TextStyle(fontSize: 12, fontWeight: FontWeight.bold))
          ]),
          const SizedBox(
            height: 18,
          ),
          Row(mainAxisAlignment: MainAxisAlignment.end, children: [
            if (widget.order.isWaitingForPayment())
              SizedBox(
                  width: 100,
                  child: HButton(
                      title: 'pay'.tr(),
                      rounded: true,
                      onPressed: () {
                        widget.onPay(widget.order);
                      },
                      backgroundColor: AppColors.red)),
            if (widget.order.isWaitingForPayment()) const SizedBox(width: 10),
            if (widget.order.canCancel())
              SizedBox(
                  width: 100,
                  child: HButton(
                      title: 'cancel'.tr(),
                      rounded: true,
                      onPressed: () {
                        widget.onCancel(widget.order);
                      },
                      backgroundColor: AppColors.black))
          ]),
        ],
      ),
    );
  }
}
