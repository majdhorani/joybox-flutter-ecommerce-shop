import 'dart:convert';
import 'dart:developer';
import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:spirit/core/server/configurations.dart';
import '../../../../core/errors/exceptions.dart';
import '../../../../core/server/responses/order_response.dart';
import '../../../../core/server/responses/orders_response.dart';
import '../../../../core/session/session.dart';

abstract class OrdersRemoteDataSource {
  Future<OrdersResponse> orders();
  Future<OrderResponse> order({required String id});
  Future<bool> cancel({required String id});
}

class OrdersRemoteDataSourceImpl implements OrdersRemoteDataSource {
  Dio client;
  OrdersRemoteDataSourceImpl({required this.client});

  @override
  Future<OrdersResponse> orders() async {
    try {
      String jsonString = await rootBundle.loadString('assets/json/orders.json');
      Map<String, dynamic> jsonData = jsonDecode(jsonString);
      var result = OrdersResponse.fromJson(jsonData);
      return result;
    } catch (e) {
      log(e.toString());
      if (e is DioError) {
        // Status Code
        log(e.response.toString());
        print(e.requestOptions.baseUrl);
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }

  @override
  Future<OrderResponse> order({required String id}) async {
    try {
      String jsonString = await rootBundle.loadString('assets/json/order.json');
      Map<String, dynamic> jsonData = jsonDecode(jsonString);
      var result = OrderResponse.fromJson(jsonData);
      return result;
    } catch (e) {
      print(e);
      if (e is DioError) {
        // Status Code
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }

  @override
  Future<bool> cancel({required String id}) async {
    try {
      var token = Session.token();
      var response = await client.post(
          ServerConfigurations.cancelOrder,
          options: Options(
            headers: {"token": token},
          ),
        data: jsonEncode({'order_id': id})
      );
      print('Cancel order Response');
      print(response.data);
      return response.data['data'];
    } catch (e) {
      if (e is DioError) {
        // Status Code
        print(e.response);
        var statusCode = e.response?.statusCode;
        Map<String, dynamic> json = e.response?.data;
        throw ServerException(statusCode: statusCode);
      } else {
        throw ServerException();
      }
    }
  }
}
