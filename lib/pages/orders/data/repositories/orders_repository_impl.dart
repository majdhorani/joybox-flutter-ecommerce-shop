import 'package:dartz/dartz.dart';
import 'package:spirit/core/errors/failures.dart';
import 'package:spirit/core/network/network_info.dart';
import 'package:spirit/core/server/responses/orders_response.dart';
import '../../../../core/errors/exceptions.dart';
import '../../../../core/server/responses/order_response.dart';
import '../../domain/repositories/orders_repository.dart';
import '../datasources/orders_local_datasource.dart';
import '../datasources/orders_remote_datasource.dart';

class OrdersRepositoryImpl implements OrdersRepository {
  OrdersRemoteDataSource remoteDataSource;
  OrdersLocalDataSource localDataSource;
  NetworkInfo networkInfo;

  OrdersRepositoryImpl(
      {
        required this.remoteDataSource,
        required this.localDataSource,
        required this.networkInfo
      });

  @override
  Future<Either<Failure, OrderResponse>> order({required String id}) async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.order(id: id);
        return Right(response);
      } catch (e) {
        print(e);
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }

  @override
  Future<Either<Failure, OrdersResponse>> orders() async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.orders();
        return Right(response);
      } catch (e) {
        print(e);
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> cancel({required String id}) async {
    if (await networkInfo.isConnected) {
      try {
        var response = await remoteDataSource.cancel(id: id);
        return Right(response);
      } catch (e) {
        print(e);
        if (e is ServerException) {
          return Left(
              ServerFailure(statusCode: e.statusCode, message: e.message));
        } else {
          return Left(ServerFailure());
        }
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }


}
