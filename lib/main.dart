import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:spirit/core/session/session.dart';
import 'package:spirit/di.dart';
import 'package:spirit/routes/routes.dart';

void main() async {
  // await Firebase.initializeApp();
  WidgetsFlutterBinding.ensureInitialized();
  await registerDependencies();
  await EasyLocalization.ensureInitialized();
  runApp(EasyLocalization(
      supportedLocales: const [Locale('en'), Locale('ar')],
      path: 'assets/translations',
      fallbackLocale: const Locale('en'),
      child: const App()));
}

class App extends StatefulWidget {
  const App({super.key});

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        debugShowCheckedModeBanner: false,
        title: 'JOYBOX',
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
        ),
        initialRoute:
            Session.isLoggedIn() ? Routes.routeDashboard : Routes.routeStart,
        onGenerateRoute: Routes.generateRoutes);
  }
}
