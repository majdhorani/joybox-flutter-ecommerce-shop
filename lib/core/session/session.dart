import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:spirit/di.dart';
import 'package:spirit/pages/account/domain/entities/user_info.dart';
import 'package:spirit/pages/auth/domain/entities/user.dart';
import 'package:spirit/pages/categories/domain/entities/category.dart';
import 'package:spirit/pages/product/domain/entities/product.dart';

class Session {
  static String cacheSessionKey = 'session';
  static String cacheIsFirstTimeKey = 'isFirstTime';
  static String cacheTokenKey = 'token';
  static String cacheIsLoggedIn = 'isLoggedIn';
  static String cacheCategoriesKey = 'categories';
  static String cacheBrandsKey = 'brands';
  static String cacheUserKey = 'user';
  static String cacheWishlistKey = 'wishlist';
  static String cacheCartKey = 'cart';
  static String cacheUserInfoKey = 'userInfo';

  static SharedPreferences _getSharedPreference() {
    SharedPreferences sharedPreferences = sl();
    return sharedPreferences;
  }

  static Future<bool> setIsFirstTime(bool isFirstTime) async {
    return _getSharedPreference().setBool(cacheIsFirstTimeKey, isFirstTime);
  }

  static bool isFirstTime() {
    return _getSharedPreference().getBool(cacheIsFirstTimeKey) ?? true;
  }

  static Future<User?> user() async {
    var userJson = _getSharedPreference().getString(cacheUserKey);
    if (userJson != null) {
      return User.fromJson(jsonDecode(userJson));
    }
    return null;
  }

  static Future<bool> setUser(User user) async {
    return _getSharedPreference().setString(cacheUserKey, jsonEncode(user));
  }

  static Future<bool> setToken(String token) async {
    return _getSharedPreference().setString(cacheTokenKey, token);
  }

  static String? token() {
    return _getSharedPreference().getString(cacheTokenKey);
  }

  static Future<bool> setIsLoggedIn(bool isLoggedIn) async {
    return _getSharedPreference().setBool(cacheIsLoggedIn, isLoggedIn);
  }

  static bool isLoggedIn() {
    return _getSharedPreference().getBool(cacheIsLoggedIn) ?? false;
  }

  static Future<bool> cacheCategories(List<Category> categories) async {
    var categoriesJson = jsonEncode(categories);
    return _getSharedPreference().setString(cacheCategoriesKey, categoriesJson);
  }

  // Get cached categories
  static List<Category> categories() {
    String? cardsJson = _getSharedPreference().getString(cacheCategoriesKey);
    if (cardsJson == null) {
      return [];
    }
    var categories = (jsonDecode(cardsJson) as List)
        .map((value) => Category.fromJson(value))
        .toList();
    return categories;
  }

  // Cache brands
  // static Future<bool> cacheBrands(List<Brand> brands) async {
  //   var brandsJson = jsonEncode(brands);
  //   return _getSharedPreference().setString(CACHE_BRANDS, brandsJson);
  // }

  // Get cached brands
  // static List<Brand> brands() {
  //   String? brandsJson = _getSharedPreference().getString(CACHE_BRANDS);
  //   if (brandsJson == null) {
  //     return [];
  //   }
  //   var brands = (jsonDecode(brandsJson) as List)
  //       .map((value) => Brand.fromJson(value))
  //       .toList();
  //   return brands;
  // }

  // Get cached wishlist
  static List<Product> wishlist() {
    String? wishlistJson = _getSharedPreference().getString(cacheWishlistKey);
    if (wishlistJson == null) {
      return [];
    }
    var wishlist = (jsonDecode(wishlistJson) as List)
        .map((value) => Product.fromJson(value))
        .toList();
    return wishlist;
  }

  // Get cached wishlist
  static List<Product> cart() {
    String? cartJson = _getSharedPreference().getString(cacheCartKey);
    if (cartJson == null) {
      return [];
    }
    var cart = (jsonDecode(cartJson) as List)
        .map((value) => Product.fromJson(value))
        .toList();
    return cart;
  }

  // Cache wishlist
  static Future<bool> cacheWishlist(List<Product> products) async {
    var wishlistJson =
        jsonEncode([...products.map((e) => e.toJson()).toList()]);
    return _getSharedPreference().setString(cacheWishlistKey, wishlistJson);
  }

  // Cache cart
  static Future<bool> cacheCart(List<Product> products) async {
    var cartJson = jsonEncode([...products.map((e) => e.toJson()).toList()]);
    return _getSharedPreference().setString(cacheCartKey, cartJson);
  }

  // Add to cart
  static Future<bool> addToeCart(Product product) async {
    var _cart = cart();
    bool found = false;
    for (var element in _cart) {
      // If product found!, update product in cart
      if (element.id == product.id) {
        print("INN");
        found = true;
        element.quantity = product.quantity;
      }
    }

    // If not found, Add to cart
    if (!found) {
      _cart.add(product);
    }

    // Cache cart
    await cacheCart(_cart);
    return found;
  }

  // Remove from cart
  static Future<bool> removeFromCart(String id) async {
    var _cart = cart();
    int index = -1;
    for (int i = 0; i < _cart.length; i++) {
      if (_cart[i].id == id) {
        index = i;
        break;
      }
    }

    if (index != -1) {
      _cart.removeAt(index);
    }

    // Cache cart
    await cacheCart(_cart);
    return index != -1;
  }

  // Add to wishlist
  static Future<bool> addToeWishlist(Product product) async {
    var _wishlist = wishlist();
    bool found = false;
    for (var element in _wishlist) {
      // If product found!, update product in cart
      if (element.id == product.id) {
        found = true;
        element = product;
      }
    }

    // If not found, Add to cart
    if (!found) {
      _wishlist.add(product);
    }

    print('wishlist to cache');
    print(_wishlist);

    // Cache cart
    await cacheWishlist(_wishlist);
    return found;
  }

  // Remove from wishlist
  static Future<bool> removeFromWishlist(String id) async {
    var _wishlist = wishlist();
    int index = -1;
    for (int i = 0; i < _wishlist.length; i++) {
      if (_wishlist[i].id == id) {
        index = i;
        break;
      }
    }

    if (index != -1) {
      _wishlist.removeAt(index);
    }

    // Cache cart
    await cacheWishlist(_wishlist);
    return index != -1;
  }

  // Cache Billing Address
  // static Future<bool> cacheBillingAddress(Address? address) async {
  //   var addressJson = jsonEncode(address);
  //   return _getSharedPreference().setString(CACHE_BILLING_ADDRESS, addressJson);
  // }

  // // Cache Billing Address
  // static Future<bool> cacheShippingAddress(Address? address) async {
  //   var addressJson = jsonEncode(address);
  //   return _getSharedPreference().setString(CACHE_SHIPPING_ADDRESS, addressJson);
  // }

  // // Billing address
  // static Future<Address?> billingAddress() async {
  //   var billingAddressJson = _getSharedPreference().getString(CACHE_BILLING_ADDRESS);
  //   return billingAddressJson == null ? null : Address.fromJson(jsonDecode(billingAddressJson));
  // }

  // // Shipping address
  // static Future<Address?> shippingAddress() async {
  //   var shippingAddressJson = _getSharedPreference().getString(CACHE_SHIPPING_ADDRESS);
  //   return shippingAddressJson == null ? null : Address.fromJson(jsonDecode(shippingAddressJson));
  // }

  // Cache User Info
  static Future<bool> cacheUserInfo(UserInfo info) async {
    var userInfoJson = jsonEncode(info);
    return _getSharedPreference().setString(cacheUserInfoKey, userInfoJson);
  }

  // User inf
  static Future<UserInfo?> userInfo() async {
    var userInfoJson = _getSharedPreference().getString(cacheUserInfoKey);
    return userInfoJson == null
        ? null
        : UserInfo.fromJson(jsonDecode(userInfoJson));
  }

  static Future<void> logout() async {
    await cacheWishlist([]);
    await cacheCart([]);
    await setIsLoggedIn(false);
  }
}
