enum SliderType {
  product, category, webview, image
}

SliderType stringToSliderType(String value) {
  return SliderType.values.firstWhere(
    (e) => e.toString().split('.').last == value,
    orElse: () => SliderType.image,
  );
}
