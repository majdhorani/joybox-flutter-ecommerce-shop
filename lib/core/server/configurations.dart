class ServerConfigurations {
  // Test Server
  //static String URL = '';
  // Live Server
  static String url = '';
  static String uploads = '$url/uploads/';

  static String login = 'Login/login';
  static String register = 'Login/register';
  static String brands = 'Api/Brands';
  static String categories = 'Api/Categories';
  static String products = 'Api/products';
  static String recentlyAdded = 'Api/recently_added';
  static String bestSellers = 'Api/best_sellers';
  static String youMayLike = 'Api/you_may_like';
  static String product = 'Api/product';
  static String wishlist = 'Api/favourit_products';
  static String addToWishlist = 'Api/favourite'; //product_id=
  static String removeFromWishlist = 'Api/un_favourite'; // product_id=
  static String cart = 'Api/cart';
  static String addToCart = 'Api/addToCart'; // {item_id, quantity}
  static String removeFromCart = 'Api/removeFromCart'; // item_id=
  static String orders = 'Api/myOrders';
  static String order = 'Api/orderItems'; // id=
  static String cancelOrder = 'Api/cancele_order'; // order_id
  static String checkout = 'Api/order'; // [{quantity, price, item_id}]
  static String address = 'Api/address';
  static String addresses = 'Api/addresses';
  static String recipient = 'Api/recipient';
  static String recipients = 'Api/recipients';
  static String slider = 'Api/slider';
  static String repair = 'Api/repair';
  static String userInfo = 'Api/userInfo';
  static String calculate = 'Api/calc';
  static String paymentLink = 'Api/paymentLink'; // order_id
}

class Configurations {
  static String whatsapp = "+971";
}