
import 'package:spirit/pages/product/domain/entities/product.dart';

class CartResponse {
  List<Product> products;

  CartResponse({required this.products});

  factory CartResponse.fromJson(Map<String, dynamic> json) {
    return CartResponse(
      products: (json['data'] == null)
          ? []
          : (json['data'] as List)
              .map((product) => Product.fromJson(product))
              .toList(),
    );
  }
}
