import 'package:spirit/pages/categories/domain/entities/category.dart';

class CategoriesResponse {
  List<Category>? data;

  CategoriesResponse({required List<Category> this.data});

  factory CategoriesResponse.fromJson(Map<String, dynamic> json) {
    print('parent json');
    return CategoriesResponse(
        data: (json['data'] == null)
            ? []
            : (json['data'] as List)
                .map((category) => Category.fromJson(category))
                .toList());
  }

  // Map<String, dynamic> toJson() {
  //   final Map<String, dynamic> data = {};
  //   var products = data.map((key, value) => value.toJson());
  //   return products;
  // }
}
