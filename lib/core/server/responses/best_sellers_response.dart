import 'package:spirit/pages/product/domain/entities/product.dart';

class BestSellersResponse {
  List<Product> data;

  BestSellersResponse({required this.data});

  factory BestSellersResponse.fromJson(Map<String, dynamic> json) {
    return BestSellersResponse(
      data: (json['data'] == null)
          ? []
          : (json['data'] as List)
              .map((product) => Product.fromJson(product))
              .toList(),
    );
  }
}
