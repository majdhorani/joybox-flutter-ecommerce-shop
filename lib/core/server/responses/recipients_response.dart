import 'package:spirit/pages/recipients/domain/entities/recipient.dart';

class RecipientsResponse {
  List<Recipient> data;

  RecipientsResponse({required this.data});

  factory RecipientsResponse.fromJson(Map<String, dynamic> json) {
    return RecipientsResponse(
      data: (json['data'] == null)
          ? []
          : (json['data'] as List)
              .map((product) => Recipient.fromJson(product))
              .toList(),
    );
  }
}
