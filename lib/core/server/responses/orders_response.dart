
import 'package:spirit/pages/orders/domain/entities/order.dart';

class OrdersResponse {
  List<MyOrder> orders;

  OrdersResponse(
      {required this.orders});

  factory OrdersResponse.fromJson(Map<String, dynamic> json) {
    return OrdersResponse(
        orders : (json['data'] == null) ? [] : (json['data'] as List).map((order) => MyOrder.fromJson(order)).toList(),
    );
  }

  // Map<String, dynamic> toJson() {
  //   final Map<String, dynamic> data = {};
  //   var products = data.map((key, value) => value.toJson());
  //   return products;
  // }
}
