
import 'package:spirit/pages/product/domain/entities/product.dart';

class WishlistResponse {
  List<Product> products;

  WishlistResponse({required this.products});

  factory WishlistResponse.fromJson(Map<String, dynamic> json) {
    return WishlistResponse(
        products: (json['data'] == null)
            ? []
            : (json['data'] as List)
                .map((product) => Product.fromJson(product))
                .toList(),
        //attributes: (json['filters'] == null) ? [] : (json['filters'] as List).map((product) => Attribute.fromJson(product)).toList()
        );
  }

  // Map<String, dynamic> toJson() {
  //   final Map<String, dynamic> data = {};
  //   var products = data.map((key, value) => value.toJson());
  //   return products;
  // }
}
