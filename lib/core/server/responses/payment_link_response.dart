class PaymentLinkResponse {
  String? data;

  PaymentLinkResponse({required this.data});

  factory PaymentLinkResponse.fromJson(Map<String, dynamic> json) {
    return PaymentLinkResponse(
      data: json['payment_link'],
    );
  }
}
