
import 'package:spirit/pages/addresses/domain/entities/address.dart';

class AddressesResponse {
  List<Address> data;

  AddressesResponse({required this.data});

  factory AddressesResponse.fromJson(Map<String, dynamic> json) {
    return AddressesResponse(
      data: (json['data'] == null)
          ? []
          : (json['data'] as List)
              .map((product) => Address.fromJson(product))
              .toList(),
    );
  }
}
