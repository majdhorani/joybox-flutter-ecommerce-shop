import 'package:spirit/pages/product/domain/entities/product.dart';

import '../../../pages/orders/domain/entities/order.dart';

class OrderResponse {
  List<Product> products;
  MyOrder order;

  OrderResponse({required this.products, required this.order});

  factory OrderResponse.fromJson(Map<String, dynamic> json) {
    return OrderResponse(
      products: (json['data'] == null)
          ? []
          : (json['data']['items'] as List)
              .map((product) => Product.fromJson(product))
              .toList(),
      order: MyOrder.fromJson(json['data']['order'])
    );
  }
}
