class CheckoutResponse {
  int orderId;
  String paymentLink;

  CheckoutResponse({required this.orderId, required this.paymentLink});

  factory CheckoutResponse.fromJson(Map<String, dynamic> json) {
    return CheckoutResponse(
      orderId: json['order_id'], 
      paymentLink: json['payment_link']);
  }
}