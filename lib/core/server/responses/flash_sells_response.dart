import 'package:spirit/pages/product/domain/entities/product.dart';

class FlashSellsResponse {
  List<Product> data;

  FlashSellsResponse({required this.data});

  factory FlashSellsResponse.fromJson(Map<String, dynamic> json) {
    return FlashSellsResponse(
      data: (json['data'] == null)
          ? []
          : (json['data'] as List)
              .map((product) => Product.fromJson(product))
              .toList(),
    );
  }
}
