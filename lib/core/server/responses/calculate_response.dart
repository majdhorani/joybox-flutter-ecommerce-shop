class CalculateResponse {
  String? message;
  String? subtotal;
  String? discount;
  String? itemTotal;
  String? deliveryFee;
  String? total;
  CalculateResponse(
      {required this.message,
      required this.subtotal,
      required this.discount,
      required this.itemTotal,
      required this.deliveryFee,
      required this.total});

  bool isCouponDiscountApplied() {
    return (discount ?? '0') != '0';
  }

  bool hasMessage() {
    return (message ?? '').isNotEmpty;
  }

  double untilFreeDelivery() {
    var result = (100 - double.parse(subtotal ?? '0'));
    if (result < 0) {
      result = 0;
    }
    return result;
  }

  bool isFreeDelivery() {
    return untilFreeDelivery() == 0;
  }

  factory CalculateResponse.fromJson(Map<String, dynamic> json) {
    return CalculateResponse(
      message: json['data']['message'] == null
          ? null
          : json['data']['message'].toString(),
      subtotal: json['data']['subtotal'] == null
          ? null
          : json['data']['subtotal'].toString(),
      discount: json['data']['discount'] == null
          ? null
          : json['data']['discount'].toString(),
      itemTotal: json['data']['item_total'] == null
          ? null
          : json['data']['item_total'].toString(),
      deliveryFee: json['data']['delivery_fee'] == null
          ? null
          : json['data']['delivery_fee'].toString(),
      total: json['data']['total'] == null
          ? null
          : json['data']['total'].toString(),
    );
  }
}
