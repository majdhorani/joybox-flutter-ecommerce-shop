
import 'package:spirit/pages/home/domain/entities/slider.dart';

class SliderResponse {
  List<Slider>? data;

  SliderResponse({required List<Slider> this.data});

  factory SliderResponse.fromJson(Map<String, dynamic> json) {
    print('parent json');
    return SliderResponse(
        data: (json['data'] == null)
            ? []
            : (json['data'] as List)
                .map((category) => Slider.fromJson(category))
                .toList());
  }
}
