
import 'package:spirit/pages/auth/domain/entities/user.dart';

class RegisterResponse {
  User? user;

  RegisterResponse(
      {required this.user});

  factory RegisterResponse.fromJson(Map<String, dynamic> json) {
    return RegisterResponse(
        user: User.fromJson(json['data']),
    );
  }

  // Map<String, dynamic> toJson() {
  //   final Map<String, dynamic> data = {};
  //   var products = data.map((key, value) => value.toJson());
  //   return products;
  // }
}
