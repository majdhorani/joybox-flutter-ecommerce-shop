
import 'package:spirit/pages/auth/domain/entities/user.dart';

class LoginResponse {
  User? user;

  LoginResponse(
      {required this.user});

  factory LoginResponse.fromJson(Map<String, dynamic> json) {
    print('parsing object');
    print(json);
    return LoginResponse(user: User.fromJson(json['data']));
  }

  // Map<String, dynamic> toJson() {
  //   final Map<String, dynamic> data = {};
  //   var products = data.map((key, value) => value.toJson());
  //   return products;
  // }
}
