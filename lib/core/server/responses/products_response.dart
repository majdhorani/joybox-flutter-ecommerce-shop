import 'package:spirit/pages/product/domain/entities/product.dart';

class ProductsResponse {
  List<Product> products;
  int totalCount;

  ProductsResponse(
      {required this.products, required this.totalCount});

  int pages() {
    return (totalCount/10).ceil();
  }

  factory ProductsResponse.fromJson(Map<String, dynamic> json) {
    return ProductsResponse(
        products : (json['products'] == null) ? [] : (json['products'] as List).map((product) => Product.fromJson(product)).toList(),
        totalCount: (json['total'] == null) ? 0 : json['total']
    );
  }
}
