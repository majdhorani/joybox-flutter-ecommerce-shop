import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:iconsax/iconsax.dart';

import '../theme/app_colors.dart';

class HTextField extends StatefulWidget {
  IconData? icon;
  String? labelText;
  String? hintText;
  TextEditingController? controller;
  bool passwordField;
  int maxLength;
  TextInputType type;
  Function()? onTap;
  Function(String value)? onSubmit;
  bool readOnly;
  bool disabled;
  TextAlign textAlign;
  Function? onValidate, onChanged;
  EdgeInsets? contentPadding;
  Color? fillColor;
  Widget? iconWidget;
  String? error;
  String? title;
  List<TextInputFormatter>? formatter;
  HTextField(
      {Key? key,
      this.icon,
      this.labelText,
      this.hintText,
      this.controller,
      this.passwordField = false,
      this.maxLength = 30,
      this.type = TextInputType.text,
      this.textAlign = TextAlign.start,
      this.onValidate,
      this.onTap,
      this.onSubmit,
      this.readOnly = false,
      this.disabled = false,
      this.contentPadding,
      this.onChanged,
      this.fillColor = const Color(0xFFF2F2F2),
      this.iconWidget,
      this.error,
      this.title,
      this.formatter})
      : super(key: key);

  @override
  State<HTextField> createState() => _HTextFieldState();
}

class _HTextFieldState extends State<HTextField> {
  bool obscureText = false;

  @override
  void initState() {
    super.initState();
    obscureText = widget.passwordField;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (widget.title != null)
          Text(widget.title ?? "",
              style: const TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
        if (widget.title != null) const SizedBox(height: 8),
        TextFormField(
          inputFormatters: widget.formatter,
          enabled: !widget.disabled,
          cursorColor: Colors.black,
          controller: widget.controller,
          obscureText: obscureText,
          maxLength: widget.maxLength,
          keyboardType: widget.type,
          readOnly: widget.readOnly,
          onTap: widget.onTap,
          onFieldSubmitted: (value) {
            print("onFieldSubmitted: $value");
            if (widget.onSubmit != null) {
              widget.onSubmit!(value);
            }
          },
          validator: (value) {
            if (widget.onValidate != null) {
              return widget.onValidate!(value);
            }
            return null;
          },
          style: const TextStyle(
            color: Colors.black,
            fontFamily: 'Montserrat',
            fontWeight: FontWeight.w400,
            fontSize: 16.0,
            letterSpacing: 0.15,
          ),
          textAlign: widget.textAlign,
          onChanged: (value) =>
              widget.onChanged != null ? widget.onChanged!(value) : null,
          decoration: InputDecoration(
            labelText: widget.labelText,
            counterText: "",
            errorText: widget.error,
            fillColor: widget.disabled
                ? Colors.grey.withOpacity(0.4)
                : widget.fillColor,
            filled:
                (widget.fillColor != null || widget.disabled) ? true : false,
            contentPadding: widget.contentPadding ??
                const EdgeInsets.symmetric(horizontal: 12),
            hintText: widget.hintText,
            hintStyle: const TextStyle(
                color: Colors.grey,
                fontFamily: 'Montserrat',
                fontWeight: FontWeight.w400,
                fontSize: 16.0,
                letterSpacing: 0.15),
            labelStyle: const TextStyle(
                fontFamily: 'Montserrat',
                color: Colors.black,
                fontSize: 16.0,
                fontWeight: FontWeight.w400,
                letterSpacing: 0.15),
            prefixIcon: widget.icon != null
                ? Icon(
                    widget.icon,
                    color: Colors.black,
                    size: 18,
                  )
                : widget.iconWidget,
            suffixIcon: widget.passwordField
                ? IconButton(
                    icon: obscureText
                        ? const Icon(Iconsax.eye, color: Colors.black, size: 18)
                        : const Icon(
                            Iconsax.eye_slash,
                            color: Colors.black,
                            size: 18,
                          ),
                    onPressed: () {
                      setState(() {
                        obscureText = !obscureText;
                      });
                    },
                  )
                : null,
            enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(
                  color: Color(0xFFF2F2F2), width: 1),
              borderRadius: BorderRadius.circular(8.0),
            ),
            disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey.shade400, width: 1),
              borderRadius: BorderRadius.circular(8.0),
            ),
            errorBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.redAccent, width: 2),
              borderRadius: BorderRadius.circular(8.0),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.redAccent, width: 2),
              borderRadius: BorderRadius.circular(8.0),
            ),
            floatingLabelStyle: const TextStyle(
              color: Colors.black,
              fontSize: 18.0,
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: AppColors.black, width: 1.5),
              borderRadius: BorderRadius.circular(8.0),
            ),
          ),
        ),
      ],
    );
  }

  bool isObscureText() {
    if (widget.passwordField) {
      return true;
    }
    return false;
  }
}
