import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:spirit/pages/home/presentation/cubits/sliders_cubit.dart';
import 'package:spirit/pages/home/presentation/cubits/sliders_state.dart';
import '../../di.dart';
import '../theme/app_colors.dart';

class ImagesSlider extends StatefulWidget {
  bool usePadding;
  ImagesSlider({Key? key, this.usePadding = true}) : super(key: key);

  @override
  State<ImagesSlider> createState() => _ImagesSliderState();
}

class _ImagesSliderState extends State<ImagesSlider> {
  SlidersCubit slidersCubit = sl<SlidersCubit>();

  @override
  void initState() {
    super.initState();
    slidersCubit.sliders();
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(8),
      child: BlocBuilder<SlidersCubit, SlidersState>(
        bloc: slidersCubit,
        builder: (context, state) {
          if(state is SlidersLoadedState) {
            return ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Container(
                height: 160,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: AppColors.grey400, width: 0.3)
                ),
                margin: EdgeInsets.symmetric(horizontal: widget.usePadding ? 16 : 0),
                child: CarouselSlider(
                    options: CarouselOptions(
                      enlargeCenterPage: false,
                      viewportFraction: 1,
                      aspectRatio: 1.0,
                      autoPlay: true,
                      autoPlayInterval: const Duration(seconds: 5),
                      autoPlayAnimationDuration: const Duration(milliseconds: 800)
                    ),
                    items: state.sliders.map((slider) => SizedBox(
                      width: double.infinity,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: CachedNetworkImage(
                          imageUrl: slider.getImage(),
                          fit: BoxFit.cover,
                          progressIndicatorBuilder: (context, url, downloadProgress) =>
                          const Center(child: SpinKitCircle(size: 20, color: Colors.white)),
                          errorWidget: (context, url, error) => const Icon(Icons.error),
                        ),
                      ),
                    )).toList())),
            );
          }
    
          return const SizedBox();
        }
      ),
    );
  }
}
