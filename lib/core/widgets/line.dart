import 'package:flutter/cupertino.dart';

import '../theme/app_colors.dart';

class HLine extends StatelessWidget {
  const HLine({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Expanded(child: Container(width: 73, height: 1, color: AppColors.dividerGreyColor),)
    ]);
  }
}