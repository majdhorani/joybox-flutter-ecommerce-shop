import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:spirit/core/theme/app_colors.dart';

class HButton extends StatefulWidget {
  String title;
  Function onPressed;
  bool disabled;
  bool rounded;
  Color backgroundColor;
  Color? borderColor;
  Color? textColor;
  IconData? iconData;
  Color? iconColor;
  String? assetSvg;
  bool svg;
  bool iconAlignmentRight;
  bool textBold;
  HButton({
    Key? key,
    required this.title,
    required this.onPressed,
    this.disabled = false,
    this.rounded = false,
    this.svg = false,
    this.iconData,
    this.iconColor,
    this.borderColor,
    this.textColor,
    this.textBold = false,
    this.iconAlignmentRight = false,
    this.assetSvg,
    this.backgroundColor = AppColors.red
  }) : super(key: key);

  @override
  State<HButton> createState() => _HButtonState();
}

class _HButtonState extends State<HButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 54,
      decoration: widget.borderColor == null ? null : BoxDecoration(border: Border.all(width: 1, color: widget.disabled ? AppColors.grey500 : widget.borderColor!),borderRadius: BorderRadius.circular(widget.rounded ? 50  : 8),),
      child: Material(
        color: widget.disabled ? AppColors.grey500 : widget.backgroundColor,
        borderRadius: BorderRadius.circular(widget.rounded ? 50  : 8),
        child: InkWell(
          borderRadius: BorderRadius.circular(widget.rounded ? 50  : 8),
          onTap: widget.disabled ? null : () {
            widget.onPressed();
          },
          child: Center(child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if(widget.assetSvg != null && widget.iconData == null)
                Container(
                    width: 24,
                    height: 24,
                    margin: const EdgeInsets.only(right: 10),
                    child: SvgPicture.asset(widget.assetSvg!, color: widget.iconColor,)),
              if(widget.assetSvg == null && widget.iconData != null && !widget.iconAlignmentRight)
                Container(
                    width: 24,
                    height: 24,
                    margin: const EdgeInsets.only(right: 10),
                    child: Icon(widget.iconData, color: widget.iconColor)),
              Text(widget.title, style: TextStyle(fontSize: 14, color: widget.textColor ?? Colors.white, fontWeight: widget.textBold ?  FontWeight.w500 : null),),
              if(widget.assetSvg == null && widget.iconData != null && widget.iconAlignmentRight)
                Container(
                    width: 24,
                    height: 24,
                    margin: const EdgeInsets.only(left: 10),
                    child: Icon(widget.iconData, color: widget.iconColor)),
            ],
          )),
        ),),
    );
  }
}
