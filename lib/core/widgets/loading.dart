import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class HLoading extends StatelessWidget {
  bool isWhite;
  HLoading({Key? key, this.isWhite = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Center(
        child: Container(
            color: isWhite ? Colors.white : Colors.black.withOpacity(0.8),
            child: SpinKitCircle(color: isWhite ? Colors.black : Colors.white)),
      ),
    );
  }
}
