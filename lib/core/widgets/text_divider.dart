import 'package:flutter/material.dart';

class TextDivider extends StatelessWidget {
  String text;
  double height;
  TextDivider({super.key, required this.text, this.height = 1});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: Divider(
            indent: 20.0,
            endIndent: 10.0,
            thickness: 1,
            height: height,
          ),
        ),
        Text(
          text,
          style: const TextStyle(color: Colors.blueGrey),
        ),
        Expanded(
          child: Divider(
            indent: 10.0,
            endIndent: 20.0,
            thickness: 1,
            height: height,
          ),
        ),
      ],
    );
  }
}
