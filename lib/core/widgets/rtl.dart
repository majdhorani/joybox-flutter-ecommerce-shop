import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';

class HRtl extends StatelessWidget {
  Widget child;
  HRtl({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Transform.scale(
        scaleX: context.locale.toString() == 'ar' ? -1 : 1,
        child: child);
  }
}
