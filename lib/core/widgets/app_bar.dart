import 'package:flutter/material.dart';
import 'package:iconsax/iconsax.dart';
import 'package:spirit/core/theme/app_colors.dart';
import 'package:spirit/routes/routes.dart';

class HomeBar extends StatelessWidget {
  const HomeBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top, bottom: 8),
      color: Colors.white,
      child: Column(
        children: [
          const SizedBox(height: 8),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                  padding: const EdgeInsets.only(left: 16),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 30,
                        decoration: BoxDecoration(color: Colors.black, borderRadius: BorderRadius.circular(4)),
                        padding: const EdgeInsets.symmetric(horizontal: 6),
                        alignment: Alignment.center,
                        child: Text(
                          "JoyBox".toUpperCase(),
                          style: const TextStyle(
                              color: AppColors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 18),
                        ),
                      )
                    ],
                  )),
              const SizedBox(width: 10),
              Expanded(
                  child: Container(
                    height: 30  ,
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                decoration: BoxDecoration(
                  border: Border.all(color: const Color.fromARGB(255, 2, 2, 2).withOpacity(0.2)),
                    color: Colors.grey.withOpacity(0.2),
                    borderRadius: BorderRadius.circular(4)),
                    alignment: Alignment.center,
                child: Text(
                  "What are you looking for?",
                  style: TextStyle(color: Colors.black.withOpacity(0.4)),
                  maxLines: 1,
                ),
              )),
              Padding(
                padding: const EdgeInsets.only(right: 16, left: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: 34,
                      width: 34,
                      decoration: BoxDecoration(border: Border.all(
                        width: 0.6,
                        color: Colors.black), borderRadius: BorderRadius.circular(100)),
                      child: Material(
                          borderRadius: BorderRadius.circular(16),
                        child: InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, Routes.routeSearch);
                          },
                          child: const Icon(
                            Iconsax.heart,
                            size: 20,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),  
                    const SizedBox(width: 8),
                    Container(
                      height: 34,
                      width: 34,
                      decoration: BoxDecoration(border: Border.all(
                        width: 0.6,
                        color: Colors.black), borderRadius: BorderRadius.circular(100)),
                      child: Material(
                          borderRadius: BorderRadius.circular(16),
                        child: InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, Routes.routeSearch);
                          },
                          child: const Icon(
                            Iconsax.search_normal_1,
                            size: 16,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
          // const SizedBox(height: 8),
          // Container(
          //   width: double.infinity,
          //   height: .5,
          //   color: Colors.black.withOpacity(0.6),
          // )
        ],
      ),
    );
  }
}
