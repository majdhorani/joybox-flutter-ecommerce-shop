import 'package:flutter/material.dart';
import '../theme/app_colors.dart';

class HDropDown extends StatefulWidget {
  String value;
  List<String> options;
  Function? onChange;
  HDropDown(
      {Key? key, required this.value, required this.options, this.onChange})
      : super(key: key);

  @override
  State<HDropDown> createState() => _HDropDownState();
}

class _HDropDownState extends State<HDropDown> {
  late String value;

  @override
  void initState() {
    super.initState();
    value = widget.value;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(100),
          border: Border.all(color: AppColors.textFieldEnableColor)),
      child: DropdownButton(
        isExpanded: true,
        dropdownColor: Colors.white,
        style: const TextStyle(
          color: Colors.black,
          backgroundColor: Colors.white,
        ),
        value: value,
        items: widget.options
            .map((item) => DropdownMenuItem(value: item, child: Text(item)))
            .toList(),
        onChanged: (String? value) {
          if (value != null) {
            setState(() {
              this.value = value;
            });
            if (widget.onChange != null) {
              widget.onChange!(value);
            }
          }
        },
      ),
    );
  }
}
