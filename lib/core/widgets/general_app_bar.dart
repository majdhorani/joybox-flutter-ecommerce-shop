import 'package:flutter/material.dart';

class SGeneralAppBar extends StatelessWidget {
  final String title;
  final bool showBackButton;

  SGeneralAppBar({Key? key, required this.title, this.showBackButton = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: Column(
        children: [
          const SizedBox(height: 6),
          Stack(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  if (showBackButton)
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: const Padding(
                        padding: EdgeInsets.only(left: 16),
                        child: Icon(
                          Icons.arrow_back_ios,
                          size: 24,
                        ),
                      ),
                    ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Text(
                        title.toUpperCase(),
                        maxLines: 1, // Set the maximum number of lines
                        overflow: TextOverflow.ellipsis, // Handle overflow by ellipsis
                        style: const TextStyle(
                          color: Colors.black,
                          fontSize: 22,
                          fontFamily: "Oswald",
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
          const SizedBox(height: 8),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Container(
              width: double.infinity,
              height: 2,
              color: Colors.grey.withOpacity(0.4),
            ),
          )
        ],
      ),
    );
  }
}