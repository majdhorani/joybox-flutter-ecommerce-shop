import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:spirit/core/theme/app_colors.dart';
import 'line.dart';

class HHeader extends StatelessWidget {
  String title;
  bool showBackButton;

  HHeader({Key? key, required this.title, this.showBackButton = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                decoration: BoxDecoration(color: Colors.grey.shade200, borderRadius: BorderRadius.circular(4)),
                width: double.infinity,
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                child: Text(title,
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                        color: Colors.black.withOpacity(0.6),
                        fontFamily: "Oswald"),
                    textAlign: TextAlign.start)),
            // const SizedBox(height: 10),
            // const HLine(),
          ],
        ),
        if (showBackButton)
          Positioned(
              right: 0,
              top: 0,
              bottom: 0,
              child: InkWell(
                child: Image.asset('assets/images/back.png',
                    color: AppColors.black, height: 20),
                onTap: () {
                  Navigator.maybePop(context);
                },
              ))
      ],
    );
  }
}
