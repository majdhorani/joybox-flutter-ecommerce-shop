import 'dart:async';
import 'package:flutter/material.dart';

class ImagesSwitcher extends StatefulWidget {
  final List<String> images;

  ImagesSwitcher({required this.images});

  @override
  _ImageSwitcherState createState() => _ImageSwitcherState();
}

class _ImageSwitcherState extends State<ImagesSwitcher>
    with TickerProviderStateMixin {
  int currentIndex = 0;

  @override
  void initState() {
    super.initState();
    // Start the automatic image switch
    startImageSwitch();
  }

  void startImageSwitch() {
    const switchInterval = Duration(seconds: 5);

    Timer.periodic(switchInterval, (Timer timer) {
      setState(() {
        currentIndex = (currentIndex + 1) % widget.images.length;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
          duration: Duration(seconds: 1),
          opacity: 1.0,
          child: Image.asset(
            widget.images[currentIndex],
            fit: BoxFit.cover,
          ),
        );
  }
}