// s_bottom_navigation_bar.dart
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:iconsax/iconsax.dart';

class SBottomNavigationBar extends CupertinoTabBar {
  SBottomNavigationBar({
    Key? key,
    required int currentIndex,
    required Function(int index) onTap,
  }) : super(
          backgroundColor: Colors.black,
          activeColor: Colors.white,
          inactiveColor: Colors.grey,
          height: 60,
          key: key,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(
                Iconsax.main_component,
                size: 30,
              ),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Iconsax.search_normal,
                size: 30,
              ),
              label: 'Categories',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Iconsax.shopping_cart,
                size: 30,
              ),
              label: 'Cart',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Iconsax.gift,
                size: 30,
              ),
              label: 'Orders',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Iconsax.user,
                size: 30,
              ),
              label: 'Account',
            ),
          ],
          onTap: onTap,
          currentIndex: currentIndex,
        );
}
