import 'package:flutter/material.dart';
import 'package:iconsax/iconsax.dart';
import 'package:spirit/core/theme/app_colors.dart';

class ErrorMessage extends StatelessWidget {
  String message;
  ErrorMessage({super.key, required this.message});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 16),
      decoration: BoxDecoration(
          color: AppColors.red, borderRadius: BorderRadius.circular(4)),
      width: double.infinity,
      child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 12),
          child: Row(
            children: [
              const Icon(Iconsax.warning_2, color: Colors.white,),
              const SizedBox(width: 16),
              Expanded(child: Text(message, style: const TextStyle(color: Colors.white))),
            ],
          )),
    );
  }
}
