import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  const Failure([
    List properties = const <dynamic>[],
  ]) : super();
}

class ServerFailure extends Failure {
  //ErrorResponse? error;
  int? statusCode;
  String? message;
  ServerFailure({this.statusCode, this.message});

  // String? getMessage() {
  //   if (error == null) return 'Server error, Please try again later';
  //   return error?.message;
  // }

  @override
  List<Object?> get props => [];
}

class CacheFailure extends Failure {
  @override
  List<Object?> get props => [];
}

class NoConnectionFailure extends Failure {
  @override
  List<Object?> get props => [];
}

