import 'package:flutter/cupertino.dart';

extension TextEditingControllerExtension on TextEditingController {
  bool isEmpty() {
    return text.isEmpty;
  }
}