extension NumericExtension on String {
  bool isNumeric() {
    if (this == null) {
      return false;
    }
    return double.tryParse(this) != null;
  }

  bool isEmail() {
    return RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(this);
  }

  String formatPrice() {
    double parsedPrice = double.tryParse(this) ?? 0.0;
    return parsedPrice.toStringAsFixed(2);
  }
}