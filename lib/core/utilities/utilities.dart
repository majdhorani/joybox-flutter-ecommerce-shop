import 'dart:io';
import 'package:spirit/core/server/configurations.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:fluttertoast/fluttertoast.dart';

void launchWhatsapp() async{
   var contact = Configurations.whatsapp;
   var androidUrl = "whatsapp://send?phone=$contact&text=Hello, I need some help";
   var iosUrl = "https://wa.me/$contact?text=${Uri.parse('Hello, I need some help')}";
   
   try{
      if(Platform.isIOS){
         await launchUrl(Uri.parse(iosUrl));
      }
      else{
         await launchUrl(Uri.parse(androidUrl));
      }
   } on Exception{
     Fluttertoast.showToast(msg: "WhatsApp is not installed.");
  }
}