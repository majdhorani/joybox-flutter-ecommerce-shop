import 'dart:ui';

class AppStyles {
  static TextStyle bold12TextStyle = TextStyle(fontSize: 12, fontWeight: FontWeight.bold);
  static var  regular12TextStyle = TextStyle(fontSize: 12);
  static var  regular9TextStyle = TextStyle(fontSize: 9);
}