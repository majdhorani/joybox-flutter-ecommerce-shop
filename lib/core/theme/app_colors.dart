import 'package:flutter/material.dart';

class AppColors {
  static const lightPurple = Color(0xff825ef6);
  static const darkPurple = Color(0xff3b31a3);
  static const mainGreen = Color(0xff43d2d8);
  // static const primaryPink = Color(0xfff85974);
  static const primaryBlue = Color(0xff00629E);
  static const primaryOrange = Color(0xffffa818);
  static const paleYellowBackground = Color(0xffFAFCF4);

  static const red = Color(0xffEB212E);
  static const lightGreen = Color(0xffa9c824);
  static const lightYellow = Color(0xfff2cb04);


  static const white = Color(0xffFFFFFF);
  static const black = Color(0xff000000);
  static const grey = Color(0xffF2F4F4);

  static const buttonBorder = Color(0xffE5E5E5);
  static const formColor = Color(0xffF2F4F4);



  static const blackAccent = Color(0xff474647);
  static const grey50 = Color(0xff94a0b4);
  static const grey100 = Color(0xffF7F9FC);
  static const grey200 = Color(0xffEEF2F8);
  static const grey300 = Color(0xffdce1ea);
  static const grey400 = Color(0xffBAC3D2);
  static const grey500 = Color(0xff94A0B4);
  static const grey600 = Color(0xff717E95);
  static const grey666 = Color(0xff666666);
  static const grey800 = Color(0xff636773);
  static const textFieldGrey = Color(0xfff9f9f9);
  static const grey900 = Color(0xff293345);
  static const defaultIconColor = Color(0xffB3B4BA);
  static const violet = Color(0xff163560);
  static const shimmerColor = Color(0xffEEF2F8);
  static const shimmerHighlightColor = Color(0xffdce1ea);
  static const imagesBorderColor = Color(0xffD9D9D9);
  static const greenishTextColor = Color(0xff455A64);

  static const dividerGreyColor = Color(0xFFDADADA);
  static const textFieldEnableColor = Color(0xFFC9C9C9);
  static const blue = Color(0xFF3C8FDC);

  static const appBar = Color(0xFFCCCCCC);
  static const menu = Color(0xFF232222);
  static const menuItemSelected = Color(0xFFCCCCCC);

  static const branchHeaderUnSelected = Color(0xFFCCCCCC);
  static const branchHeaderSelected = Color(0xFFD9D9D9);
  static const bottomNavigationBar = Color(0xFF222222);
  static const textColor = Color(0xFF343E48);
  static const green = Color(0xff4AB72C);
  static const buttonGrey = Color(0xFFEFECEC);
  
  
  static const productBackground = Color(0xFFD9D9D9);
}
