# JoyBox Flutter Ecommerce Shop

<b>Screenshots</b>
<p align="center">
  <img src="https://i.ibb.co/71CdBPB/Screenshot-1710917278.png" width="250" title="hover text">
  <img src="https://i.ibb.co/CtB57Vz/Screenshot-1710917289.png" width="250" title="hover text">
  <img src="https://i.ibb.co/27yyC0G/Screenshot-1710917299.png" width="250" title="hover text">
  <img src="https://i.ibb.co/sKdBgzS/Screenshot-1710917320.png" width="250" title="hover text">
  <img src="https://i.ibb.co/sH4LsPL/Screenshot-1710917326.png" width="250" title="hover text">
  <img src="https://i.ibb.co/7Rqk4Jd/Screenshot-1710917335.png" width="250" title="hover text">
  <img src="https://i.ibb.co/3CQ1Xhs/Screenshot-1710917340.png" width="250" title="hover text">
</p>

<br>
<br>
<br>

<b>Architecture and Dependency Rule</b>
<p align="center">
<img src="https://miro.medium.com/v2/resize:fit:828/format:webp/1*HbeyJXebaioUc3SGBGhlow.png" width="400" title="hover text">
</p>
